/* Main.java 
 * 
 *  APCS Labs 2011-2021
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 */

public class Main
{
    public static void main(String[] args) 
    {
        // This is a recursion Group Quiz. Your partners have already been assigned to you.
        // Your task is to write/insert appropriate lines of code in AMazing.java and
        // here in Main.java to create a random maze and solve this maze. The locations 
        // of your code are labeled from 1 to 11 in these two files. Good luck!
        int N = 20;        // Experiment to see what this does.
        AMazing maze = new AMazing(N);
        StdDraw.show(0);
        maze.drawMaze();  // Experiment to see what this does.
        
        /******************************************************************
         *** Note you must complete all these tasks in the given order. ***
         ******************************************************************/
        
        // Study Amazing.java and correctly complete problems 1-5 in this order.
        // 6. Insert appropriate code here (or edit above code) to test your code in parts 1-5.
        
        
        // 11. Insert appropriate code here (or edit above code) to test your code in parts 1-10.
        maze.solveMaze();
        
        // 13
        System.out.println(maze.getPath());
        
        // 14
        maze.clearMaze();
        
        // 15
        maze.followPath();
        
        // 16
        // maze.insSortIn();
        // System.out.println(maze.getPath());
        
        // 17
        maze.createDirectPath();
        
        // 18
        System.out.println(maze.getDirectPath());
        
        // 19
        maze.followDirectPath();
    }
}

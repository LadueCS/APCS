/* AMazing.java 
 * 
 *  APCS Labs 2011-2021
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 */

import java.util.*;

public class AMazing 
{
	private int N;                      // dimension for maze with N x N cells - with boundary (N+2)x(N+2)
	private boolean[][] cellNorthWall;  // existence of wall to the north of cell(i,j)
	private boolean[][] cellEastWall;   // existence of wall to the east of cell(i,j)
	private boolean[][] cellSouthWall;  // existence of wall to the south of cell(i,j)
	private boolean[][] cellWestWall;   // existence of wall to the west of cell(i,j)
	private boolean[][] cellVisited;    // has cell(i,j) been visited yet in the maze generation
	private boolean done = false;
	
	private List<String> path = new ArrayList<String>();
	private List<String> directPath = new ArrayList<String>();
	
	public List<String> getPath() {
        return path;
    }
    
    public void clearMaze() {
        for (String s : path) {
            Scanner sc = new Scanner(s);
            sc.useDelimiter("\\D+");
            int i = sc.nextInt(), j = sc.nextInt();
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.filledCircle(i + 0.5, j + 0.5, 0.3);
            StdDraw.show(30);
        }
    }
    
    public void followPath() {
        for (String s : path) {
            Scanner sc = new Scanner(s);
            sc.useDelimiter("\\D+");
            int i = sc.nextInt(), j = sc.nextInt();
            StdDraw.setPenColor(StdDraw.CYAN);
            StdDraw.filledCircle(i + 0.5, j + 0.5, 0.25);
            StdDraw.show(30);
        }
    }
	
	public void insSortIn() {
        int n = path.size();
        for (int i = 0; i < n; ++i) {
            for (int j = i+1; j < n; ++j) {
                if (path.get(j).compareTo(path.get(i)) < 0) {
                    String tmp = path.get(i);
                    path.set(i, path.get(j));
                    path.set(j, tmp);
                }
            }
        }
	}
	
	public void createDirectPath() {
        int idx = 0, n = path.size();
        while (idx < n) {
            idx = path.lastIndexOf(path.get(idx));
            directPath.add(path.get(idx));
            ++idx;
        }
	}
	
	public List<String> getDirectPath() {
        return directPath;
	}
	
	public void followDirectPath() {
        for (String s : directPath) {
            Scanner sc = new Scanner(s);
            sc.useDelimiter("\\D+");
            int i = sc.nextInt(), j = sc.nextInt();
            StdDraw.setPenColor(StdDraw.MAGENTA);
            StdDraw.filledCircle(i + 0.5, j + 0.5, 0.25);
            StdDraw.show(30);
        }
    }
	
	public AMazing(int N) 
	{
	    this.N = N;
	    StdDraw.setXscale(0, N+2);
	    StdDraw.setYscale(0, N+2);
	    initialize();
	    // 5. After completing 1-4 below, insert appropriate code here.
	    createMaze();
	}

	private void initialize() 
	{
	    // Initialize border cells as already visited since these (dummy) boundary neighbors
		// will need to have boolean values in order for the createMaze method to be able
		// to easily refer to and check all neighbors of each cell. Actual maze indexes
		// run from 1 to N.
	    cellVisited = new boolean[N+2][N+2];
	    for (int i = 0; i < N+2; i++) 
	    { 
	    	cellVisited[i][0] = cellVisited[i][N+1] = true;
	    }
	    
	    for (int j = 0; j < N+2; j++)
	    {	
	    	cellVisited[0][j] = cellVisited[N+1][j] = true;
	    }

	    // initialze all walls as present
	    cellNorthWall = new boolean[N+2][N+2];
	    cellEastWall  = new boolean[N+2][N+2];
	    cellSouthWall = new boolean[N+2][N+2];
	    cellWestWall  = new boolean[N+2][N+2];
	    for (int i = 0; i < N+2; i++)
	    {
	         for (int j = 0; j < N+2; j++)
	         {
	                cellNorthWall[i][j] = cellEastWall[i][j] = cellSouthWall[i][j] = cellWestWall[i][j] = true;
	         }
	    }
	}

    // create the maze starting from cell(1,1) as base case of recursion
    private void createMaze() 
    {
    	createMaze(1, 1);
    }

	// recursively create the maze with random wall placement
	private void createMaze(int i, int j) 
	{
	    cellVisited[i][j] = true;

	    // while cell(i,j) has an unvisited neighbor 
	    while (!cellVisited[i][j+1] || !cellVisited[i+1][j] ||
	    	   !cellVisited[i][j-1] || !cellVisited[i-1][j]) 
	    {
	         
	         double r = Math.random();
	         if (r < 0.25 && !cellVisited[i][j+1]) 
	         {
	        	// 1. Insert appropriate code here to kill some walls
	        	//    and recursively call this method again.
	        	cellNorthWall[i][j] = cellSouthWall[i][j+1] = false;
	        	createMaze(i, j+1);
	         }  
	         else if (0.25 <= r && r < 0.50 && !cellVisited[i+1][j]) 
	         {
	        	// 2. Insert appropriate code here to kill some walls
		        //    and recursively call this method again.
                cellEastWall[i][j] = cellWestWall[i+1][j] = false;
	        	createMaze(i+1, j);
	         }
	         else if (0.5 <= r && r < 0.75 && !cellVisited[i][j-1]) 
	         {
	        	// 3. Insert appropriate code here to kill some walls
		        //    and recursively call this method again.
                cellSouthWall[i][j] = cellNorthWall[i][j-1] = false;
	        	createMaze(i, j-1);
	         }
	         else if (0.75 <= r && r < 1.00 && !cellVisited[i-1][j]) 
	         {
	        	// 4. Insert appropriate code here to kill some walls
		        //    and recursively call this method again.
                cellWestWall[i][j] = cellEastWall[i-1][j] = false;
	        	createMaze(i-1, j);
	        	 
	         }
	    }
	}

    // solve the maze starting from the start state as base 
    // case of recursion and depth-first search
    public void solveMaze() 
    {
        for (int i = 1; i <= N; i++)
        {
            for (int j = 1; j <= N; j++)
            {
                cellVisited[i][j] = false;  // re-initialize all internal maze cells as unvisited
            }
        }
        done = false;
        solveMaze(1, 1);
    }
	
    // solve the maze using recursive depth-first search
    private void solveMaze(int i, int j) 
    {
        if (i == 0 || j == 0 || i == N+1 || j == N+1) return;
        if (done || cellVisited[i][j]) return;
        cellVisited[i][j] = true;

        StdDraw.setPenColor(StdDraw.BLUE);
        StdDraw.filledCircle(i + 0.5, j + 0.5, 0.25);
        StdDraw.show(30);

        // reached middle
        if (i == N/2 && j == N/2) done = true;

        // Only after completing and testing parts 1-6:
        // 7. Insert code to recursively explore an appropriate direction.
        path.add("("+i+","+j+")");
        if (!cellNorthWall[i][j]) solveMaze(i, j+1);
        if (done) return;
        
        // 8. Insert code to recursively explore an appropriate direction.
        path.add("("+i+","+j+")");
        if (!cellEastWall[i][j]) solveMaze(i+1, j);
        if (done) return;
        
        // 9. Insert code to recursively explore an appropriate direction.
        path.add("("+i+","+j+")");
        if (!cellSouthWall[i][j]) solveMaze(i, j-1);
        if (done) return;
        
        // 10. Insert code to recursively explore an appropriate direction.
        path.add("("+i+","+j+")");
        if (!cellWestWall[i][j]) solveMaze(i-1, j);
        if (done) return;

        StdDraw.setPenColor(StdDraw.GRAY);
        StdDraw.filledCircle(i + 0.5, j + 0.5, 0.25);
        StdDraw.show(30);
    }
    
    // draw the maze without a trace
    public void drawMaze() 
    {
    	drawMaze(false);
    }

    // draw the maze with or without a trace
    public void drawMaze(boolean trace) 
    {
        int delay;
        if(trace)
        {
           delay = 50;
           StdDraw.setXscale(0, N+1);
   	       StdDraw.setYscale(0, N+1);
           StdDraw.setPenColor(StdDraw.BLUE);
	       StdDraw.line(0, 0, 0, N+1);
	       StdDraw.line(0, N+1, N+1, N+1);
	       StdDraw.line(N+1, N+1, N+1,0);
	       StdDraw.line(N+1, 0, 0, 0);   
	       StdDraw.textLeft(0, 0.5, "All border walls (i = 0 or N+1, and j = 0 or N+1) shown intially in blue.");
	       StdDraw.setPenColor(StdDraw.BLACK);
        }    
        else
        {
           delay = 0;
        }
        
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.filledCircle(N/2 + 0.5, N/2 + 0.5, 0.375);
        StdDraw.filledCircle(1.5, 1.5, 0.375);
        StdDraw.setPenColor(StdDraw.BLACK);
        
        for (int i = 1; i < N+1; i++) 
        {
            for (int j = 1; j < N+1; j++)
            {
            	
            	String walls = "";
                if (cellSouthWall[i][j]) 
                {	
                	walls += "S";
                	// draw south wall using cell i, j indexes as coordinates of SW corner of cell
                	StdDraw.line(i, j, i + 1, j);
                	StdDraw.show(delay); 
                }
                if (cellNorthWall[i][j]) 
                {	
                	walls += "N";
                	// draw north wall using cell i, j indexes as coordinates of SW corner of cell
                	StdDraw.line(i, j + 1, i + 1, j + 1);
                	StdDraw.show(delay); 
                }
                if (cellWestWall[i][j])
                {	
                	walls += "W";
                	// draw west wall using cell i, j indexes as coordinates of SW corner of cell
                	StdDraw.line(i, j, i, j + 1);
                	StdDraw.show(delay); 
                }
                if (cellEastWall[i][j])  
                {	
                	walls += "E";
                	// draw east wall using cell i, j indexes as coordinates of SW corner of cell
                	StdDraw.line(i + 1, j, i + 1, j + 1);	                	
                	StdDraw.show(delay); 
                }
                if(trace)
                {
                   // draw point at SW corner of cell using cell i, j indexes as coordinates of this point
                   StdDraw.setPenRadius(.01);
                   StdDraw.point(i, j);
                   StdDraw.setPenRadius();
                   // draw text for point at SW corner
                   StdDraw.text(i, j , "          (" + i + "," + j + ")");
                   // draw text coding walls around cell(i,j)
                   StdDraw.text(i + 0.5, j + 0.65, walls);
                   StdDraw.text(i + 0.5, j + 0.4, "cell(" + i + "," + j + ")");
                }
            }
        }
        StdDraw.show(1000);
    }

}

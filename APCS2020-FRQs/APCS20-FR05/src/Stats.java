/**********************************************************************************************
   ** NOTE that this question is in the file ap09frq_computer_science_a.pdf.                               **
   ********************************************************************************************
   AP2009 A Exam: Free Response Question 1.(a), and 1.(b).
   In this class create the following static methods: getCubeTosses,
   and getLongestRun. 
   Note that in a class that you want to use as an archive of methods
   (and don't need to create objects of the class) it is useful to
   use the static parameter in all the methods. Then you don't have
   to create an object when you invoke a method, instead you can
   just use Stats.<method name>. See the Main class for examples.
**/

public class Stats 
{
	
	public static int[] getCubeTosses(NumberCube cube, int numTosses)
	{
		
		int[] ret = new int[numTosses];
		for (int i = 0; i < numTosses; ++i) ret[i] = cube.toss();
		return ret;
	}
	
	public static int longestRun(int[] nums)
	{
		
		//insert your code here
		int cur = nums[0], len = 0, max = 1, idx = -1;
		for (int i = 1; i < nums.length; ++i) {
            if (nums[i-1] == nums[i]) {
                ++len;
                if (len > max) {
                    idx = i-len+1;
                    max = len;
                }
            }
            else len = 1;
		}
		return idx;
	}		

}

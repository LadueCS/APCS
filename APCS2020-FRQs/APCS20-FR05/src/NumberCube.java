
public class NumberCube 
{
    private int faceUp;
    
    public NumberCube()
    {
    	faceUp = toss();
    }
	
    public int toss()
    {
    	return (int) Math.ceil((6*Math.random()));
    }
}

/**********************************************************************************************
   ** NOTE that this question is in the file ap09frq_computer_science_a.pdf.                               **
   ********************************************************************************************
   AP2009 A Exam: Free Response Question 1.(a), and 1.(b).
   In the Stats class create the following static methods: getCubeTosses,
   and getLongestRun.
**/

import java.util.Arrays;

public class Main 
{
	public static void main(String[] args) 
	{
		
        NumberCube cube = new NumberCube();
        // Uncomment these lines below to test your methods.
        // Ten runs
        for (int i = 0; i < 10; i++)
        {
           int[] tosses = Stats.getCubeTosses(cube, 18);
           System.out.println("tosses = " + Arrays.toString(tosses));
           System.out.println("longest run index = " + Stats.longestRun(tosses));
        }
        
		
	}

}

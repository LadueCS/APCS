import java.util.*;

public class Main {
    public static void main(String args[]) {
        List<LogMessage> L = Arrays.asList(new LogMessage("test:abcd"), new LogMessage("hello:world"));
        SystemLog s = new SystemLog(L);
        s.print();
        s.removeMessages("abcd");
        s.print();
    }
}

public class LogMessage {
    private String machineId;
    private String description;
    
    public LogMessage(String message) {
        int idx = message.indexOf(":");
        machineId = message.substring(0, idx);
        description = message.substring(idx+1);
    }
    
    public boolean containsWord(String keyword) {
        return description.indexOf(keyword) != -1;
    }
    
    public String getMachineId() {
        return machineId;
    }
    
    public String getDescription() {
        return description;
    }
}


public class SystemLog {
    private List<LogMessage> messageList;
    
    public SystemLog(List<LogMessage> L) {
        messageList = L;
    }
    
    public List<LogMessage> removeMessages(String keyword) {
        List<LogMessage> ret = new ArrayList<LogMessage>();
        for (var m : messageList) {
            if (!m.containsWord(keyword)) ret.add(m);
        }
        messageList = ret;
        return ret;
    }
    
    public void print() {
        for (var m : messageList) {
            System.out.println(m.getMachineId() + " " + m.getDescription());
        }
    }
}

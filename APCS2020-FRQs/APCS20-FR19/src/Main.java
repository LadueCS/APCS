
import java.util.*;

public class Main 
{
	public static void main(String[] args) 
	{
		// Part (a). Test your constructor.
		TokenPass tp = new TokenPass(7);
        System.out.println("initial state of board = " + Arrays.toString(tp.getBoard()));
        System.out.println("current player = " + tp.getCurrentPlayer());
        
        // Part (b). Test your method.
        tp.distributeCurrentPlayerTokens();
        System.out.println("state of board = " + Arrays.toString(tp.getBoard()));
        System.out.println("current player = " + tp.getCurrentPlayer());
	}

}

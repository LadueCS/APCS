import java.util.*;

public class TokenPass 
{
	private int[] board;
	private int currentPlayer;
	private int playerCount;

	// Part (a).
	public TokenPass(int playerCount) 
	{
		// insert your code here
		// solve this problem yourself - don't copy from another source
		this.playerCount = playerCount;
		board = new int[playerCount];
		for (int i = 0; i < playerCount; ++i) board[i] = (int)(10*Math.random())+1;
		currentPlayer = (int)(playerCount*Math.random());
	}
	
	// Part (b).
	public void distributeCurrentPlayerTokens()
	{
		// insert your code here
		// solve this problem yourself - don't copy from another source
		int t = board[currentPlayer];
		board[currentPlayer] = 0;
		for (int i = 1; i <= t; ++i) ++board[(i+currentPlayer)%playerCount];
	}
	
	public int[] getBoard()
	{
		return board;
	}
	
	public int getCurrentPlayer()
	{
		return currentPlayer;
	}
	
}

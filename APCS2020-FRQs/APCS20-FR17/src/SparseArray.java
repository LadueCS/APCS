import java.util.*;

public class SparseArray 
{
	private int numRows;
	private int numCols;
	private List<SparseArrayEntry> entries;
	
	public SparseArray(int numRows, int numCols, ArrayList<SparseArrayEntry> entries)
	{
		this.numRows = numRows;
		this.numCols = numCols;
		this.entries = entries;
	}
	
	// Part (a).
	public int getValueAt(int row, int col)
	{
		for (SparseArrayEntry entry : entries) {
            if (entry.getRow() == row && entry.getCol() == col) return entry.getValue();
		}
		return 0;
	}
	
	// Part (b).
	public void removeCol(int col)
	{
        ArrayList<SparseArrayEntry> tmp = new ArrayList<SparseArrayEntry>();
		for (SparseArrayEntry entry : entries) {
            if (entry.getCol() < col) tmp.add(entry);
            else if (entry.getCol() > col)
                tmp.add(new SparseArrayEntry(entry.getRow(), entry.getCol()-1, entry.getValue()));
		}
		entries = tmp;
		--numCols;
	}
	
	// Part (c).
	public void removeRow(int row)
	{
		ArrayList<SparseArrayEntry> tmp = new ArrayList<SparseArrayEntry>();
		for (SparseArrayEntry entry : entries) {
            if (entry.getRow() < row) tmp.add(entry);
            else if (entry.getRow() > row)
                tmp.add(new SparseArrayEntry(entry.getRow()-1, entry.getCol(), entry.getValue()));
		}
		entries = tmp;
		--numRows;
	}
	
	// Part (d). This method should work not only for a square matrix, 
	// but also for a rectangular one. So, you have to use a definition
	// of "main diagonal" that works for a rectangular matrix too.
	public boolean hasZeroDiagonal(SparseArray sparse)
	{
		for (SparseArrayEntry entry : entries)
            if (entry.getRow() == entry.getCol() && entry.getValue() != 0) return false;
        return true;
	}
	
	// Part (e). Bonus! There are possible issues in the way a SparseArray is constructed
	// by the user. Identify these issues and fix them with appropriate method(s), etc.
	
	// insert your code here
	
	
	public int getNumRows()
	{
		return numRows;
	}
	
	public int getNumCols()
	{
		return numCols;
	}
	
	public List<SparseArrayEntry> getEntries()
	{
		return entries;
	}

}

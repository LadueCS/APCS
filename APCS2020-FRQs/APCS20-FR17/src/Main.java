
import java.util.*;

public class Main 
{
	public static void main(String[] args) 
	{
		ArrayList<SparseArrayEntry> entries = new ArrayList<SparseArrayEntry>();
		entries.add(new SparseArrayEntry(1,4,4));
		entries.add(new SparseArrayEntry(2,0,1));
		entries.add(new SparseArrayEntry(3,1,-9));
		entries.add(new SparseArrayEntry(1,1,5));
		System.out.println("\nentries = " + entries);
		
		// Test Part (a).
		SparseArray sparse = new SparseArray(6,5,entries);
		System.out.println("\nTest Part (a).");
		
		System.out.println("sparse.getEntries() = " + sparse.getEntries());
		System.out.println("sparse.getValueAt(1,4) = " + sparse.getValueAt(1,4));
		System.out.println("sparse.getValueAt(2,0) = " + sparse.getValueAt(2,0));
		System.out.println("sparse.getValueAt(3,1) = " + sparse.getValueAt(3,1));
		System.out.println("sparse.getValueAt(1,1) = " + sparse.getValueAt(1,1));
		System.out.println("sparse.getValueAt(3,4) = " + sparse.getValueAt(3,4));
		
		
		// Test Parts (b) and (c).
		System.out.println("\nTest Parts (b) and (c).");
		
		System.out.println("sparse.getEntries() = " + sparse.getEntries());
		System.out.println("sparse.getNumCols() = " + sparse.getNumCols());
		sparse.removeCol(1);
		System.out.println("after sparse.removeCol(1) = " + sparse.getEntries());
		System.out.println("sparse.getNumCols() = " + sparse.getNumCols());
		System.out.println("sparse.getNumRows() = " + sparse.getNumRows());
		sparse.removeRow(2);
		System.out.println("after sparse.removeRow(2) = " + sparse.getEntries());
		System.out.println("sparse.getNumRows() = " + sparse.getNumRows());
		
		
		// Test Part (d).
		ArrayList<SparseArrayEntry> entries2 = new ArrayList<SparseArrayEntry>();
		entries2.add(new SparseArrayEntry(2,2,0));
		entries2.add(new SparseArrayEntry(4,4,0));
		entries2.add(new SparseArrayEntry(0,0,0));
		entries2.add(new SparseArrayEntry(1,1,0));
		System.out.println("\nTest Part (d).");
		
		System.out.println("entries2 = " + entries2);
		SparseArray sparse2 = new SparseArray(5,6,entries2);
		System.out.println("\nsparse2.getEntries() = " + sparse2.getEntries());
		System.out.println("sparse2.hasZeroDiagonal(sparse2) = " + sparse2.hasZeroDiagonal(sparse2));
		entries2.add(new SparseArrayEntry(3,3,-1));
		System.out.println("\nsparse2.getEntries() = " + sparse2.getEntries());
		System.out.println("sparse2.hasZeroDiagonal(sparse2) = " + sparse2.hasZeroDiagonal(sparse2));
		
		
		// Test Part (e) - Create your own testing code below.
		System.out.println("\nTest Part (e).");
		
		
		
	}

}

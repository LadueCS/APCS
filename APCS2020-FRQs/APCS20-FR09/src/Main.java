/**********************************************************************************************
   ** NOTE that this question is in the file ap10frq_computer_science_a.pdf.                               **
   ********************************************************************************************
   AP2010 A Exam: Free Response Question 2.
   Create the APline class according to the instructions and create the following 
   methods: getSlope, and isOnLine.
**/

public class Main 
{
	public static void main(String[] args) 
	{
		// Uncomment the code below to test your methods
		/***
		APLine line1 = new APLine(5,4,-17);
		double slope1 = line1.getSlope();
		boolean onLine1 = line1.isOnLine(5, -2);
		System.out.println("\nslope1 = " + slope1);
		System.out.println("onLine1 = " + onLine1);
		
		APLine line2 = new APLine(-25,40,-30);
		double slope2 = line2.getSlope();
		boolean onLine2 = line2.isOnLine(5, -2);
		System.out.println("\nslope2 = " + slope2);
		System.out.println("onLine2 = " + onLine2);
		***/
		
	}

}

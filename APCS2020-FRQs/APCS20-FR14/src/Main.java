import java.util.*;

public class Main 
{
	public static void main(String[] args) 
	{
		int[][] arr1 = {{255,184,178,84,129},
				        {84,255,255,130,84},
				        {78,255,0,0,78},
				        {84,130,255,130,84}
		               };
		/** Also, could do the following.
		int[][] arr1 = new int[4][5];
		arr1[0][0] = 255;
		arr1[0][1] = 184;
		arr1[0][2] = 178;
		arr1[0][3] = 84;
		arr1[0][4] = 129;
		arr1[1][0] = 84;
		arr1[1][1] = 255;
		arr1[1][2] = 255;
		arr1[1][3] = 130;
		arr1[1][4] = 84;
		arr1[2][0] = 78;
		arr1[2][1] = 255;
		arr1[2][2] = 0;
		arr1[2][3] = 0;
		arr1[2][4] = 78;
		arr1[3][0] = 84;
		arr1[3][1] = 130;
		arr1[3][2] = 255;
		arr1[3][3] = 130;
		arr1[3][4] = 84;
		**/
		// Note that arr1 is an array of arrays which we think of as the
		// rows of a matrix. So, the number of rows are give as follows. 
		int rows = arr1.length;
		// To get the number of columns of this matrix we can just 
		// find the length of one of the rows which is an array inside
		// the array of arrays.
		int cols = arr1[0].length;
		System.out.println("number of rows = " + rows);
		System.out.println("number of cols = " + cols);
		System.out.println("arr1:");
		for (int i = 0; i < rows; i++)
		{
			System.out.println(Arrays.toString(arr1[i]));
		}
		
		//uncomment the code below to test your methods
        
		GrayImage gi1 = new GrayImage(arr1);
		System.out.println("arr1: number of white pixels = " + gi1.countWhitePixels());
		
		int[][] arr2 = {{221,184,178,84,135},
		                {84,255,255,130,84},
		                {78,255,0,0,78},
		                {84,130,255,130,84}
                       };
		System.out.println("\narr2:");
		for (int i = 0; i < rows; i++)
		{
			System.out.println(Arrays.toString(arr2[i]));
		}
		 
		GrayImage gi2 = new GrayImage(arr2);
		gi2.processImage();
		System.out.println("\narr2 after processing:");
		for (int i = 0; i < rows; i++)
		{
			System.out.println(Arrays.toString(arr2[i]));
		}
		
	}

}

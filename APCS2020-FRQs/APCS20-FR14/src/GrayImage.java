
public class GrayImage 
{
	public static final int BLACK = 0;
	public static final int WHITE = 255;
	private int[][] pixelValues;
	
	public GrayImage(int[][] pixelValues)
	{
		this.pixelValues = pixelValues;
	}
	
	public int countWhitePixels()
	{
        int ret = 0;
		for (int i = 0; i < pixelValues.length; ++i)
            for (int j = 0; j < pixelValues[i].length; ++j)
                if (pixelValues[i][j] == WHITE) ++ret;
        return ret;
	}
	
	public void processImage()
	{
		// insert your code here
		
		for (int i = 0; i < pixelValues.length-2; ++i)
            for (int j = 0; j < pixelValues[i].length-2; ++j)
                pixelValues[i][j] = (pixelValues[i][j]-pixelValues[i+2][j+2] < BLACK ? BLACK : pixelValues[i][j]-pixelValues[i+2][j+2]);
	}
	
	
	
	
	
}

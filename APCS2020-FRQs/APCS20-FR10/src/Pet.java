
// abstract classes are extended but not instantiated.
public abstract class Pet
{
  // private instance variable.
  private String myName;

  // constructor
  public Pet(String name) { myName = name; }

  // method
  public String getName() { return myName; }

  // abstract methods have no implementation defined, and no return statement.
  public abstract String speak();
}

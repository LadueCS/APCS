/**********************************************************************************************
   ** NOTE that this question is in the file AP2004A_FRQ2.pdf.                               **
   ********************************************************************************************
   AP2004 A Exam: Free Response Question 2.(a), 2.(b), and 2.(c).
   Create Cat class, LoudDog class, and allSpeak method in Kennel class.
**/

public class JBGeneric
{

  public static void main(String[] args)
  {

    Kennel kennel = new Kennel();
    kennel.add(new Dog("Rosie"));

    System.out.println("\nTesting Answers to Question 2.(a), 2.(b), and 2.(c):\n");

    // Uncommenting the following should work after you create the Cat class:
    //kennel.add(new Cat("Tommy"));

    // Uncommenting the following should work after you create the LoudDog class:
    //kennel.add(new LoudDog("Barker"));

    // Uncommenting the following should work after you create the allSpeak method:
    //kennel.allSpeak();
  }
}







// Dog is defined as a subclass of Pet, Pet is a superclass of Dog
public class Dog extends Pet
{

  // constructor that uses the superclass's, i.e. Pet's, constructor
  public Dog(String name)
  {
    super(name);
  }

  // method defined for this class only
  public String speak()
  {
    return "woof";
  }
}


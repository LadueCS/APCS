/**********************************************************************************************
   ** NOTE that this question is in the file AP2005A_FRQ4.pdf.                               **
   ********************************************************************************************
   AP2005 A Exam: Free Response Question 4.(a), 4.(b), and 4.(c).
   In the StudentRecord class create the following methods: average,
   hasImproved, and finalAverage.
**/

public class JBGeneric
{

  public static void main(String[] args)
  {

    int scores1[] = {50, 50, 20, 80, 53};
    int scores2[] = {20, 50, 50, 53, 80};
    int scores3[] = {20, 50, 50, 80};

    System.out.println("\nTesting Answers to Question 4.(a), 4.(b), and 4.(c):\n");
    // Uncommenting the following should work after you create all three methods:
    
    System.out.println("scores1 Improved-Ave = " + (new StudentRecord(scores1)).finalAverage());
    System.out.println("scores2 Improved-Ave = " + (new StudentRecord(scores2)).finalAverage());
    System.out.println("scores3 Improved-Ave = " + (new StudentRecord(scores3)).finalAverage());
    System.out.println();
    
  }
}






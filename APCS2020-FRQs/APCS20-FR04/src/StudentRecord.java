
public class StudentRecord
{
  // private instance variable
  private int[] scores; // contains scores.length values
                        // scores.length > 1 (see Question 4.(b) below)

  // constructor
  public StudentRecord(int[] nums)
  {
    scores = new int[nums.length];
    for (int k = 0; k < nums.length; k++)
      scores[k] = nums[k];
  }

  // Question 4.(a)

  // Returns the average (arithmetic mean) of the values in scores
  // whose subscripts are between first and last inclusive.
  // Precondition:  0 <= first <= last < scores.length.
  // Note that this means that the average method must be able
  // to compute the average of any subsequence of (part of) the
  // complete list of scores.
  private double average(int first, int last) {
    double sum = 0;
    for (int i = first; i <= last; ++i) sum += scores[i];
    return sum/(last-first+1);
  }

  // Insert your code here.

  // Question 4.(b)

  // Returns true if each successive value in scores is greater
  // than or equal to the previous value.
  // Otherwise, it returns false.
  // Precondition: scores.length > 1, i.e. there are at least two scores,
  // and so both scores[0] and scores[1] exist.

  // Insert your code here.
  private boolean hasImproved() {
    for (int i = 1; i < scores.length; ++i) if (scores[i-1] > scores[i]) return false;
    return true;
  }

  // Question 4.(c)

  // if the values in scores have improved, returns the average
  // of the elements in scores with indexes greater than or equal
  // to scores.length/2;
  // Otherwise, returns the average of all of the values in scores.

  // Insert your code here.
    public double finalAverage() {
        if (hasImproved()) return average(scores.length/2, scores.length-1);
        else return average(0, scores.length-1);
    }
}

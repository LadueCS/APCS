// Note that NumberTile is a subclass of Ntuple.

public class NumberTile extends Ntuple
{
	public NumberTile(int top, int right, int bottom, int left)
	{
		super(top,right,bottom,left);
	}

	public int getTop()
	{
		return getTop(this);
	}
	
	public int getRight()
	{
		return getRight(this);
	}
	
	public int getBottom()
	{
		return getBottom(this);
	}
	
	public int getLeft()
	{
		return getLeft(this);
	}



	// Rotate clockwise by 90, i.e. perform a cyclic
    // permutation of this NumberTile.
	public NumberTile rotate()
	{
		return rotateTile(this);
	}
	
	// Rotate clockwise by 90, i.e. perform a cyclic
	// permutation of the NumberTile.
	public NumberTile rotateTile(NumberTile tile)
	{
		NumberTile rotate = tile.cloneTile();
		for(int i = 0; i < tile.getN(); i++)
		{
			rotate.setkth((i+1)%4, tile.getkth(i));
		}
		return rotate;
	}

	public NumberTile cloneTile()
	{
		return cloneTile(this);
	}
	
	public NumberTile cloneTile(NumberTile tile)
	{
		return new NumberTile(tile.getTop(),tile.getRight(),tile.getBottom(),tile.getLeft());
	}
	
	public int getTop(NumberTile tile)
	{
		return (Integer)tile.getkth(0);
	}
	
	public int getRight(NumberTile tile)
	{
		return (Integer)tile.getkth(1);
	}
	
	public int getBottom(NumberTile tile)
	{
		return (Integer)tile.getkth(2);
	}
	
	public int getLeft(NumberTile tile)
	{
		return (Integer)tile.getkth(3);
	}
}

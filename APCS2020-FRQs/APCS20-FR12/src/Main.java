/**********************************************************************************************
   ** NOTE that this question is in the file ap10frq_computer_science_a.pdf.                               **
   ********************************************************************************************
   AP2010 A Exam: Free Response Question 4.
   In the TileGame class create the following methods: getIndexForFit, and insertTile.
   Note that NumberTile is a subclass of Ntuple.
**/

import java.util.*;

public class Main 
{
	public static void main(String[] args) 
	{
		ArrayList<NumberTile> board = new ArrayList<NumberTile>();
		board.add(new NumberTile(4,3,7,4));
		board.add(new NumberTile(6,4,3,3));
		board.add(new NumberTile(1,2,3,4));
		board.add(new NumberTile(3,2,5,2));
		board.add(new NumberTile(5,9,2,2));
		System.out.println("\nboard = " + board);
		
		NumberTile tile1 = new NumberTile(4,2,9,2);
		NumberTile tile2 = new NumberTile(4,2,9,8);
		NumberTile tile3 = new NumberTile(1,5,9,2);
		NumberTile tile4 = new NumberTile(3,1,3,1);
		
		TileGame game = new TileGame(board);
		System.out.println("board alignment = " + game.boardAlignment());
		
		// Uncomment this code to test your getIndexForFit method.
		
		System.out.println("\ntile1 = " + tile1 + ", aligment = " + game.tileAlignment(tile1) +", Fit index tile1 = " + game.getIndexForFit(tile1));
		System.out.println("tile2 = " + tile2 + ", aligment = " + game.tileAlignment(tile2) +", Fit index tile2 = " + game.getIndexForFit(tile2));
		System.out.println("tile3 = " + tile3 + ", aligment = " + game.tileAlignment(tile3) +", Fit index tile3 = " + game.getIndexForFit(tile3));
		System.out.println("tile4 = " + tile4 + ", aligment = " + game.tileAlignment(tile4) +", Fit index tile4 = " + game.getIndexForFit(tile4));
		
		
		// Uncomment this code to test your insertTile method.
		
		int index;
		index = game.getIndexForFit(tile1);
		game.insertTile(tile1);
		System.out.println("\ngame.insertTile(tile1)");
		System.out.println("board = " + game.getBoard());
		System.out.println("board alignment = " + game.boardAlignment());
		System.out.println("tile1 = " + tile1 + ", aligment = " + game.tileAlignment(tile1) +", Fit index tile1 = " + index);
		
		index = game.getIndexForFit(tile2);
		game.insertTile(tile2);
		System.out.println("\ngame.insertTile(tile2)");
		System.out.println("board = " + game.getBoard());
		System.out.println("board alignment = " + game.boardAlignment());
		System.out.println("tile2 = " + tile2 + ", aligment = " + game.tileAlignment(tile2) +", Fit index tile2 = " + index);
		
		index = game.getIndexForFit(tile3);
		game.insertTile(tile3);
		System.out.println("\ngame.insertTile(tile3)");
		System.out.println("board = " + game.getBoard());
		System.out.println("board alignment = " + game.boardAlignment());
		System.out.println("tile3 = " + tile3 + ", aligment = " + game.tileAlignment(tile3) +", Fit index tile3 = " + index);
		
		index = game.getIndexForFit(tile4);
		game.insertTile(tile4);
		System.out.println("\ngame.insertTile(tile4)");
		System.out.println("board = " + game.getBoard());
		System.out.println("board alignment = " + game.boardAlignment());
		System.out.println("tile4 = " + tile4 + ", aligment = " + game.tileAlignment(tile4) +", Fit index tile4 = " + index);
		
	}

}

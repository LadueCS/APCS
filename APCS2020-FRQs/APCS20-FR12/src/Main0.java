import java.util.*;

public class Main0 
{

	public static void main(String[] args) 
	{
		// Example use of Ntuple class
		Ntuple ntuple1 = new Ntuple(0,1,2,3,4);
		System.out.println("ntuple1 = " + ntuple1);
		System.out.println("ntuple1.getkth(0) = " + ntuple1.getkth(0));
		
		Ntuple ntuple2 = new Ntuple("aa","bb","cc","dd");
		System.out.println("\nntuple2 = " + ntuple2);
		System.out.println("ntuple2.getkth(1) = " + ntuple2.getkth(1));
		
		Ntuple ntuple3 = new Ntuple(0,"aa",1,"bb",2,"cc",3,"dd");
		System.out.println("\nntuple3 = " + ntuple3);
		System.out.println("ntuple3.getkth(2) = " + ntuple3.getkth(2));
		
		ArrayList<Ntuple> ntuple4arr = new ArrayList<Ntuple>();
		ntuple4arr.add(ntuple1);
		ntuple4arr.add(ntuple2);
		ntuple4arr.add(ntuple3);
		System.out.println("\nntuple4arr = " + ntuple4arr);
		System.out.println("ntuple4arr.get(2).getkth(2) = " + ntuple4arr.get(2).getkth(2));
		
		Ntuple ntuple5 = new Ntuple(ntuple1,ntuple2,ntuple3,ntuple4arr);
		System.out.println("\nntuple5 = " + ntuple5);
		System.out.println("ntuple5.getkth(3) = " + ntuple5.getkth(3));
		
		
		
	}

}

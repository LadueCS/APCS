/**********************************************************************************************
   ** NOTE that this question is in the file ap10frq_computer_science_a.pdf.  
                             **
   ********************************************************************************************
   AP2010 A Exam: Free Response Question 4.
   In the TileGame class create the following methods: getIndexForFit, and insertTile.
   Note that NumberTile is a subclass of Ntuple.
**/

import java.util.ArrayList;

public class TileGame 
{
   private ArrayList<NumberTile> board;
   
   public TileGame(ArrayList<NumberTile> board)
   {
	   this.board = board;
   }
	
   public int getIndexForFit(NumberTile tile)
   {
        if (board.size() == 0) return 0;
        if (tile.getRight() == board.get(0).getLeft()) return 0;
        for (int i = 1; i < board.size(); ++i)
            if (tile.getLeft() == board.get(i-1).getRight() && tile.getRight() == board.get(i).getLeft()) return i;
        if (tile.getLeft() == board.get(board.size()-1).getRight()) return board.size();
        return -1;
	}
   
    public boolean insertTile(NumberTile tile)
    {
       NumberTile next = tile.cloneTile();
	   int index = getIndexForFit(next);
	   
	   // insert your code here - you must use the two variables defined above
	   for (int d = 0; d < 4; ++d) {
            if (index != -1) {
                board.add(index, next);
                return true;
            }
            next = next.rotate();
            index = getIndexForFit(next);
	   }
	   return false;
    }
    
    public ArrayList<NumberTile> getBoard()
    {
    	return board;
    }
    
    public String tileAlignment(NumberTile tile)
    {
    	return tile.getLeft() + "-L,R-" + tile.getRight();
    }
    
    public String boardAlignment()
    {
    	String alignment = "";
    	for(int i = 0; i < board.size(); i++)
    	{
    		alignment +=  tileAlignment(board.get(i)) + " ";
    	}
    	return alignment;
    }
	
}

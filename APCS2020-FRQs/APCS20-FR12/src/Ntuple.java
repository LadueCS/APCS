/********  Ntuple.java *********
 *  
 *  APCS Project 2011-16
 *  Computational Biology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *
 *  Ntuple is a universal data type that creates an
 *  ordered ntuple of java Objects. Since every data type 
 *  created by a class is a subclass of the Object class,
 *  the Ntuple class provides a data structure for combining
 *  (representing) any collection of heterogeneous data types, 
 *  i.e. any collection of data objects into one Ntuple object.
 */

public class Ntuple 
{

	private Object[] objList;
	private int N;
	
	public Ntuple(Object... objList)
	{
		this.objList = objList;
		N = objList.length;
	}
	
	public int getN()
	{
		return N;
	}
	
	public Object getkth(int k)
	{
		if (k < N)
		   return objList[k];
		else
		   return null;
	}
	
	public void setkth(int k, Object obj)
	{
		if (k < N)
		{
			objList[k] = obj;
		}
	}
	
	public boolean equals(Ntuple tuple)
	{
		boolean itsEqual = true;
		
		for(int i = 0; i < N; i++)
		{
			itsEqual = itsEqual && (this.getkth(i).equals(tuple.getkth(i)));
			if(!itsEqual)
			   i = N;
		}
		return itsEqual;
	}
	
	public Ntuple clone()
	{
		Object[] copy = new Object[N];
		for(int i = 0; i < N; i++)
		{ 
			copy[i] = objList[i];
		}
		return new Ntuple(copy);
	}
	
	public String toString()
	{
		String vargs = "Ntuple(";
		for(int i = 0; i < N; i++)
		{  if (i < N-1)
			  vargs += getkth(i) + "," ;
		   else
			  vargs += getkth(i) + ")";
		}
		return vargs;
	}
	
}

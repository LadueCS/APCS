// Question 2.(a)

// Insert your code below:

// Advance is defined as a subclass of Ticket, Ticket is a superclass of Advance
public class Advance extends Ticket {

  // Private instance variable
    private int days;
    private double price;

  // Constructor
    public Advance(int d) {
        super();
        days = d; price = d < 10 ? 30 : 40;
    }

  // Method
    public double getPrice() {
        return price;
    }
    public void setPrice(double p) {
        price = p;
    }
}

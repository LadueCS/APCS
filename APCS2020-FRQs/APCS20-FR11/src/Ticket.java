
// Abstract classes are extended but not instantiated.
public abstract class Ticket
{
  // Private instance variable
  private int serialNumber;

  // Constructor
  public Ticket()
  {
    serialNumber = getNextSerialNumber();
  }

  // Abstract methods have no implementation defined, and no return statement.
  // Returns the price for this ticket.
  public abstract double getPrice();

  // Override toString method to return a string with information about the ticket.
  public String toString()
  {
    return "Serial Number = " + serialNumber + ", Price = $" + getPrice();
  }

  // Private instance variable
  private static int nextSerialNumber = 0;

  // Private method that returns a new unique serial number
  private static int getNextSerialNumber()
  {
    return nextSerialNumber++;
  }
}

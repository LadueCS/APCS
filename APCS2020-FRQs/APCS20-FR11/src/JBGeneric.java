/**********************************************************************************************
   ** NOTE that this question is in the file AP2005A_FRQ2.pdf.                               **
   ********************************************************************************************
   AP2005 A Exam: Free Response Question 2.(a) and 2.(b).
   Create Advance class and StudentAdvance class, including constructors and methods.
**/

public class JBGeneric
{

  public static void main(String[] args)
  {
    System.out.println("\nTesting Answers to Question 2.(a) and 2.(b):\n");

    // Uncommenting the following should work after you create the two classes:
    
    System.out.println(" 5 days in advance: " + new Advance(5));
    System.out.println(" 9 days in advance: " + new Advance(9));
    System.out.println("10 days in advance: " + new Advance(10));
    System.out.println("20 days in advance: " + new Advance(20));
    System.out.println();

    System.out.println(" 5 days in advance: " + new StudentAdvance(5));
    System.out.println(" 9 days in advance: " + new StudentAdvance(9));
    System.out.println("10 days in advance: " + new StudentAdvance(10));
    System.out.println("20 days in advance: " + new StudentAdvance(20));
    System.out.println();
    
  }
}






// Question 2.(b)

// Insert your code below:

// StudentAdvance is defined as a subclass of Advance, Advance is a superclass of StudentAdvance
public class StudentAdvance extends Advance {
    
  // Constructor that uses the superclass's, i.e. Advance's constructor.
    public StudentAdvance(int d) {
        super(d);
        super.setPrice(super.getPrice() / 2);
    }

  // Method that uses the superclass's, i.e. Advance's getPrice method.
    public double getPrice() {
        return super.getPrice();
    }

  // Method that overrides toString and uses the superclass's method, i.e. Advance's toString method.

}

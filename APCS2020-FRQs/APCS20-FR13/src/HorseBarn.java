
public class HorseBarn
{
	private Horse[] spaces;
	
	public HorseBarn(Horse[] spaces) 
	{
		this.spaces = spaces;
	}
	
	public int findHorseSpace(String name)
	{
		for (int i = 0; i < spaces.length; ++i) {
            if (spaces[i] != null && spaces[i].getName() == name) return i;
		}
		return -1;
	}
	
	public void consolidate()
	{
        for (int i = 0, j = 0; i < spaces.length; ++i) {
            if (spaces[i] != null) {
                spaces[j++] = spaces[i];
                spaces[i] = null;
            }
        }
        // insert your code here
	}
	
	public Horse[] getSpaces()
	{
		return spaces;
	}
	
}

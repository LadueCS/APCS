
import java.util.*;

public class Main 
{

	
	public static void main(String[] args) 
	{
		// uncomment to test your methods
		Equine[] spaces = new Equine[7];
		spaces[0] = new Equine("Trigger",1340);
		spaces[2] = new Equine("Silver",1210);
		spaces[5] = new Equine("Patches",1350);
		spaces[6] = new Equine("Duke",1410);
		
		for(int i = 0; i < spaces.length; i++)
		{
			System.out.println(spaces[i]);
		}
		
		HorseBarn barn = new HorseBarn(spaces);
		
		for(int i = 0; i < spaces.length; i++)
		{
			if(spaces[i] != null)
			   System.out.println(barn.findHorseSpace(spaces[i].getName()));
		}
		
		System.out.println(Arrays.toString(barn.getSpaces()));
		barn.consolidate();
		System.out.println(Arrays.toString(barn.getSpaces()));
		int[] a = {8,3,1};
		System.out.println(a[1] + " ");
		a[1]--;
		System.out.println(a[1] + " ");
		
	}

}

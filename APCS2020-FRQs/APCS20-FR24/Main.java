import java.util.*;

public class Main {
    public static void main(String args[]) {
        MC m = new MC();
        m.as(new MS("r", 2));
        m.as(new MS("r", 2));
        m.as(new MS("b", 5));
        m.as(new MS("r", 4));
        m.as(new MS("g", 2));
        
        System.out.println("Removing red");
        System.out.println(m.removeColor("r"));
        
        System.out.println("Collapsing sets");
        System.out.println(m.sets.get(0).c + " " + m.sets.get(1).c);
        
        
    }
}

public class MS {
    public String c;
    public int nm;
    public MS(String col, int numm) {
        c = col;
        nm = numm;
    }    
}

public class MC {
    public List<MS> sets;
    
    public MC() {
        sets = new ArrayList<MS>();
    }
    
    public void as(MS ts) {
        sets.add(ts);
    }
    
    public int getTotalMarbles() {
        int ret = 0;
        for (var v : sets) ret += v.nm;
        return ret;
    }
    
    public int removeColor(String mc) {
        int ret = 0;
        var tmp = sets;
        sets = new ArrayList<MS>();
        for (var v : tmp) {
            if (v.c != mc) sets.add(v);
            else ret += v.nm;
        }
        return ret;
    }
    
    public void joinMarbleSets() {
        List<MS> tmp = sets;
        sets = new ArrayList<MS>();
        for (var v : tmp) {
            boolean ins = false;
            for (var w : sets) {
                if (v.c.equals(w.c)) {
                    ins = true;
                    w.nm += v.nm;
                    break;
                }
            }
            if (ins) continue;
            sets.add(v);
        }
    }
    
    public MC joinMarbleCollection() {
        var ret = this;
        ret.joinMarbleSets();
        return ret;
    }
}

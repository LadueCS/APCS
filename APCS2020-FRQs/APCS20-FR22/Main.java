import java.util.*;

public class Main {
    public static void main(String args[]) {
        boolean[][] b = new boolean[4][4];
        b[0][0] = true;
        b[3][0] = true;
        b[2][2] = true;
        var c = new Crossword(b);
        c.print();
    }
}

public class Square {
    public int num;
    public boolean isBlack;
    
    public Square(boolean b, int n) {
        num = n;
        isBlack = b;
    }
}


public class Crossword {
    private Square[][] puzzle;
    
    public Crossword(boolean[][] blackSquares) {
        int r = blackSquares.length;
        int c = blackSquares[0].length;
        puzzle = new Square[r][c];
        for (int i = 0; i < r; ++i) {
            for (int j = 0; j < c; ++j) {
                puzzle[i][j] = new Square(blackSquares[i][j], 0);
            }
        }
        
        int cnt = 1;
        for (int i = 0; i < r; ++i) {
            for (int j = 0; j < c; ++j) {
                if (toBeLabeled(i, j)) puzzle[i][j].num = cnt++;
            }
        }           
    
    }
    private boolean toBeLabeled(int r, int c) {
        if (puzzle[r][c].isBlack) return false;
        if (r > 0 && !puzzle[r-1][c].isBlack && c > 0 && !puzzle[r][c-1].isBlack) return false;
        return true;
    }
    
    public void print() {
        int r = puzzle.length;
        int c = puzzle[0].length;
        
        for (int i = 0; i < r; ++i) {
            for (int j = 0; j < c; ++j) {
            
            if (puzzle[i][j].num > 0) System.out.print(puzzle[i][j].num);
            else if (puzzle[i][j].isBlack == true) System.out.print("B");
            else System.out.print(" ");
            }
            System.out.println();
        }
    }
}

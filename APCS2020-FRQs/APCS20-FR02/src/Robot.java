import java.util.Arrays;

public class Robot
{
  // private instance variables
  private int[] hall;           // A List containing the number of things on each hall tile.
  private int pos;              // The current position (tile number) of the robot.
  private boolean facingRight;  // True if robot facing right and false otherwise.

  // constructor
  public Robot(int[] toys, int pos, boolean facingRight)
  {
    hall = new int[toys.length];
    for (int i = 0; i < toys.length; i++)
      hall[i] = toys[i];

    // In this.<name> the 'this' prefix always refers to an instance variable with the given name,
    // which allows us to use the same name for explicit parameters in the constructor.
    this.pos = pos;
    this.facingRight = facingRight;
  }

  // Question 4.(a)

  // Insert your code here.
  private boolean forwardMoveBlock() {
    return facingRight ? pos < hall.length-1 : pos > 0;
  }

  // Question 4.(b)

  // Insert your code here.
  private void move() {
    if (hall[pos] > 0) --hall[pos];
    if (hall[pos] == 0) {
        if (forwardMoveBlock()) pos = facingRight ? pos+1 : pos-1;
        else facingRight = !facingRight;
    }
  }

  // Question 4.(c)
  public int clearHall() {
    int ret = 0;
    while (!hallIsClear()) {
        move();
        ++ret;
    }
    return ret;
  }

  // Insert your code here.

  private boolean hallIsClear()
  {
    int count = 0;

    for (int i = 0; i < hall.length; i++)
      count += hall[i];

    return (count == 0);
  }

}



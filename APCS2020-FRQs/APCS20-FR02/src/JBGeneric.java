/**********************************************************************************************
   ** NOTE that this question is in the file AP2004A_FRQ4.pdf.                               **
   ********************************************************************************************
   AP2004 A Exam: Free Response Question 4.(a), 4.(b), and 4.(c).
   In the Robot class create the following methods: forwardMoveBlocked,
   move, and clearHall.
**/

public class JBGeneric
{

  public static void main(String[] args)
  {
    System.out.println("\nTesting Answers to Question 4.(a), 4.(b), and 4.(c):\n");

    Robot robot = new Robot(new int[] {1,1,2,2}, 1, true);

    // Uncommenting the following should work after you create all three methods:
    System.out.println("\n It took " + robot.clearHall() + " moves to clear the hall.");
  }
}






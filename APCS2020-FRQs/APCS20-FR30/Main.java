import java.util.*;

public class Main {
    public static void main(String args[]) {
        for (int i = 0; i < 3; ++i) {
            System.out.println("Debug output");
            List<Tile> t = new ArrayList<Tile>();
            t.addAll(Arrays.asList(new Tile("A", 1), new Tile("B", 2), new Tile("C", 3)));
            var ts = new TileSet(t);

            var p = new Player();
            p.replaceTiles(ts);

            System.out.println("the real stuff");

            System.out.println(p.getWordScore(new int[]{ 0 }));
            System.out.println(p.getWordScore(new int[]{ 1 }));

            
        }
    }
}

public class Tile {
    public String letter;
    public int value;
    public Tile(String l, int v) {
        letter = l;
        value = v;
    }
}

public class TileSet {
    public List<Tile> tiles;
    public int unusedSize;

    public TileSet(List<Tile> t) {
        tiles = t;
    }
    
    public boolean allUsed() { return unusedSize == 0; }
    
    public void shuffle() {
        for (int i = tiles.size()-1; i > 0; --i) {
            int j = (int)((i+1)*Math.random());
            var a = tiles.get(i);
            var b = tiles.get(j);
            tiles.set(i, b);
            tiles.set(j, a);
        }
    }
}


public class Player {
    public static final int NumLetters = 2;

    private List<Tile> playerTiles;
    
    public Player() {
        playerTiles = new ArrayList<Tile>();
    }

    public void replaceTiles(TileSet t) {
        while (playerTiles.size() < NumLetters) {
            t.shuffle();
            for (var p : t.tiles) System.out.print(p.letter + " ");
            System.out.println();
            playerTiles.add(t.tiles.get(t.tiles.size()-1));
            System.out.println(t.tiles.size()-1);
            System.out.println(t.tiles);
            t.tiles.remove(t.tiles.size()-1);
        }

    }

    public int getWordScore(int[] indexes) {
        if (indexes[0] == -1) return 0;
        int ret = 0;
        for (int x : indexes) {
            ret += playerTiles.get(x).value;
        }
        if (indexes.length == NumLetters) ret += 20;
        return ret;
    }
}

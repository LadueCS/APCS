import java.util.*;

public class Main {
    public static void main(String args[]) {
        String[] A = { "test", "abc", "hello", "world" };
        RandomStringChooser rsc = new RandomStringChooser(A);
        for (int i = 0; i < 6; ++i) {
            System.out.print(rsc.getNext() + " ");
        }
        String s = "abcd";
        RandomLetterChooser rlc = new RandomLetterChooser(s);
        for (int i = 0; i < 6; ++i) {
            System.out.print(rlc.getNext() + " ");
        }
    }
}

public class RandomStringChooser {
    public ArrayList<String> al = new ArrayList<String>();
    public RandomStringChooser() {
    
    }
    public RandomStringChooser(String[] A) {
        for (var s : A) al.add(s);
    }
    public String getNext() {
        if (al.size() == 0) return "NONE";
        else {
            int idx = (int)(Math.random()*al.size());
            String ret = al.get(idx);
            al.remove(idx);
            return ret;
        }
    }
}

public class RandomLetterChooser extends RandomStringChooser {
    public RandomLetterChooser(String S) {
        for (int i = 0; i < S.length(); ++i) super.al.add(Character.toString(S.charAt(i)));
    }
    public String getNext() {
        return super.getNext();
    }
}

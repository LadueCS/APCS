import java.util.*;

public class WordList
{
  // Private instance variable declared at the top of this file.
  private ArrayList<String> myList;

  // Constructor with no explicit input parameters.
  public WordList() 
  { 
	  myList = new ArrayList<String>(); 
  }

  // Create add method that permits user to create the ArrayList of strings.
  public void add(String word) 
  { 
	  myList.add(word); 
	  
  }

  // Override Object toString() in order to be used by myList.
  public String toString() 
  { 
	  return myList.toString(); 
  }

  // Question 1.(a)

  public int numWordsOfLength(int len)
  {
    int count = 0;

    for (int i = 0; i < myList.size(); i++)
    {

        if (myList.get(i).length() == len) ++count;

    }
    return count;
  }

  // Question 1.(b)

  public void removeWordsOfLength(int len)
  {

    ArrayList<String> tmp = new ArrayList<String>();
    for (int i = 0; i < myList.size(); ++i)
    {
        if (myList.get(i).length() != len) tmp.add(myList.get(i));
    }
    myList = tmp;
  }

}

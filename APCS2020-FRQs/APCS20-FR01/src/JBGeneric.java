/**********************************************************************************************
   ** NOTE that this question is in the file AP2004A_FRQ1.pdf.                               **
   ********************************************************************************************
   AP2004 A Exam: Free Response Question 1.(a) and 1.(b).
   Create two methods in WordList class: numWordsOfLength and removeWordsOfLength.
**/

public class JBGeneric
{
   public static void main(String[] args)
   {
       // Test Answers to Question 1.(a) and 1.(b) created in the WordList class.

       WordList w = new WordList();
       w.add("cat");
       w.add("mouse");
       w.add("frog");
       w.add("dog");
       w.add("dog");
       System.out.println("Testing Answers to Question 1.(a) and 1.(b):");
       System.out.println(w + " " +
              w.numWordsOfLength(4) + " " +
              w.numWordsOfLength(3) + " " +
              w.numWordsOfLength(2));
       w.removeWordsOfLength(4);
       System.out.println(w);
       w.removeWordsOfLength(3);
       System.out.println(w);
       w.removeWordsOfLength(2);
       System.out.println(w);

   }
}





/**********************************************************************************************
   ** NOTE that this question is in the file ap09frq_computer_science_a.pdf.                               **
   ********************************************************************************************
   AP2009 A Exam: Free Response Question 3.(a), and 3.(b).
   In the BatteryCharger class create the following methods: getChargingCost,
   and getChargingStartTime.
**/

public class Main 
{

	public static void main(String[] args) 
	{
		int[] rateTable = {50,60,160,60,80,100,100,120,150,150,150,200,
				           40,240,220,220,200,200,180,180,140,100,80,60};
		BatteryCharger charger = new BatteryCharger(rateTable);
		/*** Uncomment the lines below to test.
		// Test getChargingCost method:
		int start = 12;
		int time = 1;
		System.out.println("Test getChargingCost method:");
		System.out.println("start = " + start + " time = " + time + " cost = " + charger.getChargingCost(start, time));
		start = 0;
		time = 2;
		System.out.println("start = " + start + " time = " + time + " cost = " + charger.getChargingCost(start, time));
		start = 22;
		time = 7;
		System.out.println("start = " + start + " time = " + time + " cost = " + charger.getChargingCost(start, time));
		start = 22;
		time = 30;
		System.out.println("start = " + start + " time = " + time + " cost = " + charger.getChargingCost(start, time));
		***/
		
		/*** Uncomment the lines below to test.
		// Test getChargingStartTime method:
		time = 1;
		System.out.println("\nTest getChargeStartTime method:");
		System.out.println("time = " + time + " start = " + charger.getChargingStartTime(time));
		time = 2;
		System.out.println("time = " + time + " start = " + charger.getChargingStartTime(time));
		time = 7;
		System.out.println("time = " + time + " start = " + charger.getChargingStartTime(time));
		time = 30;
		System.out.println("time = " + time + " start = " + charger.getChargingStartTime(time));
		time = 10;
		System.out.println("time = " + time + " start = " + charger.getChargingStartTime(time));
		***/
	}

}

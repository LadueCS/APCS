
public class DownloadInfo 
{
	private int numDownloads;
	private String title;
	
	public DownloadInfo(String title) 
	{
		numDownloads = 0;
		this.title = title;
	}
	
	public int getNumDownloads()
	{
		return numDownloads;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public void incrementTimesDownloaded()
	{
		numDownloads++;
	}
	
	public String toString()
	{
		return "DownloadInfo(" + title + "," + numDownloads + ")";
	}
	
}

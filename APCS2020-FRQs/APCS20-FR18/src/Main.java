
import java.util.*;

public class Main 
{
	public static void main(String[] args) 
	{
		// Create initial state of example problem data - objects and number of downloads
        DownloadInfo obj1 = new DownloadInfo("Hey Jude");
        for(int i = 0; i < 5; i++)
		{
			obj1.incrementTimesDownloaded();
		}
        DownloadInfo obj2 = new DownloadInfo("Soul Sister");
        for(int i = 0; i < 3; i++)
		{
			obj2.incrementTimesDownloaded();
		}
        DownloadInfo obj3 = new DownloadInfo("Aqualung");
        for(int i = 0; i < 10; i++)
		{
			obj3.incrementTimesDownloaded();
		}
        
        // Create initial state of downloadList for the example problem
        List<DownloadInfo> list1 = new ArrayList<DownloadInfo>();
        list1.add(obj1);
        list1.add(obj2);
        list1.add(obj3);
        System.out.println(list1);
        
        // Set up initial state of downloadList for the example problem
		MusicDownloads music = new MusicDownloads(list1);
		System.out.println(music.getDownloadList());
		
		// Test method getDownloadInfo
		
		System.out.println("\n" + music.getDownloadInfo("Hey Jude"));
		System.out.println(music.getDownloadInfo("Soul Sister"));
		System.out.println(music.getDownloadInfo("Aqualung"));
		System.out.println(music.getDownloadInfo("Happy Birthday"));
		
		
		// Test method updateDownloads
		
		ArrayList<String> moreTitles = new ArrayList<String>();
		moreTitles.add("Lights");
		moreTitles.add("Aqualung");
		moreTitles.add("Soul Sister");
		moreTitles.add("Go Now");
		moreTitles.add("Lights");
		moreTitles.add("Soul Sister");
	    System.out.println("\n" + moreTitles);
	    
	    music.updateDownloads(moreTitles);
	    System.out.println("\n" + music.getDownloadList());
	    
		
	}

}

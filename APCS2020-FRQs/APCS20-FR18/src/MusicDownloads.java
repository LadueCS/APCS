import java.util.*;

public class MusicDownloads 
{
	private List<DownloadInfo> downloadList;
	
	public MusicDownloads()
	{
		downloadList = new ArrayList<DownloadInfo>();
	}
	
	// This constructor makes it easier to set up initial state of the example problem 
	public MusicDownloads(List<DownloadInfo> downloadList)
	{
		this.downloadList = downloadList;
	}
	
	// Part (a).
	public DownloadInfo getDownloadInfo(String title)
	{
		// insert your code here
		for (DownloadInfo d : downloadList) {
            if (d.getTitle() == title) return d;
		}
		return null;
	}
	
	// Part (b).
	public void updateDownloads(List<String> titles)
	{
		// insert your code here
		for (String s : titles) {
            DownloadInfo d = getDownloadInfo(s);
            if (d != null) d.incrementTimesDownloaded();
            else {
                DownloadInfo n = new DownloadInfo(s);
                n.incrementTimesDownloaded();
                downloadList.add(n);
            }
        }
	}
	
	public List<DownloadInfo> getDownloadList()
	{
		return downloadList;
	}

}

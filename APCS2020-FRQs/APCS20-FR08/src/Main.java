/**********************************************************************************************
   ** NOTE that this question is in the file ap10frq_computer_science_a.pdf.                               **
   ********************************************************************************************
   AP2010 A Exam: Free Response Question 3.(a), and 3.(b).
   In the Trail class create the following methods: isLevelTrailSegment,
   and isDifficult.
**/

import java.util.*;

public class Main 
{
	public static void main(String[] args) 
	{
		int[] markers = {100,150,105,120,90,80,50,75,75,70,80,90,100};
		Trail trail = new Trail(markers);
		// Uncomment the code below to test your methods
		/***
		// Find all level paths.
		for (int i = 0; i < markers.length-1; i++)
		{
			for (int j = i+1; j < markers.length; j++)
			{
				int start = i;
				int end = j;
				if (trail.isLevelTrailSegment(start, end))
				{
					System.out.println("\n markers = " + Arrays.toString(markers));
					System.out.println("\n start = " + start + " end = " + end);
					System.out.println("\n isLevelTrailSegment = " + trail.isLevelTrailSegment(start, end));
				} 
			}
		}
		
		// Determine whether or not the trail is difficult.
		System.out.println("\n markers = " + Arrays.toString(markers));
		System.out.println("\n isDifficult = " + trail.isDifficult());
		***/
	}

}

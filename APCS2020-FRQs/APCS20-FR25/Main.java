import java.util.*;

public class Main {
    public static void main(String args[]) {
        var p1 = new Parabola(2, -6, -5);
        System.out.println(p1.getAxis());
        System.out.println(p1.isOnGraph(4, 3));
        
        var p2 = new Parabola(4, 2, -2);
        var pt = getIntersectionPoints(p1, p2);
        for (var p : pt) {
            System.out.println(Double.toString(p.x()) + " " + Double.toString(p.y()));
        }
    }
    public static ArrayList<Point2D> getIntersectionPoints(Parabola p, Parabola q) {
        var r = new Parabola(p.a-q.a, p.b-q.b, p.c-q.c);
        var ret = new ArrayList<Point2D>();
        if (r.b*r.b-4*r.a*r.c > 0) {
            double x1 = (-r.b-Math.sqrt(r.b*r.b-4*r.a*r.c))/2;
            ret.add(new Point2D(x1, x1*p.a*p.a + x1*p.b + p.c));
            double x2 = (-r.b+Math.sqrt(r.b*r.b-4*r.a*r.c))/2;
            ret.add(new Point2D(x2, x2*p.a*p.a + x2*p.b + p.c));
        }
        else if (r.b*r.b-4*r.a*r.c == 0) {
            double x1 = (-r.b)/2;
            ret.add(new Point2D(x1, x1*p.a*p.a + x1*p.b + p.c));
        }
        return ret;
    }
}

public class Parabola {
    public int a, b, c;
    public Parabola(int _a, int _b, int _c) {
        a = _a;
        b = _b;
        c = _c;
    }
    public double getAxis() {
        return -b/(2.0*a);
    }
    public boolean isOnGraph(int x, int y) {
        return y == a*x*x + b*x + c;
    }    
} 

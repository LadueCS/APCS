/**********************************************************************************************
   ** NOTE that this question is in the file ap10frq_computer_science_a.pdf.                               **
   ********************************************************************************************
   AP2010 A Exam: Free Response Question 1.(a), and 1.(b).
   In the MasterOrder class create the following methods: getTotalBoxes,
   and removeVariety.
**/

public class Main 
{

	public static void main(String[] args) 
	{
		// Uncomment the following lines to test your methods.
		/***
		MasterOrder goodies = new MasterOrder();
		goodies.addOrder(new CookieOrder("Chocolate Chip",1));
		goodies.addOrder(new CookieOrder("Shortbread",5));
		goodies.addOrder(new CookieOrder("Macaroon",2));
		goodies.addOrder(new CookieOrder("Chocolate Chip",3));
		
		System.out.println("\ngoodies = " + goodies.getOrders());
		System.out.println("total boxes = " + goodies.getTotalBoxes());
		System.out.println("total Oatmeal Rasin removed = " + goodies.removeVariety("Oatmeal Rasin"));
		System.out.println("total boxes = " + goodies.getTotalBoxes());
		System.out.println("total Chocolate Chip removed = " + goodies.removeVariety("Chocolate Chip"));
		System.out.println("total boxes = " + goodies.getTotalBoxes());
		
		MasterOrder goodies2 = new MasterOrder();
		
		System.out.println("\ngoodies2 = " + goodies2.getOrders());
		System.out.println("total boxes = " + goodies2.getTotalBoxes());
		System.out.println("total Oatmeal Rasin removed = " + goodies2.removeVariety("Oatmeal Rasin"));
		System.out.println("total boxes = " + goodies2.getTotalBoxes());
		System.out.println("total Chocolate Chip removed = " + goodies2.removeVariety("Chocolate Chip"));
		System.out.println("total boxes = " + goodies2.getTotalBoxes());
		***/
	}

}

/**********************************************************************************************
   ** NOTE that this question is in the file ap10frq_computer_science_a.pdf.                               **
   ********************************************************************************************
   AP2010 A Exam: Free Response Question 1.(a), and 1.(b).
   In the MasterOrder class create the following methods: getTotalBoxes,
   and removeVariety.
**/

import java.util.*;

public class MasterOrder 
{
	private List<CookieOrder> orders;
 	
	public MasterOrder() 
	{
		orders = new ArrayList<CookieOrder>();
	}
	
	public void addOrder(CookieOrder order)
	{
		orders.add(order);
	}
	
	public List<CookieOrder> getOrders()
	{
		return orders;
	}
	
	
	public int getTotalBoxes()
	{
		
		// insert your code here
		
	}
	
	public int removeVariety(String cookieVar)
	{
		
		// insert your code here
		
	}
	
	
}

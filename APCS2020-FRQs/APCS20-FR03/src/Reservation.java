public class Reservation
{
  // private instance variables
  private String guestName;
  private int roomNumber;

  // constructor
  public Reservation(String guestName, int roomNumber)
  {
    this.guestName = guestName;
    this.roomNumber = roomNumber;
  }

  // method
  public int getRoomNumber()
  {
    return roomNumber;
  }

  // Override toString method for specialized printing.
  public String toString()
  {
    return "[" + guestName + ", " + roomNumber + "]";
  }
}

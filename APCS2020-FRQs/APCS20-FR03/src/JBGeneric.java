/**********************************************************************************************
   ** NOTE that this question is in the file AP2005A_FRQ1.pdf.                               **
   ********************************************************************************************
   AP2005 A Exam: Free Response Question 1.(a) and 1.(b).
   In the Hotel class create the following methods: requestRoom and cancelAndReassign.
**/

public class JBGeneric
{
   public static void main(String[] args)
   {
       // Test Answers to Question 1.(a) and 1.(b) created in the Hotel class.

       int hotelSize = 3;
       Hotel hotel = new Hotel(hotelSize);
       String createRes = "Try to create reservation for ";

       System.out.println("\nTest Answers to Question 1.(a) and 1.(b) created in the Hotel class:\n");

       // Uncommenting the following should work after you create both methods:

       
       System.out.println(createRes + "Amanda: " + hotel.requestRoom("Amanda"));
       System.out.println(createRes + "Ben: " + hotel.requestRoom("Ben"));
       System.out.println(createRes + "Cate: " + hotel.requestRoom("Cate"));
       System.out.println(createRes + "Don: " + hotel.requestRoom("Don"));
       System.out.println(createRes + "Euginia: " + hotel.requestRoom("Euginia"));
       System.out.println();

       String currHotelRes = "Current Hotel Reservations: ";
       System.out.println(currHotelRes + hotel);
       System.out.println();

       System.out.println("Cancel Cate Rm 2 and try to reassign: " +
                          hotel.cancelAndReassign(new Reservation("Cate", 2)));
       System.out.println("Cancel Amanda Rm 0 and try to reassign: " +
                          hotel.cancelAndReassign(new Reservation("Amanda", 0)));
       System.out.println("Cancel Ben Rm 1 and try to reassign: " +
                          hotel.cancelAndReassign(new Reservation("Ben", 1)));
       System.out.println();

       System.out.println(currHotelRes + hotel);
       System.out.println();

       System.out.println(createRes + "Frank: " + hotel.requestRoom("Frank"));
       System.out.println(createRes + "Gabrielle: " + hotel.requestRoom("Gabrielle"));
       System.out.println();

       System.out.println(currHotelRes + hotel);
    System.out.println();
    
   }
}





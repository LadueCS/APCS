import java.util.ArrayList;

public class Hotel
{
  // private instance variable
  private Reservation[] rooms;
    // Each list element corresponds to a room in the hotel.
    // If rooms[index] is null, the room is empty,
    // otherwise, it contains a reference to the Reservation
    // for that room, such that
    // rooms[index].getRoomNumber() returns index.

  // private instance variable
  private ArrayList<String> waitList;
    // The ArrayList waitList contains names of guests who have
    // not yet been assigned a room because all rooms are full.

  // constructor
  public Hotel(int nRooms)
  {
    rooms = new Reservation[nRooms];  // Ordinary list must be dimensioned by nRooms,
                                      // which is the total number of rooms in the hotel.
    waitList = new ArrayList<String>();       // ArrayList doesn't need to be dimensioned.
  }

  // Question 1.(a)

  // If there are any empty rooms (rooms with no reservation),
  // then create a reservation for an (the first) empty room
  // for the specified guest and return the new Reservation.
  // Otherwise, add the guest to the end of waitList
  // and return null.

  // Insert your code here.
  public Reservation requestRoom(String guestName) {
    for (int i = 0; i < rooms.length; ++i) {
        if (rooms[i] == null) {
            rooms[i] = new Reservation(guestName, i);
            return rooms[i];
        }
    }
    waitList.add(guestName);
    return null;
  }

  // Question 1.(b)

  // Release the room associated with parameter res, effectively
  // cancelling the reservation.
  // If any names are stored in waitList, then remove the first
  // mame and create a Reservation for this person in the room
  // currently reserved by res and return that new Reservation.
  // If waitList is empty, mark the room specified by res as empty (null)
  // and return null.
  // Precondition:  res is a valid Reservation for some room
  //                in this hotel.

  // Insert your code here.
    public Reservation cancelAndReassign(Reservation res) {
        int i = res.getRoomNumber();
        if (waitList.size() > 0) {
            rooms[i] = new Reservation(waitList.get(0), i);
            waitList.remove(0);
            return rooms[i];
        }
        return null;
    }
  
  
  // Override toString method for specialized printing.
  public String toString()
  {
    String s = "";

    for (int i = 0; i < rooms.length; i++)
      s += rooms[i] + " ";

    s += "Waitlist: " + waitList;
    return s;
  }
}

import java.util.*;

public class Main {
    public static void main(String args[]) {
        List<String> wl = Arrays.asList("AP", "COMP", "SCI", "ROCKS");
        
        System.out.println(SF.basicGapWidth(wl, 20));
        System.out.println(SF.leftoverSpaces(wl, 20));
        System.out.println(SF.format(wl, 20));
        
    }
}

public class SF {
    public static int totalLetters(List<String> wordList) {
        int ret = 0;
        for (var v : wordList) ret += v.length();
        return ret;
    }
    public static int basicGapWidth(List<String> wordList, int formattedLen) {
        int tl = totalLetters(wordList);
        return (formattedLen-tl)/(wordList.size()-1);
    }
    public static int leftoverSpaces(List<String> wordList, int formattedLen) {
        int bgw = basicGapWidth(wordList, formattedLen);
        return formattedLen-totalLetters(wordList)-(wordList.size()-1)*bgw;
    }
    public static String format(List<String> wordList, int formattedLen) {
        int bgw = basicGapWidth(wordList, formattedLen);
        int ls = leftoverSpaces(wordList, formattedLen);
        
        String ret = "";
        for (int i = 0; i < wordList.size(); ++i) {
            ret += wordList.get(i);
            if (i < wordList.size()-1) {
                for (int j = 0; j < bgw; ++j) ret += " ";
                if (i < ls) ret += " ";
            }
            
        }
        return ret;
    }
    
}

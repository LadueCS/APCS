/**********************************************************************************************
   ** NOTE that this question is in the file ap11frq_computer_science_a.pdf.                               **
   ********************************************************************************************
   AP2011 A Exam: Free Response Question 4.
   In the RouteCipher class create the following methods: fillBlock, encryptBlock, and encryptMessage.
**/

public class RouteCipher 
{
	private String plainText;
	private int numRows;
	private int numCols;
	private String[][] letterBlock;
	
	public RouteCipher(String plainText, int numRows, int numCols)
	{
		this.plainText = plainText;
		this.numRows = numRows;
		this.numCols = numCols;
		letterBlock = new String[numRows][numCols];
		fillBlock(plainText);
	}
	
	private void fillBlock(String str)
	{
		for (int i = 0; i < numRows; ++i)
            for (int j = 0; j < numCols; ++j)
                letterBlock[i][j] = i*numCols+j < str.length() ? str.substring(i*numCols+j, i*numCols+j+1) : "A";
	}
	
	public String encryptBlock()
	{
		String ret = "";
		for (int j = 0; j < numCols; ++j)
            for (int i = 0; i < numRows; ++i) ret += letterBlock[i][j];
        return ret;
	}
	
	public String encryptMessage(String message) 
	{
		String ret = "";
		for (int i = 0; i < (message.length()+numRows*numCols-1)/(numRows*numCols); ++i) {
            fillBlock(message.substring(i*numRows*numCols, Math.min((i+1)*numRows*numCols, message.length())));
            System.out.println(encryptBlock());
            ret += encryptBlock();
		}
		return ret;
	}
	
	public String[][] getLetterBlock()
	{
		return letterBlock;
	}
	
	public void printLetterBlock()
	{
	   for (int i = 0; i < numRows; i++)
	   {
		   for (int j = 0; j < numCols; j++)
		   { 
	          System.out.print(letterBlock[i][j]);
		   }
		   System.out.println();
	   }
	}
}

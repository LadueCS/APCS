/**********************************************************************************************
   ** NOTE that this question is in the file ap11frq_computer_science_a.pdf.                               **
   ********************************************************************************************
   AP2011 A Exam: Free Response Question 4.
   In the RouteCipher class create the following methods: fillBlock, encryptBlock, and encryptMessage.
**/

public class Main 
{
	public static void main(String[] args) 
	{
		// Uncomment this code to test your fillBlock and encryptBlock methods.
		
		String str1 = "Meet at noon";
		RouteCipher rc1 = new RouteCipher(str1,3,5);
		rc1.printLetterBlock();
		System.out.println(rc1.encryptBlock()+ "\n");
		
		String str2 = "Meet at midnight";
		RouteCipher rc2 = new RouteCipher(str2,3,5);
		rc2.printLetterBlock();
		System.out.println(rc2.encryptBlock()+ "\n");
		
		
		// Uncomment this code to test your encryptMessage method.
		
		RouteCipher rc3 = new RouteCipher(str2,2,3);
		System.out.println(rc3.encryptMessage(str2));
		
	}
}

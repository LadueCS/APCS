import java.util.*;

public class MainNxMExample
{

	public static void main(String[] args) 
	{
		int[][] arr = new int[4][5];
		for(int i = 0; i < arr.length; i++)  // number rows = arr.length
		{
			System.out.println("\n");
			for(int j = 0; j < arr[0].length; j++)
			{
				arr[i][j]= i*arr[0].length + j;  // number cols = arr[0].length
				System.out.print("arr["+i+"]["+j+"] = " + arr[i][j] + " ");
			}
			System.out.println();
			System.out.println("arr["+i+"] = "+ Arrays.toString(arr[i]));
		}

	}

}

import java.util.*;
// An archive of array methods.
public class DiverseArray 
{
	// Part (a).
	public static int arraySum(int[] arr)
	{
		int ret = 0;
		for (int i = 0; i < arr.length; ++i) ret += arr[i];
		return ret;
	}

	// Part (b).
	public static int[] rowSums(int[][] arr2D)
	{ 
		int ret[] = new int[arr2D.length];
		for (int i = 0; i < arr2D.length; ++i) ret[i] = arraySum(arr2D[i]);
		return ret;
	}
	
	// Part (c).
	public static boolean isDiverse(int[][] arr2D)
	{
		int ret[] = rowSums(arr2D);
		Arrays.sort(ret);
		for (int i = 1; i < ret.length; ++i)
            if (ret[i-1] == ret[i]) return false;
        return true;
	}
	

}

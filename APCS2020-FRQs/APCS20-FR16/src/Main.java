import java.util.*;

public class Main 
{
	public static void main(String[] args) 
	{
		int[] arr1 = {1,3,2,7,3};
		
		int[][] mat1 = {{1,3,2,7,3},
				        {10,10,4,6,2},
				        {5,3,5,9,6},
				        {7,6,4,2,1}
		               };
	
		int[][] mat2 = {{1,1,5,3,4},
						{12,7,6,1,9},
						{8,11,10,2,5},
						{3,2,3,0,6}
				       };
		
		// Part (a). Test your arraySum method on arr1. Note that for static methods
		// in a class archive of methods, you can simply 'dot' off the class name.
		System.out.println("DiverseArray.arraySum(arr1) = " + DiverseArray.arraySum(arr1));
		
		// Part (b). Test your rowSums method on both mat1 & mat2.
		System.out.println("\nDiverseArray.rowSums(mat1) = " + Arrays.toString(DiverseArray.rowSums(mat1)));
		System.out.println("DiverseArray.rowSums(mat2) = " + Arrays.toString(DiverseArray.rowSums(mat2)));
				
		// Part (c). Test your isDiverse method on both mat1 & mat2.
		System.out.println("\nDiverseArray.isDiverse(mat1) = " + DiverseArray.isDiverse(mat1));
		System.out.println("DiverseArray.isDiverse(mat2) = " + DiverseArray.isDiverse(mat2));
		


		
		
		
	}

}

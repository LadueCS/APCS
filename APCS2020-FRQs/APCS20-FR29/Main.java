import java.util.*;

public class Main {
    public static void main(String args[]) {
        Shoes shoes = new Shoes(40);
        Pants pants = new Pants(50);
        Top top = new Top(10);
        
        ClothingItem outfit = new Outfit(shoes, pants, top);
        System.out.println(outfit.getPrice());
        
        
    }
}

public interface ClothingItem {
    public String getDescription();
    public double getPrice();
}

public class Shoes implements ClothingItem {
    private double price;
    public String getDescription() {
        return "shoes";
    }
    public Shoes(double p) {
        price = p;
    }
    public double getPrice() { return price; }
}

public class Pants implements ClothingItem {
    private double price;
    public String getDescription() {
        return "pants";
    }
    public Pants(double p) {
        price = p;
    }
    public double getPrice() { return price; }
}

public class Top implements ClothingItem {
    private double price;
    public String getDescription() {
        return "top";
    }
    public Top(double p) {
        price = p;
    }
    public double getPrice() { return price; }
}

public class Outfit implements ClothingItem {
    private String des;
    private double price;
    public String getDescription() {
        return des;
    }
    public Outfit(Shoes s, Pants p, Top t) {
        des = "";
        des += s.getDescription();
        des += "/";
        des += p.getDescription();
        des += "/";
        des += t.getDescription();
        des += " outfit";
        if (s.getPrice()+p.getPrice()>=100 || s.getPrice()+t.getPrice() >= 100 || t.getPrice()+p.getPrice() >= 100) price = 0.75*(s.getPrice()+t.getPrice()+p.getPrice());
        else price = 0.9*(s.getPrice()+t.getPrice()+p.getPrice());
    }
    public double getPrice() { return price; }

}

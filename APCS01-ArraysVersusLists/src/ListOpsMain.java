
import java.util.*;

public class ListOpsMain 
{
	public static void main(String[] args) 
	{
		/** This is an introduction to the ArrayList data structure
		 ** that you are required to learn:
		 ** (1) read the examples below, run this file, and learn the
		 ** corresponding 7 ArrayList operations that are displayed,
		 ** (2) experiment by changing the artists below to your 
		 ** favorite artists.
		**/
		// An Array of Strings
		String[] favArtists = new String[5];
		favArtists[0] = "Eric Clapton";
		favArtists[1] = "Alicia Keys";
		favArtists[2] = "Eagles";
		favArtists[3] = "Fleetwood Mac";
		favArtists[4] = "Rolling Stones";
		System.out.println("Unchangeable String[] array version:");
		System.out.println(Arrays.toString(favArtists));
		System.out.println();
        // A List of Strings
		List<String> favArtists2 = new ArrayList<String>();
		favArtists2.add("Eric Clapton");
		favArtists2.add("Alicia Keys");
		favArtists2.add("Eagles");
		favArtists2.add("Fleetwood Mac");
		favArtists2.add("Rolling Stones");
		System.out.println("Changeable ArrayList<String> version:");
		System.out.println(favArtists2);
		System.out.println();
		
		// 1. you can add stuff at the end!
		System.out.println("1. you can add stuff at the end!");
		favArtists2.add("Chiles");
		System.out.println(favArtists2);
		System.out.println();
		
		// 2. you can remove stuff!
		System.out.println("2. you can remove stuff, index = 2!");
		favArtists2.remove(2);
		System.out.println(favArtists2);
		System.out.println();
		
		// 3. you can add stuff in the middle!
		System.out.println("3. you can add stuff in the middle, index = 3!");
		favArtists2.add(3, "Beatles");
		System.out.println(favArtists2);
		System.out.println();
		
		// 4. you can replace stuff!
		System.out.println("4. you can replace stuff, index = 4!");
		favArtists2.set(4, "Drake");
		System.out.println(favArtists2);
		System.out.println();
		
		// 5. you can get an item at an index!
		System.out.println("5. you can get an item at an index, index = 3!");
		String item = favArtists2.get(3); // get at index 3
		System.out.println(favArtists2);
		System.out.println(item);
		System.out.println();
		
		// 6. you can get the index of an item!
		System.out.println("6. you can get the index of an item (Drake)!");
		int index = favArtists2.indexOf("Drake"); // get the index of an item
		System.out.println(favArtists2);
		System.out.println("index = " + index);
		System.out.println();
		
		// 7. you can get the size (length) of an ArrayList!
		System.out.println("7. you can get the size (length) of an ArrayList!");
		int size = favArtists2.size(); // get size
		System.out.println(favArtists2);
		System.out.println("size = " + size);
		System.out.println();
		
		// Same operations on an ArrayList of Integer objects 
		ArrayList<Integer> arr = new ArrayList<Integer>();
		arr.add(51);
		arr.add(62);
		arr.add(73);
		arr.add(84);
		System.out.println("Same operations on an ArrayList of Integer objects!");
		System.out.println(arr);
		System.out.println();
		
		// 1. add to end
		System.out.println("1. add to end:");
		arr.add(95);
		System.out.println(arr);
		System.out.println();
		
		// 2. remove at index
		System.out.println("2. remove at index = 1:");
		arr.remove(1);
		System.out.println(arr);
		System.out.println();
		
		// 3. add at index
		System.out.println("3. add at index = 0:");
		arr.add(0,62);
		System.out.println(arr);
		System.out.println();
		
		// 4. set (replace) at index
		System.out.println("4. set (replace) at index = 2:");
		arr.set(2,62);
		System.out.println(arr);
		System.out.println();
		
		// 5. get item at an index
		System.out.println("5. get item at an index = 3:");
		int it = arr.get(3); // get at index 3
		System.out.println(arr);
		System.out.println(it);
		System.out.println();
		
		// 6. get index of item
		System.out.println("6. get index of item = 95:");
		int ind = arr.indexOf(95);
		System.out.println(arr);
		System.out.println("index = " + ind);
		System.out.println();
		
		// 7. get size of ArrayList
		System.out.println("7. get size of ArrayList:");
		int siz = arr.size();
		System.out.println(arr);
		System.out.println("size = " + siz);
		System.out.println();
	}

}

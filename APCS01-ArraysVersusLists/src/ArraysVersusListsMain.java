import java.util.*;

public class ArraysVersusListsMain {

	public static void main(String[] args) 
	{
		// An Array of ints
		int[] arr1 = {1,2,3};
		int[] arr2 = new int[5];
        for(int i = 0; i < 5; i++)
        {
        	arr2[i] = 5-i;
        }
        System.out.println("Start Array Examples output:");
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
        // An Array of doubles
        double[] arr3 = new double[4];
        for(int i = 0; i < 4; i++)
        {
        	arr3[i] = i*Math.PI;
        }
        System.out.println(Arrays.toString(arr3));
        // An Array of chars
        char[] arr4 = {'l', 'a','d','u','e'};
        System.out.println(Arrays.toString(arr4));
        // An Array of booleans
        boolean[] arr5 = new boolean[6];
        arr5[0] = true;
        arr5[1] = false;
        arr5[2] = true;
        arr5[3] = false;
        arr5[4] = true;
        System.out.println(Arrays.toString(arr5));
        System.out.println(sayHi("bob"));
        // An Array of Strings
        String[] peeps = {"jalen","winston","bobby","jason","gavin",
        		          "anushka","juno","nevin","zeru", "heinrich",
        		          "adam","william","brendan","sam","braden"};
        System.out.println(Arrays.toString(peeps));
        System.out.println("End Array Examples output.");
    	System.out.println("\nArray version:");
    	System.out.println(Arrays.toString(peeps));
    	System.out.println(Arrays.toString(genRandomIndexes(10,6)));
    	System.out.println(Arrays.toString(randomSubset(peeps,5)));
    	System.out.println(Arrays.toString(genRandomIndexes(15,15)));
    	System.out.println(Arrays.toString(randomSubset(peeps,15)));
    	System.out.println(Arrays.toString(genRandomPerm(10)));
    	// An Array of Strings
		String[] words = {"abrasive","absent","absolute","abridge","abroad","abrogate",
				          "abstention","absorb","abstract","absurd","abrupt","abundant"};
		System.out.println(Arrays.toString(matchingSubset(words,"ab")));
		System.out.println(Arrays.toString(matchingSubset(words,"abr")));
		System.out.println(Arrays.toString(matchingSubset(words,"abro")));
		System.out.println(Arrays.toString(matchingSubset(words,"abs")));
		System.out.println(Arrays.toString(matchingSubset(words,"abso")));
		System.out.println(Arrays.toString(matchingSubset(words,"abst")));    	
		// A List of Strings
    	List<String> peeps2 = new ArrayList<String>(Arrays.asList(peeps));
    	System.out.println("\nList version:");
    	System.out.println(peeps2);
    	System.out.println(genRandomListIndexes(10,6));
    	System.out.println(randomSubset(peeps2,5)); // can overload method name with different input parameters
    	System.out.println(genRandomListIndexes(15,15));
    	System.out.println(randomSubset(peeps2,15));
    	// A List of Strings
		List<String> words2 = new ArrayList<String>(Arrays.asList(words));
		System.out.println(matchingSubset(words2,"ab"));
		System.out.println(matchingSubset(words2,"abr"));
		System.out.println(matchingSubset(words2,"abro"));
		System.out.println(matchingSubset(words2,"abs"));
		System.out.println(matchingSubset(words2,"abso"));
		System.out.println(matchingSubset(words2,"abst"));    	
    	
    }        
              
	
	public static String sayHi(String name)
	{
		return "Hi " + name;
	}
/**	
	// Given an array of strings, arr, return an array of
	// length m comprised of m randomly chosen elements of arr.
	public static String[] randomSubset(String[] arr, int m)
	{
		
	}
	
	// Given an array, arr, and a string, str, return an array
	// of all those elements of arr that have a prefix matching str.
	public static String[] matchingSubset(String[] arr, String str)
	{
		
	}
**/

// Array versions.

// Test whether or not an integer is in an array of integers.
public static boolean inArray(int[] indexes, int m)
{
	for(int i = 0; i < indexes.length; i++)
	{
		if(indexes[i] == m)
		{
			return true;
		}
	}
	return false;
}

// Generate an array of m random indexes with 0 < m <= n.
public static int[] genRandomIndexes(int n, int m)
{
	if(0 < m && m <= n)
	{
	   int[] indexes = new int[m];
	   int index = -1;
	   for(int i = 0; i < m; i ++)
	   {
		   indexes[i] = -1;
       }
	   Random rand = new Random();
	   for(int j = 0; j < m; j ++)
	   {
	       while(inArray(indexes,index))
	       {
	          index = rand.nextInt(n);
	       }
	       indexes[j] = index;
	   }
	   return indexes;
	}
	else
	{
		return null;
	}
}

// Create an array which is a random permutation of n indexes.
// We get this useful method free by solving the general problem.
public static int[] genRandomPerm(int n)
{
   return genRandomIndexes(n,n);
}

// Given an array of strings, arr, return an array of length m
// comprised of m randomly chosen elements of arr.
public static String[] randomSubset(String[] arr, int m)
{
	int[] indexes = genRandomIndexes(arr.length,m);
	String[] randomSubset = new String[m];
	for(int i = 0; i < m; i++)
	{
		randomSubset[i] = arr[indexes[i]];
	}
	return randomSubset;
}

// Given an array, arr, and a string, str, return an array
// of all those elements of arr that have a prefix matching str.
public static String[] matchingSubset(String[] arr, String str)
{
	String[] match1 = new String[arr.length];
	int j = 0;
	for(int i = 0; i < arr.length; i++)
	{
		if(arr[i].substring(0,str.length()).equals(str))
		{
			match1[j] = arr[i];
			j++;
		}
	}
	String[] match2 = new String[j];
	for(int k = 0; k < j; k++)
	{
		match2[k] = match1[k];
	}
	return match2;
}	



// List versions.

// Generate a list of m random indexes with 0 < m <= n.
public static List<Integer> genRandomListIndexes(int n, int m)
{
	if(0 < m && m <= n)
	{
	/*	
	   int[] indexes = new int[m];
	   int index = -1;
	   for(int i = 0; i < m; i ++)
	   {
		   indexes[i] = -1;
       }
    */
	   List<Integer> indexes = new ArrayList<Integer>();
	   Integer index = null;
	   Random rand = new Random();
	   for(int j = 0; j < m; j ++)
	   {
	       //while(inArray(indexes,index))
		   while(index == null || indexes.contains(index))
	       {
	          index = rand.nextInt(n);
	       }
	       //indexes[j] = index;
		   indexes.add(index);
	   }
	   return indexes;
	}
	else
	{
		return null;
	}
}

// Create a list which is a random permutation of n indexes.
public static List<Integer> genRandomListPerm(int n)
{
   return genRandomListIndexes(n,n);
}	

// Given a list of strings, arr, return a list of length m
// comprised of m randomly chosen elements of arr.
// Note that we can overload method name with different input parameters.
public static List<String> randomSubset(List<String> arr, int m)
{
	/*
	int[] indexes = genRandomIndexes(arr.length,m);
	String[] randomSubset = new String[m];
	for(int i = 0; i < m; i++)
	{
		randomSubset[i] = arr[indexes[i]];
	}
	return randomSubset;
	*/
	
	List<Integer> indexes = genRandomListIndexes(arr.size(),m);
	List<String> randomSubset = new ArrayList<String>();
	for(int i = 0; i < m; i++)
	{
		randomSubset.add(arr.get(indexes.get(i)));
	}
	return randomSubset;
}

// Given a list, arr, and a string, str, return an array
// of all those elements of arr that have a prefix matching str.
public static List<String> matchingSubset(List<String> arr, String str)
{/*
	String[] match1 = new String[arr.length];
	int j = 0;
	for(int i = 0; i < arr.length; i++)
	{
		if(arr[i].substring(0,str.length()).equals(str))
		{
			match1[j] = arr[i];
			j++;
		}
	}
	String[] match2 = new String[j];
	for(int k = 0; k < j; k++)
	{
		match2[k] = match1[k];
	}
	return match2;
	*/
	List<String> match = new ArrayList<String>();
	for(int i = 0; i < arr.size(); i++)
	{
		if(arr.get(i).substring(0,str.length()).equals(str))
		{
			match.add(arr.get(i));
		}
	}
	return match;
}	

}

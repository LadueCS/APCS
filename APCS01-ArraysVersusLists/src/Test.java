import java.util.*;
 
public class Test {
    public static void main(String args[]) {
        System.out.println(getIndexesOf("ab","abcbabab"));
    }
    public static List<Integer> getIndexesOf(String str5, String str2)
    {
    int n1 = str5.length();
    int n2 = str2.length();
    List<Integer> indexes = new ArrayList<Integer>();

    if(n1 > n2) return indexes; // str1 is too long to occur in str2
    for(int i = 0; i < n2-n1+1; i++)
    {
        if(str2.substring(i, i + n1).equals(str5))
        {
            indexes.add(i);
        }
    }
    return indexes;

    }
}

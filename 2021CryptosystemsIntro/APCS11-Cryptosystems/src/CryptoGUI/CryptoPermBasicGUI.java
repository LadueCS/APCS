/********* CryptoPermBasicGUI.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 *
 */

package CryptoGUI;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import CryptoPerm.CryptoPermBasic;
import LexiconData.LexiconData;
import LexiconData.Ntuple;
import LexiconData.NtupleComparator;
import LexiconData.ReadWriteFile;

@SuppressWarnings("serial")
public class CryptoPermBasicGUI extends CryptoGenKeyGUI
{	
	// Instance variables for the Crypto processing
	private String enterTxt, plainTxt, cipherTxt, decipherTxt;
	// Cryptosystem variables
	private List<Character> alphabet = new ArrayList<Character>();
	private int alphaSize;
	private String dirPath;
	private String lexicon;
	private CryptoPermBasic cp;
	private int[] encryptKey = new int[alphaSize];
	private int[] decryptKey = new int[alphaSize];
	private List<String> plaintextExample;
	private String plaintextExampleStr;
	
	public CryptoPermBasicGUI(String title)
	{
		super(title);
	    // Create Alphabet & Lexicon
		Character[] arr = {'a','i','u','k','l','n','p','s','t','v'};
		alphabet = new ArrayList<Character>(Arrays.asList(arr));
	    dirPath = "src//CryptoPerm//";
	    lexicon = "mystery10.txt";
	    // Create CryptoPermBasic object
		cp = new CryptoPermBasic(alphabet, dirPath, lexicon,false);
		alphaSize = cp.getAlphaSize();
		// Mystery10 Example
		String[] mystery = {"atuti", "satisi", "vuvapi", "vivaki", "uvulu"};
		plaintextExample = new ArrayList<String>(Arrays.asList(mystery));
		plaintextExampleStr = "";
		for(String str : plaintextExample)
    	{
			plaintextExampleStr += str;
    	}
	}
	
    // Required and essential implementation of abstract  
    // actionPerformed method in ActionListener interface
	// in superclass CryptoGUI. Here we override this method
	// which is just a stub in the superclass.
	// Note also that TextArea & Button variables are passed
	// from the superclass to this subclass by declaring them
	// protected instead of private (see the superclass).
	public void actionPerformed(ActionEvent evt)
	{    
		  if (evt.getSource() == generateKeyButton)
	      {
			  encryptKey = cp.randomPermNoFixedPoints(alphaSize);
			  decryptKey = cp.inversePerm(encryptKey);
			  generateKeyTextArea.setText("alphaSize = " + alphaSize +
				                          "\nencryptKey = " + Arrays.toString(encryptKey) +
			                              "\ndecryptKey = " + Arrays.toString(decryptKey) +
			                              "\nEnter this mystery10 plaintext below: " + plaintextExampleStr);         
	      }
		  else if (evt.getSource() == encryptButton)
	      {
	    	  enterTxt = enterTextArea.getText();
		      if(inAlphabetWords(enterTxt))
	          { 
		         plainTxt = enterTxt;
		         cipherTxt = cp.permOfStr(encryptKey, plainTxt);
	             // Print in text area
	             encryptTextArea.setText("plainTxt = " + plainTxt +
	        		                    "\n" + "encryptKey = " + Arrays.toString(encryptKey) +
	        		                    "\n" + "cipherTxt = " + cipherTxt);
	             encryptTextArea.setCaretPosition(0);
	           }
		       else
		       {
		          // Print in text area
		          enterTextArea.setText("Please enter only mystery10 letters: a, i, u, k, l, n, p, s, t, v.");
		          enterTextArea.setCaretPosition(0);
		       }
	       }
	      else if (evt.getSource() == decryptButton)
	      {
	    	   enterTxt = enterTextArea.getText();
		       if(inAlphabetWords(enterTxt))
	           {
	              decipherTxt = cp.permOfStr(decryptKey, cipherTxt); 
	              // Print in text area
	              decryptTextArea.setText( "cipherTxt = " + cipherTxt +
	        		                       "\n" + "decryptKey = " + Arrays.toString(decryptKey) +
	        		                       "\n" + "decipherTxt = " + decipherTxt +
	        		                       "\n" + "Note that unless you speak mystery10 you can't see the words!" +
	        		                       "\n" + plaintextExample.toString() +
	        		                       "\n" + "Unless you speak mystery10 you can't translate seen words!" +
	                                       "\n" + cp.toEnglish(plaintextExample).toString());
	              decryptTextArea.setCaretPosition(0);
	           }
	      }
	      else if (evt.getSource() == resetButton)
	      {
		      enterTextArea.setText("Please enter only mystery10 letters: a, i, u, k, l, n, p, s, t, v.");
		      encryptTextArea.setText("");
		      decryptTextArea.setText("");
	      }

	} // End of actionPerformed() method	
	
	// Test for lowercase words in standard English. 
	public static boolean isLowerCaseAlpha(String str)
	{
		int n = str.length();
		if(n > 0)
		{
		   for(int i = 0; i < n; i++)
           {
              if((int)str.charAt(i) < 97 || (int)str.charAt(i) > 122)
              {
            	  return false;
              }    
           }
		   return true;
        }
		return false;
	}
	
	// Test for lowercase words in the specified alphabet, e.g. mystery10. 
	public boolean inAlphabetWords(String str)
	{
		int n = str.length();
		if(n > 0)
		{
		   for(int i = 0; i < n; i++)
           {
              if(!alphabet.contains(str.charAt(i)))
              {
            	  return false;
              }    
           }
		   return true;
        }
		return false;
	}
	// Must use a main method to run a java program.
	public static void main(String[] args) 
    {
		System.out.println("please wait, loading App...");
		new CryptoPermBasicGUI("Permutation Cipher App");
	    System.out.println("...Ready!");
    }

}


/********* CryptoGenKeyGUI.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 *
 */

package CryptoGUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import LexiconData.LexiconData;
import LexiconData.Ntuple;
import LexiconData.NtupleComparator;
import LexiconData.ReadWriteFile;

@SuppressWarnings("serial")

// Version 3.0

// Setup CryptoGenKeyGUI class as a subclass of a JFrame GUI
// and implement the ActionListener interface.
public class CryptoGenKeyGUI extends JFrame implements ActionListener 
{
	   // Instance variables for the JFrame GUI components.
       // These instance variables are only protected in
	   // order to be available to override the actionPerformed
	   // method in any subclass. 
	   protected JTextArea generateKeyTextArea; // Added to CryptoGUI
	   protected JTextArea enterTextArea;
	   protected JTextArea encryptTextArea;
	   protected JTextArea decryptTextArea;
	   protected JButton generateKeyButton;     // Added to CryptoGUI
	   protected JButton encryptButton;
	   protected JButton decryptButton;
	   protected JButton resetButton;
	   
	   private Dimension frameDim;
	   private Dimension textScrollerDim;
	   private Dimension buttonPanelDim;
	   	   
	   // Constructor sets up JFrame GUI window and its components
	   public CryptoGenKeyGUI(String titleJFrame)
	   {
		    // Set relative sizes: JFrame, textScrollers, buttonPanels
		    frameDim = new Dimension(400,640);  // Needed for GenKey Button
		    //frameDim = new Dimension(400,490);
		    textScrollerDim = new Dimension(380,100);
	    	buttonPanelDim = new Dimension(150,100);
	    	
	    	// Use inherited JFrame methods to setup Outer JFrame Container
	    	this.setTitle(titleJFrame);
	    	this.setLayout(new FlowLayout());
	    	this.getContentPane().setBackground(new Color(0,100,128));
	    	this.setPreferredSize(frameDim);
	    	this.setLocation(50,50);
	    	
	    	// Create Master JPanel to be inserted (below) into Outer JFrame Container
	    	JPanel masterPanel = new JPanel();
	    	masterPanel.setLayout(new BoxLayout(masterPanel, BoxLayout.Y_AXIS));
	    	masterPanel.setBackground(new Color(0,100,128));
	    	masterPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
	    	
	   // Start Code Added to CryptoGUI	    	
	    	// generateKey TextArea GUI Components
	    	generateKeyTextArea = initJTextArea( 4, 50, "Press [GENERATE KEY]");
	    	masterPanel.add(initJTextScroller(generateKeyTextArea,textScrollerDim));
	    	
		    // generateKey Button GUI Components
	    	generateKeyButton = new JButton("GENERATE KEY");
	    	masterPanel.add(initJButtonPanel(generateKeyButton, buttonPanelDim));
		    generateKeyButton.addActionListener(this);
	   // End Code Added to CryptoGUI		    
		    // enter TextArea GUI Components
		    enterTextArea = initJTextArea( 4, 50, "[Enter plaintext]");
	    	masterPanel.add(initJTextScroller(enterTextArea,textScrollerDim));
		    
		    // encrypt Button GUI Components
	    	encryptButton = new JButton("ENCRYPT");
	    	masterPanel.add(initJButtonPanel(encryptButton, buttonPanelDim));
		    encryptButton.addActionListener(this);
		    
		    // encrypt TextArea GUI Components
		    encryptTextArea = initJTextArea( 4, 50, "");
	    	masterPanel.add(initJTextScroller(encryptTextArea,textScrollerDim));
		    
		    // decrypt Button GUI Components
	    	decryptButton = new JButton("DECRYPT");
	    	masterPanel.add(initJButtonPanel(decryptButton, buttonPanelDim));
		    decryptButton.addActionListener(this);
		    
		    // decrypt TextArea GUI Components
		    decryptTextArea = initJTextArea( 4, 50, "");
	    	masterPanel.add(initJTextScroller(decryptTextArea,textScrollerDim));
		    
		    // reset Button GUI Components
	    	resetButton = new JButton("RESET");
	    	masterPanel.add(initJButtonPanel(resetButton, buttonPanelDim));
		    resetButton.addActionListener(this);
		    
	    	// JFrame <- Master JPanel
		    // Finish setting up JFrame GUI
		    this.add(masterPanel);
	        this.pack();
	        this.setVisible(true);
	        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 } // End of constructor

	     // method to setup masterPanel JTextArea
	     public JTextArea initJTextArea( int rows, int cols, String text)
	     { 
	    	 JTextArea textArea = new JTextArea(rows,cols);
			 textArea.setBackground(Color.WHITE);
			 textArea.setText(text);
			 textArea.setCaretPosition(0);
			 return textArea;
	     }
	     
	     // method to setup masterPanel JScrollPane for JTextArea
	     public JScrollPane initJTextScroller(JTextArea textArea,Dimension dim)
	     { 
	    	 JPanel panel = new JPanel(new BorderLayout());
			 panel.add(textArea, BorderLayout.CENTER);
			 JScrollPane scroller = new JScrollPane(panel);
			 scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			 scroller.setPreferredSize(dim);
			 return scroller;
	     }
	     
	     // method to setup masterPanel JPanel for JButton
	     public JPanel initJButtonPanel(JButton button, Dimension dim)
	     {
	        JPanel panel = new JPanel(new BorderLayout());
		    panel.setBackground(new Color(0,100,128));	    
		    panel.add(button,BorderLayout.CENTER);
		    panel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
		    panel.setMaximumSize(dim);
		    return panel;
	     }
	     
	     // Required and essential implementation of abstract  
	     // actionPerformed method in ActionListener interface.
	     // Here it is implemented as a stub with the intention
	     // of it being overridden in each subclass.
	     public void actionPerformed(ActionEvent evt)
	 	 {
	 		 
	 	 }	     
	      
/*    
		// Uncomment to test GUI shell. Note that there is
	    // no button functionality unless the actionPerformed
	    // method is populated or overidden in a subclass.
	    public static void main(String[] args) 
	    {
		   new CryptoGenKeyGUI("CryptoGenKeyGUI App Test");
	    }
*/
}


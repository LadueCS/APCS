/********* ModnOpTable.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 *
 *  Java GUI Rule of Thumb: In order to
 *  control the size and placement of
 *  JComponents, first embed JComponents
 *  inside of JPanels, inside of JPanels,
 *  ... inside of a Master JPanel using
 *  a BoxLayout which stacks them uniformly.
 *  Then add the Master JPanel to a JFrame.
 */

package CryptoGUI;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import LexiconData.LexiconData;
import LexiconData.Ntuple;
import LexiconData.NtupleComparator;
import LexiconData.ReadWriteFile;

// Version 4.0

@SuppressWarnings("serial")
//Setup ModnOpTable class as a subclass of a JFrame GUI 
//and implement the ActionListener interface
public class ModnOpTable extends JFrame implements ActionListener
{
	private Object[][] data;
	private String[] header;
    private JTable table;
    private JTextArea tableTextArea;
    private JTextArea gcdTextArea;
    private JTextArea unitsTextArea;
    private String originalText;
    private String gcdOriginalText;
    private String unitsOriginalText;
    private JButton tableButton;
    private JButton gcdButton;
    private JButton unitsButton;
    private int scrollerWidth;
    private int scrollerHeight;
    private Dimension scrollerDim;
    private Dimension layoutDim;
    private int xPos = 50;
    private int yPos = 50;
    private int modMax = 100;
    private static int frameNum = 0;
 
 // Constructor to setup JFrame GUI for subclass ModnOpTable - Op Tables
 public ModnOpTable(int modn, String op) 
 {
 	data = modnArith(modn,op);
 	header = blankHeader(modn);
 	table = new JTable(data,header);
 	scrollerWidth = Math.min((modn+1)*20, 800);
 	scrollerHeight = Math.min((modn+1)*18, 400);
 	scrollerDim = new Dimension(scrollerWidth,scrollerHeight);
 	layoutDim = new Dimension(Math.min(scrollerWidth+100,900),Math.min(scrollerHeight+280,700));
 	frameNum++;
 	initJFrameGUI(modn, op);
 }
 
 // Constructor to setup JFrame GUI for subclass ModnOpTable - groupOfUnits Table
 public ModnOpTable(int modn, String op, ArrayList<Integer> units) 
 {
 	data = modnUnits(modn,units);
 	header = blankHeader(units.size());
 	table = new JTable(data,header);
 	scrollerWidth = Math.min((units.size()+1)*20, 800);
 	scrollerHeight = Math.min((units.size()+1)*18, 400);
 	scrollerDim = new Dimension(scrollerWidth,scrollerHeight);
 	layoutDim = new Dimension(Math.min(scrollerWidth+100,900),Math.min(scrollerHeight+280,700));
 	frameNum++;
 	initJFrameGUI(modn, op);
 }
 
 public void initJFrameGUI(int n, String str)
 {
 	// Use inherited JFrame methods to setup Outer JFrame Container
 	this.setTitle("Mod n = " + n + " Op Table");
 	this.setLayout(new FlowLayout());
 	this.getContentPane().setBackground(new Color(0,100,128));
 	this.setPreferredSize(layoutDim);
 	this.setLocation(xPos+50*frameNum,yPos);
 	
 	// Create Master JPanel to be inserted (below) into Outer JFrame Container
 	JPanel masterPanel = new JPanel();
 	masterPanel.setLayout(new BoxLayout(masterPanel, BoxLayout.Y_AXIS));
 	masterPanel.setBackground(new Color(0,100,128));
 	
 	// GUI Creation:
 	// Master JPanel <- Table JPanel
 	// Create Table JPanel and insert it into Master Panel
 	// Use other JComponents and JTable methods to embed 
 	// and setup JTable, and then add JTable to masterPanel
 	// In particular, set JTable max column width, and create 
 	// vertical and horizontal scrollbars.
	JPanel tablePanel = new JPanel(new BorderLayout());
	tablePanel.setBackground(Color.WHITE);
	for(int i = 0; i < table.getColumnModel().getColumnCount(); i++)
	{
	    table.getColumnModel().getColumn(i).setMaxWidth(20);
	}
	tablePanel.add(table, BorderLayout.CENTER);
	JScrollPane tableScroller = new JScrollPane(tablePanel);
	tableScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	if(n == 2)
	    tableScroller.setPreferredSize(new Dimension(100,70));
	else
	    tableScroller.setPreferredSize(scrollerDim);
	masterPanel.add(tableScroller);
	    
	// GUI Creation:
 	// Master JPanel <- (JTextArea + JButton) JPanel
	// Create (JTextArea + JButton) JPanel and insert it into Master Panel
	JPanel textPlusButtonPanel = new JPanel(new BorderLayout());
	textPlusButtonPanel.setBackground(new Color(0,100,128));
	    
	tableTextArea = new JTextArea(2,10);
	originalText = "Current pair: " + n + "," + str;
    tableTextArea.setText(originalText);
	tableTextArea.setBackground(Color.white);
	textPlusButtonPanel.add(tableTextArea,BorderLayout.NORTH);
	    
	tableButton = new JButton("   New Op Table   ");
	textPlusButtonPanel.add(tableButton,BorderLayout.SOUTH);
	textPlusButtonPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
	textPlusButtonPanel.setMaximumSize(new Dimension(200,100));
	masterPanel.add(textPlusButtonPanel);
	tableButton.addActionListener(this);
	    
	// GUI Creation:
 	// Master JPanel <- gcd (JTextArea + JButton) JPanel
	// Create gcd (JTextArea + JButton) JPanel and insert it into Master Panel
	JPanel gcdTextPlusButtonPanel = new JPanel(new BorderLayout());
	gcdTextPlusButtonPanel.setBackground(new Color(0,100,128));
	    
	gcdTextArea = new JTextArea(2,10);	   
	gcdOriginalText = "Enter: a,b";
    gcdTextArea.setText(gcdOriginalText);
	gcdTextArea.setBackground(Color.white);
	gcdTextPlusButtonPanel.add(gcdTextArea,BorderLayout.NORTH);
	    
	gcdButton = new JButton("Compute gcd(a,b)");
	gcdTextPlusButtonPanel.add(gcdButton,BorderLayout.SOUTH);
	gcdTextPlusButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
	gcdTextPlusButtonPanel.setMaximumSize(new Dimension(200,100));
	masterPanel.add(gcdTextPlusButtonPanel);
	gcdButton.addActionListener(this);
	    
	// GUI Creation:
 	// Master JPanel <- units (JTextArea + JButton) JPanel
	// Create units (JTextArea + JButton) JPanel and insert it into Master Panel
	JPanel unitsTextPlusButtonPanel = new JPanel(new BorderLayout());
	unitsTextPlusButtonPanel.setBackground(new Color(0,100,128));
	    
	unitsTextArea = new JTextArea(2,10);
	unitsOriginalText = "Enter: n (> 1)";
    unitsTextArea.setText(unitsOriginalText);
	unitsTextArea.setBackground(Color.white);
	JScrollPane unitsScroller = new JScrollPane(unitsTextArea);
	unitsScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	unitsTextPlusButtonPanel.add(unitsScroller,BorderLayout.NORTH);
	    
	unitsButton = new JButton("Compute units(n)");
	unitsTextPlusButtonPanel.add(unitsButton,BorderLayout.SOUTH);
	unitsTextPlusButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
	unitsTextPlusButtonPanel.setMaximumSize(new Dimension(200,100));
	masterPanel.add(unitsTextPlusButtonPanel);
	unitsButton.addActionListener(this);
	    
	// GUI Creation:
 	// JFrame <- Master JPanel
	// Finish setting up JFrame GUI
	this.add(masterPanel);
    this.pack();
    this.setVisible(true);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 }
 
 // Implementation of abstract actionPerformed method 
 // in ActionListener interface
 public void actionPerformed(ActionEvent evt)
 { 
 	if (evt.getSource() == tableButton)
	    {
		    String str = tableTextArea.getText().trim();
		    int index = str.indexOf(",");
		    String op = str.substring(index+1);
		    if(index > 0 && (op.equals("+") || op.equals("*")))
		    {
		      try
		      {
		    	  int m = Integer.parseInt(str.substring(0,index));
		    	  if(2 <= m && m <= modMax)
			      {	
			         new ModnOpTable(m,op);
			         tableTextArea.setText(originalText);
			         if(frameNum == 10) frameNum = 0;
			      }
		      }
		      catch(NumberFormatException e)
		      {
		         tableTextArea.setText("Enter only a pair: n,op");
		      } 
		    }
		    else
		    {
		    	tableTextArea.setText("Enter only a pair: n,op");
		    }
	     }
 	     else if (evt.getSource() == gcdButton)
	     {
		    String str = gcdTextArea.getText().trim();
		    int index = str.indexOf(",");
		    if(index > 0)
		    {
		      try
		      {
		    	  int a = Integer.parseInt(str.substring(0,index));
		    	  int b = Integer.parseInt(str.substring(index+1));
		    	  int gcd = gcd(a,b);
		    	  gcdTextArea.setText("gcd("+a+","+b+") = " + gcd);
		      }
		      catch(NumberFormatException e)
		      {
		         gcdTextArea.setText("Enter only a pair: a,b");
		      } 
		    }
		    else
		    {
		    	gcdTextArea.setText("Enter only a pair: a,b");
		    }
	     }
 	     else if (evt.getSource() == unitsButton)
	     {
		    String str = unitsTextArea.getText().trim();
		    try
		    {
		    	int n = Integer.parseInt(str.substring(0));
		    	if(n > 1)
		    	{
		    	ArrayList<Integer> units = groupOfUnits(n);
		    	unitsTextArea.setText("units("+n+") = "+ units.toString()+
		    			               "\n|units("+n+")| = "+ units.size());
		    	ModnOpTable unitsOp = new ModnOpTable(n, "*", groupOfUnits(n)); 
		    	unitsOp.unitsTextArea.setText("units("+n+") = "+ units.toString()+
			                                  "\n|units("+n+")| = "+ units.size());
		    	}
		    	else
		    	{
		    		unitsTextArea.setText("Enter only: n (> 1)");
		    	}
		    }
		    catch(NumberFormatException e)
		    {
		       unitsTextArea.setText("Enter only: n (> 1)");
		    } 
		 }
		    
	     
	} // End of actionPerformed() method
 
 public Object[][] modnUnits(int n, ArrayList<Integer> units)
 {
 	Object[][] table = null;
    if(2 <= n && n <= modMax)
	{  
	    int k = units.size();
		table = new Object[k+1][k+1];
		   
		table[0][0] = "*";
		for(int i = 1; i < k+1; i++)
		{
			table[0][i] = ""+ units.get(i-1);
			table[i][0] = ""+ units.get(i-1);
			for(int j = 1; j < k+1; j++)
			{
				table[i][j] = "" + ((units.get(i-1)*units.get(j-1)) % n);
			}
		}
	}
	return table;
 }
 
 public ArrayList<Integer> groupOfUnits(int n)
 {
 	ArrayList<Integer> units = null;
 	if(n > 1)
 	{
 		units = new ArrayList<Integer>();
 		for(int i = 1; i < n; i++)
 		{
 			if(gcd(i,n) == 1)
 				units.add(i);
 		}		
 	}
 	return units;
 }
 
 public int gcd(int a, int b)
 {
     if(a < 0) a = -a;
     if(b < 0) b = -b;
     while(b != 0)
     {
         a %= b;
         if(a == 0) return b;
         b %= a;
     }
     return a;
 }
 
 public String[] blankHeader(int n)
 {
 	String[] blankHeader = new String[n+1];
 	for(int i = 0; i < n+1; i++)
 	{
 		blankHeader[i] = " ";
 	}
 	return blankHeader;
 }
 
 public Object[][] modnArith(int n, String op)
{
 	Object[][] table = null;
	if(2 <= n && n <= modMax)
	{  
		table = new Object[n+1][n+1];
		if(op.equals("+"))
		{
		   table[0][0] = "+";
		   for(int i = 1; i < n+1; i++)
		   {
			   table[0][i] = ""+ (i-1);
			   table[i][0] = ""+ (i-1);
			   for(int j = 1; j < n+1; j++)
			   {
				  table[i][j] = "" + ((i + j - 2) % n);
			   }
			}
	     }
	     else if(op.equals("*"))
		 {
			table[0][0] = "*";
			for(int i = 1; i < n+1; i++)
			{
			   table[0][i] = ""+ (i-1);
			   table[i][0] = ""+ (i-1);
			   for(int j = 1; j < n+1; j++)
			   {
				  table[i][j] = "" + (((i-1)*(j-1)) % n);
			   }
		     }
		  }	   
    }
    return table;
 }
 
 public static void main(String[] args)
 {
 	new ModnOpTable(12,"+");
 	System.out.println("Running ModnOpTable App in Frame GUI");
 }
}

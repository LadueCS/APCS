/********* CryptoRSAGUI.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 *
 */

package CryptoGUI;
import java.awt.event.ActionEvent;
import java.math.BigInteger;
import java.util.Random;
import LexiconData.LexiconData;
import LexiconData.Ntuple;
import LexiconData.NtupleComparator;
import LexiconData.ReadWriteFile;

@SuppressWarnings("serial")

// Version 3.0

// Setup CryptoRSAGUI class as a subclass of a JFrame GUI
// and implement the ActionListener interface
public class CryptoRSAGUI extends CryptoGenKeyGUI
{
	     // Instance variable for the CryptoRSAGUI calculations
	     private BigInteger one = new BigInteger("1");
	     private BigInteger p, q, n, e, d, pm1, qm1, pm1qm1, gcd, plainTxt, cipherTxt, decryptTxt;
	     private Random rnd;
	     private String enterTxt;
	   
	     // Constructor sets up JFrame GUI window and its components
	     public CryptoRSAGUI(String title)
	     {
		     super(title);

	     } // End of constructor

	     // Required and essential implementation of abstract  
	     // actionPerformed method in ActionListener interface
		 // in superclass CryptoGUI. Here we override this method
		 // which is just a stub in the superclass.
		 // Note also that TextArea & Button variables are passed
		 // from the superclass to this subclass by declaring them
		 // protected instead of private (see the superclass).
		 public void actionPerformed(ActionEvent evt)
		 {
		     if (evt.getSource() == generateKeyButton)
		     {
		        //Generate public and private keys
		        do {
		             // public BigInteger(int bitLength, int certainty, Random rnd)
		             // constructs a randomly generated positive BigInteger 
		             // that is probably prime, with the specified bitLength.
		             // bitLength - bitLength of the returned BigInteger.
		             // certainty - a measure of the uncertainty that the  
		             // caller is willing to tolerate. The probability that 
		             // the new BigInteger represents a prime number will  
		             // exceed (1 - (1/2)^certainty). The execution time of 
		        	 // this constructor is proportional to the value of this 
		        	 // parameter.
		             // rnd - source of random bits used to select candidates 
		        	 // to be tested for primality.
		        	 rnd = new Random();
		             p = new BigInteger(512, 20, rnd); // generate p
		             q = new BigInteger(512, 20, rnd); // generate q
		             e = new BigInteger(212, 20, rnd); // generate e
		             n = p.multiply(q);                // n = p*q
		             pm1 = p.subtract(one);            // pm1 = p-1
		             qm1 = q.subtract(one);            // qm1 = q-1
		             pm1qm1 = pm1.multiply(qm1);       // pm1qm1 = (p-1)*(q-1)
		             gcd = e.gcd(pm1qm1);              // gcd = gcd(e,(p-1)*(q-1))
		         }
		         // Compare BigIntegers: gcd = gcd(e,(p-1)*(q-1)) and one = 1.
		         // If gcd != 1, i.e. e is not relatively prime to (p-1)*(q-1),
		         // then go back up and regenerate everything.
		         while(gcd.compareTo(one) != 0); // end do_while

		         // (1) When e is relatively prime to (p-1)*(q-1) = phi(n), 
		         // then there exists d such that d*e = 1 (mod (p-1)*(q-1)).
		         // (2) Also, by Euler's Theorem, if gcd(a,n) = 1, 
		         // then a^phi(n) = 1 (mod n).
		         // Both of which we need in order to ensure that: 
                 //          plainTxt^ed (mod n) = plainTxt
		         d = e.modInverse(pm1qm1);
		         
		         // Print n, d, e in appropriate text area
			     generateKeyTextArea.setText("n = "+ n.toString()+
		                                    "\n"+"e = "+e.toString()+
		                                    "\n"+"d = "+d.toString());
			     generateKeyTextArea.setCaretPosition(0);
		      }
		      else if (evt.getSource() == encryptButton)
		      {
    	         enterTxt = enterTextArea.getText();
		    	 if(isDecimalNumeral(enterTxt))
		         {
		            plainTxt = new BigInteger(enterTxt);
		            cipherTxt = plainTxt.modPow(e,n);    // cipherTxt = plainTxt^e (mod n)
		            // Print in text area
		            encryptTextArea.setText(cipherTxt.toString());
		            encryptTextArea.setCaretPosition(0);
		         }
		    	 else
		         {
			         // Print in appropriate text area
			         enterTextArea.setText("Please Enter Only Integers: 0-9");
			         enterTextArea.setCaretPosition(0);
			     }
		      }
		      else if (evt.getSource() == decryptButton)
		      {
		    	  enterTxt = enterTextArea.getText();
			      if(isDecimalNumeral(enterTxt))
			      {
		             decryptTxt = cipherTxt.modPow(d,n); // decryptTxt = cipherTxt^d (mod n)
		                                                 //            = plainTxt^ed (mod n)
		                                                 //            = plainTxt (mod n)
		             decryptTextArea.setText("Plain Text = " + decryptTxt.toString());
		             decryptTextArea.setCaretPosition(0);
		          }
		      }
		      else if (evt.getSource() == resetButton)
		      {
			    generateKeyTextArea.setText("Press [GENERATE KEY]");
			    enterTextArea.setText("Enter Only Integers: 0-9");
			    encryptTextArea.setText("");
			    decryptTextArea.setText("");
		      }

		    } // End of actionPerformed() method
		 
		    // Test for base 10 numerals. 
			public static boolean isDecimalNumeral(String str)
			{			   
				int n = str.length();
				if(n > 0)
				{
				   for(int i = 0; i < n; i++)
		           {
					   if(((int) str.charAt(i) < 48) || ((int) str.charAt(i) > 57))
		              {
		            	  return false;
		              }    
		           }
				   return true;
		        }
				return false;
			}
			
		    // Run application here 
	        public static void main(String[] args) 
	        {
		       new CryptoRSAGUI("RSA Cipher App");
		       System.out.println("Running CryptoRSAGUI App in Frame GUI");
		       
		       System.out.println("\nAn Example with small checkable numbers:");
		       BigInteger one = new BigInteger("1"); 
		       BigInteger p = new BigInteger("11"); // generate p
		       BigInteger q = new BigInteger("29"); // generate q
		       BigInteger n = p.multiply(q);
		       BigInteger e = new BigInteger("3");   // generate e
		       BigInteger d = new BigInteger("213"); // generate d
		       BigInteger b = new BigInteger("100"); // generate b
		       BigInteger c = new BigInteger("100"); // generate c
		       
		       System.out.println("p = " + p);
		       System.out.println("q = " + q);
		       System.out.println("n = " + n);
		       System.out.println("e = " + e);
		       System.out.println("d = " + d);
		       System.out.println("b = " + b);
		       System.out.println("c = " + c);
		       System.out.println("e.multiply(d).mod(n) = " + e.multiply(d).mod(n));
		       System.out.println("e.modInverse(n) = " + e.modInverse(n));
		       System.out.println("b.modPow(e.multiply(d),n) = " + b.modPow(e.multiply(d),n));
		       BigInteger pm1 = p.subtract(one);            // pm1 = p-1
		       BigInteger qm1 = q.subtract(one);	       
		       System.out.println("b.modPow(e.multiply(d),p.multiply(q)) = " + b.modPow(e.multiply(d),p.multiply(q)));
		       c = b.modPow(e,n);
		       System.out.println("c = b.modPow(e,n) = " + c);
		       System.out.println("c.modPow(d,n) = " + c.modPow(d,n));
	        }
}

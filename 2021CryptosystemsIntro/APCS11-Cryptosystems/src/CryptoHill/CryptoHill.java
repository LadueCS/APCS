/********* CryptoHill.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to 2021 John Pais. All rights reserved.
 *  Permission is granted to copy and modify for non-commerical use.
 *  
 *  A Partitioned-Hill Cryptosystem uses a sequence of randomly generated
 *  matrix affine ciphers, Ax + B, of different random dimensions, that 
 *  are determined by a randomly generated partition of the plaintext string.
 *  
 *  A Partitioned-Hill cipher is a symmetric random block cipher that by
 *  design has good intrinsic security due to the difficulty of searching
 *  all invertible (mod k) matrices of dimension n >= 3, e.g. a random 
 *  generation of 1 billion 3x3 invertible matrices (mod 26) will produce
 *  approximatly 1 million such matrices. In addition, even restricting
 *  to nxn matrices with 3 <= n <= 9, to ensure both significant search 
 *  complexity and easy computability of a single matrix inverse (mod k), 
 *  produces another significant search barrier in order to find the 
 *  sequence of partition numbers corresponding to a randomly generated 
 *  partition of the plaintext string.
 *  
 *  Note that this work is an especially useful example of significant 
 *  STEM integration that combines current and important subject matter 
 *  from linear algebra, number theory, computer science, cryptology, 
 *  and cybersecurity. 
 *  
 */

package CryptoHill;
import java.util.*;
import CryptoAffine.CryptoAffine;
import LexiconData.LexiconData;
import LexiconData.Ntuple;
import LexiconData.NtupleComparator;
import LexiconData.ReadWriteFile;

public class CryptoHill 
{
   protected List<Character> alphabet;
   protected int alphaSize;
   private String dirPath;
   private List<Ntuple> lexicon = new ArrayList<Ntuple>();
   private List<String> plaintextWords = new ArrayList<String>();
   private List<Integer> units = new ArrayList<Integer>();
   
   private CryptoAffine ca;
   
   public CryptoHill(List<Character> alphabet, String dirPath, String lexicon)
   {
		this.alphabet = alphabet;
		this.alphaSize = alphabet.size();
		this.dirPath = dirPath;
		ca = new CryptoAffine(alphabet, dirPath, lexicon);
		plaintextWords = ca.getPlaintextWords();
		units = ca.groupOfUnits(alphaSize);
   }
   
   // Problem 1.
   public CryptoAffine getCA()
   {
	   // insert your code here
   }
   
   // Problem 2. transpose matrix
   public long[][] transpose(long[][] a) 
   {
	  // insert your code here
   }	
   
   // Problem 3. sum of two vectors
   public static long[] sum(long[] x, long[] y) 
   {
	  // insert your code here
   }
   
   // Problem 4. matrix multiplication: c = a * b
   public long[][] multiply(long[][] a, long[][] b) 
   {
	  // insert your code here
   }

   // Problem 5. matrix-vector multiplication: a * x
   public long[] multiply(long[][] a, long[] x) 
   {
	  // insert your code here
   }

   // Problem 6. vector-matrix multiplication: x^T * a
   public long[] multiply(long[] x, long[][] a) 
   {
	  // insert your code here
   }
	
	// Problem 7. method to create submatrix of mat (w.r.t. row p and col q) 
    // written into temp
	public void submatrix(long[][] mat, long[][] temp, int p, int q)
	{
		// insert your code here
	}
	
	// Problem 8. recursive method for determinant of square matrix
	public long det(long[][] mat, int n)
	{
		// insert your code here
	}
	
   // Problem 9. matrix adjugate of a square matrix
   public long[][] adjugate(long[][] a) 
   {
	  // insert your code here
   }	
	
   // Problem 10. scalar times matrix
   public long[][] scalarMult(long r, long[][] a) 
   {
	  // insert your code here
   }
   
	// Problem 11. display matrix
	public void display(long[][] mat, int row, int col)
	{
		// insert your code here
	}  
	
   // Vector & Matrix Methods mod k (alphaSize) 
   // Needed for Hill Encryption/Decryption 
   
   // Problem 12. scalar times matrix mod k
   public long[][] scalarMultMod(long r, long[][] a, long k) 
   {
	  // insert your code here
   }  

   // Problem 13. scalar times vector mod k
   public long[] scalarMultMod(long r, long[] v, long k) 
   {
	  // insert your code here
   }     
   
   // Problem 14. matrix mod k
   public long[][] matrixMod(long[][] a, long k)
   {
	  // insert your code here
   }

   // Problem 15. vector mod k
   public long[] vectorMod(long[] v, long k)
   {
	  // insert your code here
   }   
   
   // Problem 16. matrix inverse mod k when it exists
   // note that we use two methods from our CryptoAffine object
   public long[][] matMultInvMod(long[][] a, long k) 
   {
	  // insert your code here
   }
	
   // Problem 17. vector additive inverse mod k
   public long[] vecAddInvMod(long[] v, long k) 
   {
	  // insert your code here
   }   
   
   // Problem 18. matrix multiplication mod k: c = a * b 
   public long[][] multiplyMod(long[][] a, long[][] b, long k) 
   {
	  // insert your code here
   }

   // Problem 19. matrix-vector multiplication mod k: a * x
   public long[] multiplyMod(long[][] a, long[] x, long k) 
   {
	  // insert your code here
   }   
   
    // Problem 20. generate random n by n matrix invertible mod k 
	public long[][] genRndMatrixMod(int n, int k)
	{
		// insert your code here
	}
       
    // Problem 21. generate random n vector mod k 
	public long[] genRndVectorMod(int n, int k)
	{
		// insert your code here
	}
	
	// Problem 22. precondition: matKey[n][n], vecKey[n], blocksize = n 
	// = str.length(), all characters in str are in the alphabet
	public String hillEncryptBlock(long[][] matKey, long[] vecKey, String block)
	{
		int n = block.length();
		long k = alphaSize;
		char[] plainChars = block.toCharArray();
		long[] plainIndexes = new long[n];
		long[] cipherIndexes = new long[n];
		char[] cipherChars = new char[n];
		
		// create plainIndexes vector of plainChars
		for(int i = 0; i < n; i++)
		{
			// insert your code here	
		}
		
		// encrypt plainIndexes vector
		
		// insert your code here
		
		// create cipherChars array of characters
		for(int i = 0; i < n; i++)
		{
			// insert your code here		
		}
		
		// return ciphertext string block
		
		// insert your code here
	}
   
	// Problem 23. precondition: matKey[n][n], vecKey[n], blocksize = n 
	// = str.length(), all characters in str are in the alphabet
	public String hillDecryptBlock(long[][] matKey, long[] vecKey, String block)
	{
		int n = block.length();
		long k = alphaSize;
		char[] cipherChars = block.toCharArray();
		long[] cipherIndexes = new long[n];
		long[] plainIndexes = new long[n];
		char[] plainChars = new char[n];
		
		// create cipherIndexes vector of cipherChars
		for(int i = 0; i < n; i++)
		{
			// insert your code here		
		}
		
		// decrypt cipherIndexes vector
		
		// insert your code here
		
		// create plainChars array of characters
		for(int i = 0; i < n; i++)
		{
			// insert your code here		
		}
		
		// return plaintext string block
		
		// insert your code here
	}
}
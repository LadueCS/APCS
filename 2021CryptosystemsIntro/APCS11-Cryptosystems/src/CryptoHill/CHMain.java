/********* CHMain.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to 2021 John Pais. All rights reserved.
 *  Permission is granted to copy and modify for non-commerical use.
 *  
 *  A Partitioned-Hill Cryptosystem uses a sequence of randomly generated
 *  matrix affine ciphers, Ax + B, of different random dimensions, that 
 *  are determined by a randomly generated partition of the plaintext string.
 *  
 *  A Partitioned-Hill cipher is a symmetric random block cipher that by
 *  design has good intrinsic security due to the difficulty of searching
 *  all invertible (mod k) matrices of dimension n >= 3, e.g. a random 
 *  generation of 1 billion 3x3 invertible matrices (mod 26) will produce
 *  approximatly 1 million such matrices. In addition, even restricting
 *  to nxn matrices with 3 <= n <= 9, to ensure both significant search 
 *  complexity and easy computability of a single matrix inverse (mod k), 
 *  produces another significant search barrier in order to find the 
 *  sequence of partition numbers corresponding to a randomly generated 
 *  partition of the plaintext string.
 *  
 *  Note that this work is an especially useful example of significant 
 *  STEM integration that combines current and important subject matter 
 *  from linear algebra, number theory, computer science, cryptology, 
 *  and cybersecurity. 
 *  
 */

package CryptoHill;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import LexiconData.LexiconData;
import LexiconData.Ntuple;
import LexiconData.NtupleComparator;
import LexiconData.ReadWriteFile;

public class CHMain 
{
  public static void main(String[] args)
  {
	 // Start Clock
    long start = System.currentTimeMillis();
    
    // Create alphabet character array
    List<Character> alphabet = new ArrayList<Character>();
    for(int i = 97; i <= 122; i++)
    {
    	alphabet.add((char)i);
    }
    String dirPath = "src//CryptoAffine//";
    String lexicon = "english26-3.txt";
    
// Uncomment and test as you complete Problems in CryptoHill.java
// Complete Problems 1-20 before starting here.
/**        
    // Create CryptoHill object
	CryptoHill ch = new CryptoHill(alphabet, dirPath, lexicon);

	
	// test matrix mod methods
	int n = 5; // matrix dim
	int k = 26; // mod
    long[][] matMod = ch.genRndMatrixMod(n, k);
    System.out.println("matMod:");
    ch.display(matMod,n,n);
    System.out.println();
 
    long[][] adjugatematMod = ch.adjugate(matMod);
    System.out.println("adjugatematMod:");
    ch.display(adjugatematMod,n,n);
    System.out.println();
    
    long detMod = ch.det(matMod,n) % k;
    if (detMod < 0) detMod = k + detMod;
    System.out.println("detmatMod:");
    System.out.println(ch.det(matMod,n) + " " + detMod);
    System.out.println();
  
    long[][] inv = ch.matMultInvMod(matMod,k);
    System.out.println("invmatMod:");
    ch.display(inv,n,n);
    System.out.println();
    
    System.out.println("matMod*invmatMod:");
    ch.display(ch.multiplyMod(matMod, inv, k),n,n);
    System.out.println();
    
    // Hill Block Cipher Example 0. (First complete Problems 21-23)
    System.out.println("Hill Cipher Example 0. Encrypt/Decrypt Block");
    String plaintext = "hithere";
    System.out.println("plaintext = " + plaintext);
    System.out.println();
    System.out.println("block size n, mod k:");
    n = 7;
    k = 26;
    System.out.println("n = " + n + ", k = " + k);
    System.out.println();
    
    long[][] matKey0 = ch.genRndMatrixMod(n, k);
    System.out.println("matKey0:");
    ch.display(matKey0,n,n);
    System.out.println();
    
    long[] vecKey0 = ch.genRndVectorMod(n, k);
    System.out.println("vecKey0:");
    System.out.println(Arrays.toString(vecKey0));
    System.out.println();
    
    String ciphertext = ch.hillEncryptBlock(matKey0, vecKey0, plaintext);
    System.out.println("ciphertext = " + ciphertext);
    System.out.println();
    
    String decrypttext = ch.hillDecryptBlock(matKey0, vecKey0, ciphertext);
    System.out.println("decrypttext = " + decrypttext);
    System.out.println();
**/    
    
	// End Clock
    long end = System.currentTimeMillis();
    System.out.println("\ntime = " + Math.round(1000*((end-start)/1000.))/1000. + " seconds");
    System.out.println("time = " + Math.round(1000*((end-start)/60000.))/1000. + " minutes"); 	;
  }

}


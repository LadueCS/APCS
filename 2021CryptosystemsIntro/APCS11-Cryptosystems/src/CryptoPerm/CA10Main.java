/********* CA10Main.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 *  
 *  An affine cryptosystem uses an affine, monoalphabetic, symmetric, 
 *  substitution cipher that has a private key and that is vulnerable
 *  to a plaintext attack, i.e. in this case, it is enough to find two 
 *  ciphertext characters corresponding to two plaintext characters, 
 *  in order to determine the private key.
 *  Note also that a language like English is always vulnerable to a 
 *  frequency attack, since certain individual characters and certain 
 *  bigrams occur much more frequently than others.
 *  
 *  This example uses a mystery language with only a ten character 
 *  alphabet, contained in the file mystery10.txt. In addition, as can be
 *  seen by the included Lexicon Data report, this language is an unknown 
 *  intermediate language whose plaintext words cannot be immediately 
 *  translated to English by a non-native speaker. This type of 
 *  cryptanalytic defense is similar to the Navajo language encryption 
 *  used during World War II. The traditional Navajo language is an 
 *  oral language and so only known to Native American Navajos. These
 *  'Navajo code talkers' were able to very quickly encrypt an English
 *  message into Navajo code (800 plaintext Navajo code words) and orally 
 *  transmit the message to another code talker who then quickly decrypted
 *  the message into English. This process was much faster and less error
 *  prone than the Enigma code machines used by the Germans during World 
 *  War II.
 *  
 *  Furthermore, again as can be seen by the included Lexicon Data report,
 *  this mystery language has a structure that is not so vulnerable to a
 *  frequency attack. The 3 vowel frequencies are all about 17-18 % and 
 *  the 7 consonant frequencies are are all about 6-7 %. Also, the bigram 
 *  frequencies are all about 2-3 %, and the word lengths range from 3 to 6.
 *  Note that this file gives examples of the use of an affine cipher with
 *  this 10 character alphabet. However, there are only 36 affine keys for
 *  a 10 character alphabet. In contrast, there are more than 100,000 times
 *  as many permutations of this 10 character alphabet (10!), so in this
 *  CryptoPerm package we also develop a general permutation cryptosystem,
 *  which represents all possible substitution ciphers for this alphabet.
 *  
 */
package CryptoPerm;
import java.util.*;
import CryptoAffine.CryptoAffine;
import LexiconData.LexiconData;
import LexiconData.Ntuple;
import LexiconData.NtupleComparator;
import LexiconData.ReadWriteFile;

public class CA10Main 
{
	public static void main(String[] args) 
	{
		 // Start Clock
	    long start = System.currentTimeMillis();
	    
	    // Create alphabet character array
		Character[] arr = {'a','i','u','k','l','n','p','s','t','v'};
		List<Character> alphabet = new ArrayList<Character>(Arrays.asList(arr));
	    String dirPath = "src//CryptoPerm//";
	    String lexicon = "mystery10.txt";
	    boolean init = false;
	    // Create LexiconData object
	  	LexiconData lex = new LexiconData(alphabet,dirPath,lexicon,init);
	  	String wordInLexicon = "ukuna";
		if(!init)
		{
		   lex.lexiconReport(wordInLexicon);
		}

	    // Create CryptoAffine object
		CryptoAffine ca = new CryptoAffine(alphabet, dirPath, lexicon);
        
		// Test Cryptology Methods 
		testCryptoMethods(ca);

		// End Clock
	    long end = System.currentTimeMillis();
	    System.out.println("\ntime = " + (end-start)/1000. + " seconds");
	}
	
	public static void testCryptoMethods(CryptoAffine ca)
	{
		/*
		System.out.println("\nTest Cryptology Methods");
		System.out.println("ca.getUnits() = " + ca.getUnits());
        int[] randomKey = ca.randomKey(ca.getAlphaSize());
		System.out.println("randomKey = " + Arrays.toString(randomKey));
		System.out.println("ca.inverseKey(randomKey) = " + Arrays.toString(ca.inverseKey(randomKey)));
		System.out.println("ca.getAffineKeySet().size() = " + ca.getAffineKeySet().size());
		for(int[] key : ca.getAffineKeySet())
		{
			System.out.println("key = " + Arrays.toString(key));
		}

		// Example01.
		List<String> plaintext01 = new ArrayList<String>();
		plaintext01.add("atuti");
		plaintext01.add("satisi");
		plaintext01.add("vuvapi");
		plaintext01.add("vivaki");
		plaintext01.add("uvulu");
		System.out.println("\nExample01. Apply random affine key to mystery plaintext to create cyphertext:");
		System.out.println("plaintext = " + plaintext01);
		List<String> ciphertext01 = ca.encrypt(plaintext01);
		System.out.println("ciphertext = " + ciphertext01);
		System.out.println("Decrypt ciphertext trying affine keys that produce mystery plaintext words in lexicon:");
		ca.decrypt(ciphertext01);
		List<String> english01 = ca.toEnglish(plaintext01);
		System.out.println("english = " + english01);
		
		// Example02.
		List<String> plaintext02 = new ArrayList<String>();
		plaintext02.add("nalatu");
		plaintext02.add("lupuva");
		plaintext02.add("unuti");
		plaintext02.add("kisupu");
		plaintext02.add("tavak");
		plaintext02.add("katu");
		plaintext02.add("upapi");
		plaintext02.add("kanisu");
		System.out.println("\nExample02. Apply random affine key to mystery plaintext to create cyphertext:");
		System.out.println("plaintext = " + plaintext02);
		List<String> ciphertext02 = ca.encrypt(plaintext02);
		System.out.println("ciphertext = " + ciphertext02);
		System.out.println("Decrypt ciphertext trying affine keys that produce mystery plaintext words in lexicon:");
		ca.decrypt(ciphertext02);
		List<String> english02 = ca.toEnglish(plaintext02);
		System.out.println("english = " + english02);
     */
	}
}




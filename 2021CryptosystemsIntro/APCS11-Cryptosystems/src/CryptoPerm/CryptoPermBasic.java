/********* CryptoPermBasic.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 *
 *  A permutation cryptosystem uses a permutation, monoalphabetic, 
 *  symmetric, substitution cipher that has a private key and that is 
 *  vulnerable to a plaintext attack, i.e. in this case, it is enough 
 *  to find a finite sequence of ciphertext and corresponding plaintext
 *  pairs (altogether containing the entire alphabet), in order to 
 *  determine the private key.
 *
 *  This example uses a mystery language with only a ten character 
 *  alphabet, contained in the file mystery10.txt. In addition, as can be
 *  seen by the included Lexicon Data report, this language is an unknown 
 *  intermediate language whose plaintext words cannot be immediately 
 *  translated to English by a non-native speaker. This type of 
 *  cryptanalytic defense is similar to the Navajo language encryption 
 *  used during World War II. The traditional Navajo language is an 
 *  oral language and so only known to Native American Navajos. These
 *  'Navajo code talkers' were able to very quickly encrypt an English
 *  message into Navajo code (800 plaintext Navajo code words) and orally 
 *  transmit the message to another code talker who then quickly decrypted
 *  the message into English. This process was much faster and less error
 *  prone than the Enigma code machines used by the Germans during World 
 *  War II.
 *  
 *  Furthermore, again as can be seen by the included Lexicon Data report,
 *  this mystery language has a structure that is not so vulnerable to a
 *  frequency attack. The 3 vowel frequencies are all about 17-18 % and 
 *  the 7 consonant frequencies are are all about 6-7 %. Also, the bigram 
 *  frequencies are all about 2-3 %, and the word lengths range from 3 to 6.
 *  Note that this file gives examples of the use of permutation ciphers
 *  with this 10 character alphabet. There are 10! = 3,628,800 permutation
 *  keys for a 10 character alphabet, in contrast to merely 36 affine keys
 *  which are themselves essentially 36 permutation keys. In this CryptoPerm 
 *  package we develop a general permutation cryptosystem, which represents 
 *  all possible substitution ciphers for this alphabet. In general, the 
 *  larger the key set the more secure the cryptosystem. So, this setting
 *  will require successively more challenging cryptanalysis attacks as we
 *  progress from phrases that are a list of encrypted words to a single 
 *  encrypted string of words.
 *  
 */

package CryptoPerm;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import LexiconData.LexiconData;
import LexiconData.Ntuple;
import LexiconData.NtupleComparator;
import LexiconData.ReadWriteFile;

public class CryptoPermBasic 
{
	protected List<Character> alphabet;
	protected int alphaSize;
	protected String dirPath;
	// ArrayList of Ntuple word pairs (mystery, english)
	private List<Ntuple> mysteryLexicon = new ArrayList<Ntuple>();
	// Translation Maps
	private Map<String,String> mysteryToEnglish = new HashMap<String,String>();
	private Map<String,String> englishToMystery = new HashMap<String,String>();
	// Permutations
	protected int[] idPerm;
	private Set<int[]> permSet0 = new HashSet<int[]>();
	private List<String> output1 = new ArrayList<String>();
	private Set<List<String>> decryptSet1 = new HashSet<List<String>>();
	private int fileCountEncrypt = 0;
	private int fileCountDecrypt0 = 0, fileCountDecrypt1 = 0;
	private String previousDecryptFile1;     // Delete cumulative multiple file production
                                             // in recursive decrypt methods.
	
	public CryptoPermBasic(List<Character> alphabet, String dirPath, String lexicon, boolean usePermSet0)
	{
		// Mystery Lexicon
		this.alphabet = alphabet;
		this.alphaSize = alphabet.size();
		this.dirPath = dirPath;
		readFileOfMysteryLexicon(dirPath + lexicon);
		createTranslationMaps();
		// Permutations
		createIdPerm();
		if(usePermSet0)
		{
		  createPermSet0(idPerm, alphaSize);  // Out of Memory error for n >= 11.
		                                      // For n >= 11 comment out this line
		                                      // and use decryptWriteData1 in Problem 45,
		                                      // instead of the decrypt method in Problem
		                                      // 43, which uses permSet0.
		}
	}
	
	// Problem 1. Create getter for alphabet.
	public List<Character> getAlphabet()
	{
		// insert your code here
	}
	
	// Problem 2. Create getter for alphabet size.
	public int getAlphaSize()
	{
		// insert your code here
	}
	
	// Problem 3. Read lexicon into ArrayList of ntuples of word pairs.
	public void readFileOfMysteryLexicon(String fileName)
	{
		// insert your code here
	}		
	
	// Problem 4. Create getter for mysteryLexicon.
	public List<Ntuple> getMysteryLexicon()
	{
		// insert your code here
	}
	
	// Problem 5. Create translation maps.
	public void createTranslationMaps()
	{
		// insert your code here
	}
	
	// Problem 6. Create getter for mysteryToEnglish.
	public Map<String,String> getMysteryToEnglish()
	{
		// insert your code here
	}
	
	// Problem 7. Create getter for englishToMystery.
	public Map<String,String> getEnglishToMystery()
	{
		// insert your code here
	}	
	
	// Problem 8. Create function to translate a mystery string to english.
	public String toEnglish(String mysteryKey)
	{
		// insert your code here
	}
	
	// Problem 9. Create function to translate a mystery list to english.
	public List<String> toEnglish(List<String> mystery)
	{
		// insert your code here
	}
	
	// Problem 10. Create function to translate an english string to mystery.
	public String toMystery(String englishKey)
	{
		// insert your code here
	}	
	
	// Problem 11. Create function to translate an english list to mystery.
	public List<String> toMystery(List<String> english)
	{
		// insert your code here
	}	

	// Problem 12. Create getter for identity permutation.
	public int[] getIdPerm()
	{
		// insert your code here
	}
	
	// Problem 13. A permutation of n integers will be represented as an 
	// int array of length n whose entries are just the set of indexes of
	// the array in some scrambled or permuted order. Another way to
	// think of this permutation is as a 1-1 function from the set of 
	// indexes to itself, i.e. a: {0,1,2,...,n-1} -> {0,1,2,...,n-1}.
	public boolean isPerm(int[] a)
	{
		// insert your code here
	}
	
	// Problem 14. Create identity permutation which is the 
	// length of the current alphabet. Note that we will be
	// creating substitution ciphers using permutations
	// of the alphabet.
	public void createIdPerm()
	{
		// insert your code here
	}
	
	// Problem 15. Recursive method to create permSet0 which
	// which is the set of all permutations of length n. 
	// Further, the input array, a, will be the identity 
	// permutation of length n. Also, note that we intend 
	// to use this only for alphabets of size <= 11, namely 
	// those that are reasonably computable, i.e. in less 
	// than 10 minutes. For larger alphabets, use the method
	// in Problem 29 below. 
    public void createPermSet0(int[] a, int n) 
    {
    	// insert your code here
    }  

    // Problem 16. Swap the entries of array, a, at indices i and j.
    public void swap(int[] a, int i, int j) 
    {
    	// insert your code here
    }	
	
    // Problem 17. Create getter for permSet0.
	public Set<int[]> getPermSet0()
	{
		// insert your code here
	}
    
	// Problem 18. Create factorial (recursive) function.
	public int fact(int n)
	{
		// insert your code here
	}
	
	// Problem 19. Generate a random permutation of length n.
	public int[] randomPerm(int n)
	{
		// create identity permutation
		
		// insert your code here
        
        // randomly permute identity
		
		// insert your code here
    }
    
	// Problem 20. Test whether or not an array (function) 
	// has a fixed point.
	public boolean hasFixedPoint(int[] a)
	{
		// insert your code here
	}
	
	// Problem 21. Generate a random permutation with no fixed points.
	public int[] randomPermNoFixedPoints(int n)
	{
		// insert your code here
	}
	
	// Problem 22. Create the inverse (function) of a permutation.
	public int[] inversePerm(int[] a)
	{
		// insert your code here
	}

	// Problem 23. Apply a permutation to a string.
	public String permOfStr(int[] perm, String str)
	{
		// insert your code here
	}
	
	// Problem 24. Apply a permutation to a string list.
	public List<String> permOfStrList(int[] perm, List<String> strList)
	{
		// insert your code here
	}
	
	// Problem 25. Encrypt a plaintext message represented as a list
	// of strings. Do this by generating a random permutation without
	// fixed points and applying it to each string in the list.
	public List<String> encrypt(List<String> plaintext)
	{
		int[] perm = randomPermNoFixedPoints(alphaSize);
		List<String> ciphertext = permOfStrList(perm,plaintext);
		writeEncryptData(plaintext,perm,ciphertext);
		return ciphertext;
	}
		
	// Problem 26. Create a method to save encryption data to disk.
	public void writeEncryptData(List<String> plaintext, int[] perm, List<String> ciphertext)
	{
		ArrayList<String> output = new ArrayList<String>();
		output.add("english = " + toEnglish(plaintext).toString());
		output.add("plaintext = " + plaintext.toString());
		output.add("perm = " + Arrays.toString(perm));
		output.add("ciphertext = " + ciphertext.toString());
		ReadWriteFile rw = new ReadWriteFile();
		String date = new SimpleDateFormat("yyyy-MM-dd.HH.mm.ss").format(new Date());
		String encrypt = dirPath + "PermEncrypt" + date + ".Num" + fileCountEncrypt++ + ".txt";
		rw.writeOutput(output, encrypt);
	}
	
	// Problem 27. Test whether or not a permutation of a string is 
	// contained in the keySet of the lexicon.
	public boolean inLexiconPermOfStr(int[] perm, String str)
	{
		// insert your code here
	}
	
	// Problem 28. Decrypt a ciphertext list, including code that writes
	// decryption data both to the console window and to disk. This method
	// uses (depends on) the creation of permSet0 which is the memory intensive
	// set of all permutations of length n = alphaSize, and so is impractical
	// for n >= 11 since in this case out of memory error occurs.
	public void decrypt0(List<String> ciphertext)
	{
		List<String> output = new ArrayList<String>();
		int n = 0;
		for(int[] perm : permSet0)
		{
			boolean inLexicon = true;
			for(String str : ciphertext)
			{
			   inLexicon = inLexicon && inLexiconPermOfStr(perm, str);   
			}
			
			if(inLexicon)
			{
				n++;
				int[] inv = inversePerm(perm);
				List<String> plaintext = permOfStrList(perm, ciphertext);
				System.out.println("\ncount = " + n + " perm = " + Arrays.toString(perm) +  " inv = " + Arrays.toString(inv));
				System.out.println("plaintext = " + plaintext);
				System.out.println("english = " + toEnglish(plaintext));
				output.add("ciphertext = " + ciphertext.toString());
				output.add("count = " + n + " perm = " + Arrays.toString(perm) +  " inv = " + Arrays.toString(inv));
				output.add("plaintext = " + plaintext);
				output.add("english = " + toEnglish(plaintext));	
			}
		}
		ReadWriteFile rw = new ReadWriteFile();
		String date = new SimpleDateFormat("yyyy-MM-dd.HH.mm.ss").format(new Date());
		String decrypt =  dirPath + "PermDecrypt0" + date + ".Num" + fileCountDecrypt0++ + ".txt";
		rw.writeOutput(output, decrypt);
	}
	
	// Problem 29. Decrypt a ciphertext list, without creating or using
	// permSet0 which increases efficiency and decreases memory use. In order 
	// to do this, the methods in Problems 15 (recursive method) and 28 are 
	// combined in this method. Note that, since this is a recursive method
	// that generates and applies the permutations in order to decrypt the
	// ciphertext list, the private instance variable output is used to 
	// capture the decryption data. This means that the decryption data for
	// each recursive call is added and returned in a file when the recursive
	// call terminates. So, it is only necessary to look a the last file to
	// search for possible plaintext and English candidates.
    public void decrypt1(List<String> ciphertext, int offset, int[] a, int n, boolean all) 
    {
    	// base case of recursive call
        if (n == 1) 
        {   
			int m = ciphertext.size();
			for(int i = 0; i < ciphertext.size(); i++)
			{
				if(!inLexiconPermOfStr(a.clone(), ciphertext.get(i)))
				{
					m = i;
					break;
				}
			}
			if(m == ciphertext.size() - offset)
			{
				int[] clone = a.clone();
				int[] inv = inversePerm(clone);
				List<String> plaintext = permOfStrList(clone, ciphertext.subList(0, m));
				List<String> plaintext2 = permOfStrList(clone, ciphertext);
				List<String> english = toEnglish(plaintext2);
				if(all)
				{
				    output1.add("english = " + english + " plaintext = " + plaintext2 + " perm = " + Arrays.toString(clone) + " ciphertext = " + ciphertext +  " inv = " + Arrays.toString(inv));
				}
				else
				{
				    if(!decryptSet1.contains(plaintext))
				    {
					   decryptSet1.add(plaintext);
				       output1.add("english = " + english + " plaintext = " + plaintext2 + " perm = " + Arrays.toString(clone) + " ciphertext = " + ciphertext +  " inv = " + Arrays.toString(inv));
				    }
				}
			}
            return;
        }
        else
        {
            for (int i = 0; i < n; i++) 
            {
                swap(a, i, n-1);
                decrypt1(ciphertext, offset, a, n-1, all);  // recursive call
                swap(a, i, n-1);
            }
        }
    }
    
    // Problem 30. This method decrypts a ciphertext list using the method in 
    // Problem 29 and also writes decryption data to disk. This method calls the
    // decrypt1 method, and, due to the many recursive calls that decrypt1 will make 
    // to itself in order to generate permutations, several files will be written 
    // to disk. In addition, the decryptWriteData method itself, will be called
    // by the decryptPartitionStrWriteData method (below), which introduces another
    // layer of recursive calls, by calling the recursive method decryptOrderedPartitionStr,
    // which generates ordered partitions that are used to tests partitions of a given 
    // ciphertext string. 
    public void decryptWriteData1(List<String> ciphertext, int offset, int[] a, int n, boolean all)
	{
    	decrypt1(ciphertext, offset, a, n, all);
		if(output1.size() != 0)
		{ 
		   if(fileCountDecrypt1 > 0)
		   {
			   File file = new File(previousDecryptFile1);
			   if (file.exists()) 
			   {
			       file.delete();
			   } 
			   else 
			   {
			       System.err.println(
			           "I cannot find '" + file + "' ('" + file.getAbsolutePath() + "')");
			   }
		   }
		   ReadWriteFile rw = new ReadWriteFile();
		   String date = new SimpleDateFormat("yyyy-MM-dd.HH.mm.ss").format(new Date());
		   String decryptFile = dirPath + "PermDecrypt1" + date + ".Num" + fileCountDecrypt1++ + ".txt";
		   previousDecryptFile1 = decryptFile;
		   Collections.sort(output1, null);
		   rw.writeOutput(output1, decryptFile);
		}	  
	}
    
}    
/********* CPBMain.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 *
 *  A permutation cryptosystem uses a permutation, monoalphabetic, 
 *  symmetric, substitution cipher that has a private key and that is 
 *  vulnerable to a plaintext attack, i.e. in this case, it is enough 
 *  to find a finite sequence of ciphertext and corresponding plaintext
 *  pairs (altogether containing the entire alphabet), in order to 
 *  determine the private key.
 *
 *  This example uses a mystery language with only a ten character 
 *  alphabet, contained in the file mystery10.txt. In addition, as can be
 *  seen by the included Lexicon Data report, this language is an unknown 
 *  intermediate language whose plaintext words cannot be immediately 
 *  translated to English by a non-native speaker. This type of 
 *  cryptanalytic defense is similar to the Navajo language encryption 
 *  used during World War II. The traditional Navajo language is an 
 *  oral language and so only known to Native American Navajos. These
 *  'Navajo code talkers' were able to very quickly encrypt an English
 *  message into Navajo code (800 plaintext Navajo code words) and orally 
 *  transmit the message to another code talker who then quickly decrypted
 *  the message into English. This process was much faster and less error
 *  prone than the Enigma code machines used by the Germans during World 
 *  War II.
 *  
 *  Furthermore, again as can be seen by the included Lexicon Data report,
 *  this mystery language has a structure that is not so vulnerable to a
 *  frequency attack. The 3 vowel frequencies are all about 17-18 % and 
 *  the 7 consonant frequencies are are all about 6-7 %. Also, the bigram 
 *  frequencies are all about 2-3 %, and the word lengths range from 3 to 6.
 *  Note that this file gives examples of the use of permutation ciphers
 *  with this 10 character alphabet. There are 10! = 3,628,800 permutation
 *  keys for a 10 character alphabet, in contrast to merely 36 affine keys
 *  which are themselves essentially 36 permutation keys. In this CryptoPerm 
 *  package we develop a general permutation cryptosystem, which represents 
 *  all possible substitution ciphers for this alphabet. In general, the 
 *  larger the key set the more secure the cryptosystem. So, this setting
 *  will require successively more challenging cryptanalysis attacks as we
 *  progress from phrases that are a list of encrypted words to a single 
 *  encrypted string of words.
 *  
 */

package CryptoPerm;
import java.util.*;
import LexiconData.LexiconData;
import LexiconData.Ntuple;
import LexiconData.NtupleComparator;
import LexiconData.ReadWriteFile;

public class CPBMain 
{
	public static void main(String[] args) 
	{
		 // Start Clock
	    long start = System.currentTimeMillis();
	    
	    // Create alphabet character array
		Character[] arr = {'a','i','u','k','l','n','p','s','t','v'};
		List<Character> alphabet = new ArrayList<Character>(Arrays.asList(arr));
	    String dirPath = "src//CryptoPerm//";
	    String lexicon = "mystery10.txt";
	    boolean init = false;
	    // Create LexiconData object
	  	LexiconData lex = new LexiconData(alphabet,dirPath,lexicon,init);
	  	String wordInLexicon = "ukuna";
	  	if(!init)
	  	{
	  	   lex.lexiconReport(wordInLexicon);
	  	}	    

		// Create CryptoPermBasic object
	  	boolean usePermSet0 = true;
		CryptoPermBasic cp = new CryptoPermBasic(alphabet,dirPath,lexicon,usePermSet0);
		
		// Test Permutation Methods
		//testPermMethods(cp);
		
		// Test Cryptology Methods 
		// List versions of plaintext and ciphertext
		// Both created and saved permSet0 and recursively generated permutations
		//testCryptoMethods(cp);

		// End Clock
	    long end = System.currentTimeMillis();
	    System.out.println("\ntime = " + (end-start)/1000. + " seconds");
	}
	
	public static void testPermMethods(CryptoPermBasic cp)
	{
		/*
		System.out.println("\nTest Permutation Methods");
		int[] perm = {3, 6, 9, 7, 1, 5, 4, 8, 2, 0};
		System.out.println("perm = " + Arrays.toString(perm));
		System.out.println("cp.isPerm(perm) = " + cp.isPerm(perm));
		System.out.println("cp.getPermSet0().size() = " + cp.getPermSet0().size());
		System.out.println("cp.fact(10) = " + cp.fact(10));
		System.out.println("cp.fact(11) = " + cp.fact(11));
		System.out.println("cp.fact(12) = " + cp.fact(12));
		System.out.println("cp.fact(26) = 403291461126605635584000000" +", checking 10^6/sec, time to check all > age of universe");
		int[] rndPerm1 = cp.randomPerm(5);
		while(!cp.hasFixedPoint(rndPerm1))
		{
			rndPerm1 = cp.randomPerm(5);
		}
		System.out.println("rndPerm1 = " + Arrays.toString(rndPerm1));
		System.out.println("cp.hasFixedPoint(rndPerm1) = " + cp.hasFixedPoint(rndPerm1));
		System.out.println("cp.inversePerm(rndPerm1) = " + Arrays.toString(cp.inversePerm(rndPerm1)));
		int[] rndPerm2 = cp.randomPermNoFixedPoints(5);
		System.out.println("rndPerm2 = " + Arrays.toString(rndPerm2));
		System.out.println("cp.hasFixedPoint(rndPerm2) = " + cp.hasFixedPoint(rndPerm2));
		System.out.println("cp.inversePerm(rndPerm2) = " + Arrays.toString(cp.inversePerm(rndPerm2)));
		*/
	}
	
	public static void testCryptoMethods(CryptoPermBasic cp)
	{
		/*
		System.out.println("\nTest Cryptology Methods");
		// Example01.
		List<String> english01 = new ArrayList<String>();
		english01.add("bad");
		english01.add("family");
		english01.add("article");
		english01.add("cousin");
		//english01.add("angry");
		
		System.out.println("Example01. Apply random permutation to plaintext to create cyphertext:");
		System.out.println("english = " + english01);
		List<String> plaintext01 = cp.toMystery(english01);
		System.out.println("plaintext = " + plaintext01);
		List<String> ciphertext01 = cp.encrypt(plaintext01);
		System.out.println("ciphertext = " + ciphertext01);
		System.out.println("\nDecrypt ciphertext trying permutations that produce words in lexicon:");
		cp.decrypt0(ciphertext01);
		
		// Example02.
		List<String> english02 = new ArrayList<String>();
		english02.add("bad");
		english02.add("family");
		english02.add("article");
		System.out.println("\nExample02. Apply random permutation to plaintext to create cyphertext:");
		System.out.println("english = " + english02);
		List<String> plaintext02 = cp.toMystery(english02);
		System.out.println("plaintext = " + plaintext02);
		List<String> ciphertext02 = cp.encrypt(plaintext02);
		System.out.println("ciphertext = " + ciphertext02);
		System.out.println("\nDecrypt ciphertext trying permutations that produce words in lexicon:");
		cp.decrypt0(ciphertext02);
		
		// Example03. (Uses ciphertext02 from Example02.)
		System.out.println("\nTest the more efficient decryptWriteData version of decryption, which doesn't create permSet0.");
		System.out.println("This method only writes the data files to disk and not to the console window.");
		cp.decryptWriteData1(ciphertext02, 0, cp.getIdPerm(), cp.getAlphaSize(), false);	    
      */
	}
	
}
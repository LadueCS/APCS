/********* CryptoAffine.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 *
 *  An affine cryptosystem uses an affine, monoalphabetic, symmetric, 
 *  substitution cipher that has a private key and that is vulnerable
 *  to a plaintext attack, i.e. in this case, it is enough to find two 
 *  ciphertext characters corresponding to two plaintext characters, 
 *  in order to determine the private key.
 *  Note also that a language like English is always vulnerable to a 
 *  frequency attack, since certain individual characters and certain 
 *  bigrams occur much more frequently than others.
 *  
 */

package CryptoAffine;
import java.text.SimpleDateFormat;
import java.util.*;
import LexiconData.LexiconData;
import LexiconData.Ntuple;
import LexiconData.NtupleComparator;
import LexiconData.ReadWriteFile;

public class CryptoAffine
{
	protected List<Character> alphabet;
	protected int alphaSize;
	private String dirPath;
	// Create ntuples of individual plaintext words or (plaintext word, 
	// known language word) pairs. Note that this will be determined 
	// programmatically by a ReadWriteFile method (see below).
	private List<Ntuple> lexicon = new ArrayList<Ntuple>();
	private List<String> plaintextWords = new ArrayList<String>();
	private List<Integer> units = new ArrayList<Integer>();
	private Set<int[]> affineKeySet = new HashSet<int[]>();
	private int fileCountEncrypt = 0;
	private int fileCountDecrypt = 0; 
	
	public CryptoAffine(List<Character> alphabet, String dirPath, String lexicon)
	{
		this.alphabet = alphabet;
		this.alphaSize = alphabet.size();
		this.dirPath = dirPath;
		readFileOfLexicon(dirPath + lexicon);
		createPlaintextWords();
		units = groupOfUnits(alphaSize);
		affineKeySet = createAffineKeySet(alphaSize);
	}
	
	// Problem 1. Create getter for alphabet.
	public List<Character> getAlphabet()
	{
		return alphabet;
	}

	// Problem 2. Create getter for alphabet size.
	public int getAlphaSize()
	{
		return alphaSize;
	}

	// Problem 3. Read lexicon into List of ntuples of individual words 
	// or possibly (mystery word, english word) pairs. Note that this will be 
	// determined programmatically by the ReadWriteFile method createNtupleLines, 
	// which reads each line (record) of strings into an ntuple and creates an 
	// ArrayList of these ntuples.
	public void readFileOfLexicon(String inputFile)
	{
		var rwf = new ReadWriteFile();
		rwf.createNtupleLines(inputFile, 0);
		lexicon = rwf.getNtupleLines();
	}		

	// Problem 4. Using the private instance variable lexicon, initialize the
	// private instance variable plaintextWords.
	public void createPlaintextWords()
	{
		for (var v : lexicon) plaintextWords.add((String)v.getkth(0));
	}	
	
	// Problem 5. Create getter for plaintextWords.
	public List<String> getPlaintextWords()
	{
		return plaintextWords;
	}		
	
	// Problem 6. Compute the greatest common divisor of two integers.
	public int gcd(int a, int b)
	{
		return b == 0 ? a : gcd(b, a%b);
	}	
	
	// Problem 7. A unit is an integer that has a multiplicative
	// inverse mod n. Create the list of all units for a given n,
	// which is a closed multiplicative system called a group.
	// Note that an integer i is a unit mod n, if it is relatively
	// prime to n, i.e. gcd(i,n) = 1.
	public List<Integer> groupOfUnits(int n)
	{
		List<Integer> ret = new ArrayList<>();
		for (int i = 1; i < n; ++i) if (gcd(i, n) == 1) ret.add(i);
		return ret;
	}
	 
	// Problem 8. Create getter for the units with the current 
	// alphaSize (see constructor) for this object.  
    public List<Integer> getUnits()
    {
    	return units;
    }
	
    // Problem 9. Create test for a unit with the current 
 	// alphaSize (see constructor) for this object. 
    public boolean isUnit(int k)
    {
    	return units.indexOf(k) != -1;
    }
    
    // Problem 10. As we have mentioned above, since the units
    // are a closed multiplicative system, each one has a
    // multiplicative inverse. So, given a unit k this method
    // returns its multiplicative inverse mod alphaSize.
    public int inverse(int k)
    {
    	for (int x = 1; x < alphaSize; ++x) if (x*k%alphaSize == 1) return x;
    	return 0;
    }

    // Problem 11. An affine key is a pair [k0, k1] where k0
    // is the shift key and k1 is the multiplication key in
    // an affine transformation of an index of the alphabet,
    // namely: k1*index + k0, where 1 <= k0 <= alphaSize-1
    // and k1 is a unit. So, in order to not have the identity
    // encryption that does nothing, we will not allow the 
    // additive part of the key, k0, to be zero but we will
    // allow the multiplicative part of the key, k1, to be 1.
    // For example, when alphaSize = 26 we will have 300 keys
    // that actually encrypt.
    // Note that the intended main usage of this method is
    // when n = alphaSize but it should also work properly
    // when n = 1 which should provide a version that is only
    // a random shift with k1 = 1, i.e. [k0, k1] = [k0, 1].
    public int[] randomKey(int n)
    {
    	int[] ret = new int[2];
    	ret[1] = units.get((int)(units.size()*Math.random()));
    	ret[0] = (int)((n-1)*Math.random())+1;
    	return ret;
    }

    // Problem 12. Create the inverse of a given affine key.
    public int[] inverseKey(int[] key)
    {
    	int[] ret = new int[2];
    	ret[0] = (alphaSize*alphaSize-inverse(key[1])*key[0])%alphaSize;
    	ret[1] = inverse(key[1]);
    	return ret;
    }
    
    // Problem 13. Create the set of all (300 for alphaSize = 26) affine keys.
    public Set<int[]> createAffineKeySet(int n)
    {
    	Set<int[]> ret = new HashSet<int[]>();
    	for (int i = 1; i <= alphaSize; ++i) {
            for (int j : units) {
                int[] tmp = { i, j };
                ret.add(tmp);
            }
    	}
    	return ret;
    }
    
    // Problem 14. Create getter for affine key set.
    public Set<int[]> getAffineKeySet()
    {
    	return affineKeySet;
    }
    
	// Problem 15. Apply an affine key to encrypt a string by applying
    // it to the alphabet indexes to scramble the characters of the string.
	public String affineEncrypt(int[] key, String str)
	{
		str = str.toUpperCase();
		String ret = new String();
		for (int i = 0; i < str.length(); ++i) {
            ret += (char)((key[0]+key[1]*(str.charAt(i)-'A'))%alphaSize+'A');
		}
		return ret;
	}
	
	// Problem 16. Apply an affine key to decrypt a string by applying 
	// it to the alphabet indexes to unscramble the characters of the 
	// string. Note that the encryption formula in Problem 15 and the
	// decryption formula here below are necessarily different in order
	// that the inverse affine key undoes the transformation of the
	// original affine encryption key.
	public String affineDecrypt(int[] inv, String str)
	{
		return affineEncrypt(inv, str);
	}
	
	// Problem 17. Apply an affine key to encrypt a plaintext list.
	public List<String> affineEncryptList(int[] key, List<String> plaintext)
	{
		List<String> ret = new ArrayList<>();
		for (var s : plaintext) ret.add(affineEncrypt(key, s));
		return ret;
	}
	
	// Problem 18. Apply an affine key to decrypt a ciphertext list.
	public List<String> affineDecryptList(int[] inv, List<String> ciphertext)
	{
		List<String> ret = new ArrayList<>();
		for (var s : ciphertext) ret.add(affineDecrypt(inv, s));
		return ret;
	}
	
	// Problem 19. Encrypt a plaintext message represented as a list
	// of strings. Do this by generating a random key and applying 
	// it to each string in the list. In addition, save this data
	// to disk using the method in Problem 20 below.
	public List<String> encrypt(List<String> plaintext)
	{
		int[] key = randomKey(alphaSize);
		return affineEncryptList(key, plaintext);
	}
		
	// Problem 20. Create a method to save encryption data to disk.
	public void writeEncryptData(List<String> plaintext, int[] key, List<String> ciphertext)
	{
		List<String> output = new ArrayList<String>();
		output.add("plaintext = " + plaintext.toString());
		output.add("key = " + Arrays.toString(key));
		output.add("ciphertext = " + ciphertext.toString());
		ReadWriteFile rw = new ReadWriteFile();
		String date = new SimpleDateFormat("yyyy-MM-dd.HH.mm.ss").format(new Date());
		String encrypt =  dirPath + "AffineEncrypt" + date + ".Num" + fileCountEncrypt++ + ".txt";
		rw.writeOutput(output, encrypt);
	}    
	
	// Problem 21. Test whether or not an affine decryption of a string 
	// is contained in the list of plaintextWords.
	public boolean inWordsAffineDecrypt(int[] inv, String str)
	{
		String s = affineDecrypt(inv, str).toLowerCase();
		return plaintextWords.indexOf(s) != -1;
	}	
	
	// Problem 22. Decrypt a ciphertext list, including code that writes
	// decryption data both to the console window and to disk. This method
	// uses (depends on) the creation of affineKeySet.
	public void decrypt(List<String> ciphertext)
	{
		List<String> output = new ArrayList<String>();
		int n = 0;
		for(int[] decryptKey : affineKeySet)
		{
			boolean inWords = true;
			for(String str : ciphertext)
			{
			   inWords = inWords && inWordsAffineDecrypt(decryptKey, str); 
			}
			
			if(inWords)
			{
				n++;
				int[] encryptKey = inverseKey(decryptKey);
				List<String> plaintext = affineDecryptList(decryptKey, ciphertext);
				System.out.println("count = " + n + " decryptKey = " + Arrays.toString(decryptKey) +  " encryptKey = " + Arrays.toString(encryptKey));
				System.out.println("plaintext = " + plaintext);
				output.add("ciphertext = " + ciphertext.toString());
				output.add("count = " + n + " decryptKey = " + Arrays.toString(decryptKey) +  " encryptKey = " + Arrays.toString(encryptKey));
				output.add("plaintext = " + plaintext);
			}
		}
		ReadWriteFile rw = new ReadWriteFile();
		String date = new SimpleDateFormat("yyyy-MM-dd.HH.mm.ss").format(new Date());
		String decrypt = dirPath + "AffineDecrypt" + date + ".Num" + fileCountDecrypt++ + ".txt";
		rw.writeOutput(output, decrypt);
	}
		
	// Problem 23. If the lexicon is comprised of (plaintext word, 
	// known language word) pairs as in mystery10, create a method
	// that translates the plaintext word into the known language
	// word, in the case of mystery10, an English word.
    public String toEnglish(String str)
    {
    	return str; // ?????
    }
    
	// Problem 24. If the lexicon is comprised of (plaintext word, 
	// known language word) pairs as in mystery10, create a method
	// that translates a list of plaintext words into a list of
    // known language words, in the case of mystery10, a list of
    // English words.
    public List<String> toEnglish(List<String> list)
    {
    	List<String> ret = new ArrayList<>();
    	for (var s : list) ret.add(toEnglish(s));
    	return ret;
    }
}

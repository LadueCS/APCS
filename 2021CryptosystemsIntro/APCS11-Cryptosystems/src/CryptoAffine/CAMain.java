/********* CAMain.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 *
 *  An affine cryptosystem uses an affine, monoalphabetic, symmetric, 
 *  substitution cipher that has a private key and that is vulnerable
 *  to a plaintext attack, i.e. in this case, it is enough to find two 
 *  ciphertext characters corresponding to two plaintext characters, 
 *  in order to determine the private key.
 *  Note also that a language like English is always vulnerable to a 
 *  frequency attack, since certain individual characters and certain 
 *  bigrams occur much more frequently than others.
 *
 */

package CryptoAffine;
import java.util.*;
import LexiconData.LexiconData;
import LexiconData.Ntuple;
import LexiconData.NtupleComparator;
import LexiconData.ReadWriteFile;

public class CAMain 
{
	public static void main(String[] args) 
	{
		 // Start Clock
	    long start = System.currentTimeMillis();
	    
	    // Create alphabet character array
	    List<Character> alphabet = new ArrayList<Character>();
	    for(int i = 97; i <= 122; i++)
	    {
	    	alphabet.add((char)i);
	    }
	    String dirPath = "CryptoAffine//";
	    String lexicon = "english26-3.txt";
	    boolean init = false;
	    
	    // Create LexiconData object
		LexiconData lex = new LexiconData(alphabet, dirPath, lexicon, init);
		String wordInLexicon = "post";
		if(!init)
		{
		   lex.lexiconReport(wordInLexicon);
		}

	    // Create CryptoAffine object
		CryptoAffine ca = new CryptoAffine(alphabet, dirPath, lexicon);

		// Test Cryptology Methods 
		testCryptoMethods(ca);

		// End Clock
	    long end = System.currentTimeMillis();
	    System.out.println("\ntime = " + (end-start)/1000. + " seconds");
	}
	
	public static void testCryptoMethods(CryptoAffine ca)
	{
		System.out.println("\nTest Cryptology Methods");
	    
		System.out.println("ca.getUnits() = " + ca.getUnits());
        int[] randomKey = ca.randomKey(ca.getAlphaSize());
		System.out.println("randomKey = " + Arrays.toString(randomKey));		
		System.out.println("ca.inverseKey(randomKey) = " + Arrays.toString(ca.inverseKey(randomKey)));
		
		// Test Code for randomKey(n) when n = 1.
		int[] randomKey2 = ca.randomKey(1);
		System.out.println("randomKey2 = " + Arrays.toString(randomKey2));		
		System.out.println("ca.inverseKey(randomKey2) = " + Arrays.toString(ca.inverseKey(randomKey2)));
		
		System.out.println("ca.getAffineKeySet().size() = " + ca.getAffineKeySet().size());
		for(int[] key : ca.getAffineKeySet())
		{
			System.out.println("key = " + Arrays.toString(key));
		}
		
		// Test Code for affineEncrypt and affineDecrypt
		String plaintext0 = "duetime";
		int[] key = {5,3};
		System.out.println("\nkey = " + Arrays.toString(key));
		System.out.println("plaintext0 = " + plaintext0);
		String ciphertext0 = ca.affineEncrypt(key, plaintext0);
		int[] invkey = ca.inverseKey(key);
		System.out.println("invkey = " + Arrays.toString(invkey));
		System.out.println("ca.affineEncrypt(key, plaintext0) = " + ciphertext0);
		System.out.println("ca.affineDecrypt(invkey, ciphertext0) = " + ca.affineDecrypt(invkey, ciphertext0));
		
		// Test Code for affineEncryptList and affineDecryptList
		List<String> list0 = new ArrayList<String>();
		list0.add("duetime");
		list0.add("is");
		list0.add("whenever");
		System.out.println("\nkey = " + Arrays.toString(key));
		System.out.println("list0 = " + list0);
		List<String> ciphertextList0 = ca.affineEncryptList(key, list0);
		System.out.println("invkey = " + Arrays.toString(invkey));
		System.out.println("ca.affineEncryptList(key, list0) = " + ciphertextList0);
		System.out.println("ca.affineDecryptList(invkey, ciphertextList0) = " + ca.affineDecryptList(invkey, ciphertextList0));
		
		// Example01.
		List<String> plaintext01 = new ArrayList<String>();
		plaintext01.add("go");
		//plaintext01.add("cat");
		System.out.println("\nExample01. Apply random affine key to plaintext to create cyphertext:");
		System.out.println("plaintext = " + plaintext01);
		List<String> ciphertext01 = ca.encrypt(plaintext01);
		System.out.println("ciphertext = " + ciphertext01);
		System.out.println("Decrypt ciphertext trying affine keys that produce words in lexicon:");
		ca.decrypt(ciphertext01);
		
		// Example02.
		List<String> plaintext02 = new ArrayList<String>();
		plaintext02.add("it");
		//plaintext02.add("is");
		//plaintext02.add("no");
		//plaintext02.add("deal");
		System.out.println("\nExample02. Apply random affine key to plaintext to create cyphertext:");
		System.out.println("plaintext = " + plaintext02);
		List<String> ciphertext02 = ca.encrypt(plaintext02);
		System.out.println("ciphertext = " + ciphertext02);
		System.out.println("Decrypt ciphertext trying affine keys that produce words in lexicon:");
		ca.decrypt(ciphertext02);
		
		// Example03.
		List<String> plaintext03 = new ArrayList<String>();
		plaintext03.add("attitude");
		plaintext03.add("is");
		plaintext03.add("little");
		plaintext03.add("thing");
		plaintext03.add("that");
		plaintext03.add("makes");	
		plaintext03.add("big");
		plaintext03.add("difference");
		System.out.println("\nExample03. Apply random affine key to plaintext to create cyphertext:");
		System.out.println("plaintext = " + plaintext03);
		List<String> ciphertext03 = ca.encrypt(plaintext03);
		System.out.println("ciphertext = " + ciphertext03);
		System.out.println("Decrypt ciphertext trying affine keys that produce words in lexicon:");
		ca.decrypt(ciphertext03);
		
	}
}




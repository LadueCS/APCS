/********* ReadWriteFile.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 *
 */

package LexiconData;
import java.io.*;
import java.util.*;

public class ReadWriteFile 
{
	private List<String> fileLines;
	private List<List<String>> parsedFileLines;
	private List<Ntuple> ntupleLines = new ArrayList<Ntuple>();

	public ReadWriteFile()
	{
		
	}
	
	public ReadWriteFile(String fileName)
	{ 
		// create fileLines array
		fileLines = new ArrayList<String>();
		createFileLines(fileName);
		parsedFileLines = parseFileLines(fileLines);
	}
	
	public ReadWriteFile(String fileName, int[] typeList)
	{ 
		// create ntupleLines array
		ntupleLines = new ArrayList<Ntuple>();
		createNtupleLines(fileName, typeList);
	}
	
	public ReadWriteFile(String fileName, int type)
	{ 
		// create ntupleLines array
		ntupleLines = new ArrayList<Ntuple>();
		createNtupleLines(fileName, type);
	}
	
	public List<Ntuple> getNtupleLines()
	{
		return ntupleLines;
	}
	
	// Read each homogeneous line (record) of the input file of possibly varying
	// length into an ArrayList of Ntuples: ntupleLines.
	public void createNtupleLines(String fileName, int type)
	{
		String line = null;
				    // Must use try-catch construct 
				    try{
				           BufferedReader bufferedReader = 
				                   new BufferedReader(new FileReader(fileName));
				            while((line = bufferedReader.readLine()) != null) 
				            {
				            	//System.out.println(createNtuple(parseLine(line), typeList));
				               ntupleLines.add(createNtuple(parseLine(line), type));
				             }    
				             bufferedReader.close();            
				         }
				      catch(IOException ex){}
	}
		
	// convert an ArrayList of Strings into an Ntuple of homogeneous types
	public Ntuple createNtuple(List<String> parsedLine, int type)
	{
		Ntuple ntuple = null;
				
		if(parsedLine.size() > 0)
		{
			for(int i = 0; i < parsedLine.size(); i++)
			{
				if (i == 0)
				{
					   if (type == 1)
						    ntuple = new Ntuple(Integer.parseInt(parsedLine.get(i)));
					   else if (type == 2)
							ntuple = new Ntuple(Double.parseDouble(parsedLine.get(i)));
					   else
							ntuple = new Ntuple(parsedLine.get(i));
				}
				else
				{
					   if (type == 1)
					   {
							ntuple = ntuple.append(Integer.parseInt(parsedLine.get(i)));
					   }
					   else if (type == 2)
							ntuple = ntuple.append(Double.parseDouble(parsedLine.get(i)));
					   else
							ntuple = ntuple.append(parsedLine.get(i));
				}
			}
		}
		return ntuple;
	}	
	
	// Read each heterogeneous line (record) of the input file into an ArrayList
	// of Ntuples: ntupleLines.
	public void createNtupleLines(String fileName, int[] typeList)
	{
		String line = null;
			     // Must use try-catch construct 
			     try{
			            BufferedReader bufferedReader = 
			                    new BufferedReader(new FileReader(fileName));
			            while((line = bufferedReader.readLine()) != null) 
			            {
			            	//System.out.println(createNtuple(parseLine(line), typeList));
			               ntupleLines.add(createNtuple(parseLine(line), typeList));
			             }    
			             bufferedReader.close();            
			         }
			      catch(IOException ex){}
	}
	
	// convert an ArrayList of strings into an Ntuple of heterogeneous types
	public Ntuple createNtuple(List<String> parsedLine, int[] typeList)
	{
		Ntuple ntuple = null;
			
		if(parsedLine.size() == typeList.length)
		{
			for(int i = 0; i < parsedLine.size(); i++)
			{
				if (i == 0)
				{
				    if (typeList[i] == 1)
						ntuple = new Ntuple(Integer.parseInt(parsedLine.get(i)));
					else if (typeList[i] == 2)
						ntuple = new Ntuple(Double.parseDouble(parsedLine.get(i)));
					else
						ntuple = new Ntuple(parsedLine.get(i));
				}
				else
				{
					if (typeList[i] == 1)
					{
						ntuple = ntuple.append(Integer.parseInt(parsedLine.get(i)));
					}
					else if (typeList[i] == 2)
						ntuple = ntuple.append(Double.parseDouble(parsedLine.get(i)));
					else
						ntuple = ntuple.append(parsedLine.get(i));
				}
			}
		}
		return ntuple;
	}
		
	public List<List<String>> getParsedFileLines()
	{
		return parsedFileLines;
	}
	
	public List<List<String>> parseFileLines(List<String> line)
	{
		List<List<String>> parsedFileLines = new ArrayList<List<String>>();
		for(String str : line)
		{
			parsedFileLines.add(parseLine(str));
		}
		return parsedFileLines;
	}
	
	// parse an individual line of a file into
	// an array of strings
	public List<String> parseLine(String str)
	{
		String line = str;
		List<String> lineArray = new ArrayList<String>();
		while(!line.equals(""))
		{
			lineArray.add(prefixBeforeBlank(line));
			line = suffixAfterBlank(line);
		}
		return lineArray;
	}
	
	// precondition: str is a nonempty String possibly beginning with a blank
	// Find the first blank in str and return the suffix after that blank
	// with all blanks trimmed off the front and back of that suffix.
	// If there are no blanks in str then return the empty string. 
	public String suffixAfterBlank(String str)
	{
        String suffix = "";  
		int index = str.indexOf(" ");
		if(index != -1)
		   suffix = str.substring(index).trim();
		return suffix;
	}
	
	// precondition: str is a non null String
	// Return the nonempty prefix of str before (upto) the first blank.
	// If there is no blank, return str.
	public String prefixBeforeBlank(String str)
	{  
		String prefix = "";
		while(str.length() > 0 && !str.substring(0,1).equals(" "))
		{
			prefix += str.substring(0,1);
			str = str.substring(1);
		}
		return prefix;
	}
	
	// print file lines 
	public void printFileLines()
	{
		for(int i = 0; i < fileLines.size(); i++)
		{
			System.out.println("fileLines.get(" + i + ") = " + fileLines.get(i));
		}
	}
	
	// getter method for file = array of Strings
	public List<String> getFileLines()
	{
		return fileLines;
	}

	// Read each line (record) of the input file into an ArrayList
	// of Strings: fileLines.
	public void createFileLines(String fileName)
	{
		String line = null;
		      // Must use try-catch construct 
		      try{
		             BufferedReader bufferedReader = 
		                     new BufferedReader(new FileReader(fileName));
		             while((line = bufferedReader.readLine()) != null) 
		             {
		            	 //System.out.println(line);
		                 fileLines.add(line);
		              }    
		             bufferedReader.close();            
		         }
		      catch(IOException ex){}
	}

	public List<String> createFileLinesMatching(String prefix, int from, int to, String fileName)
	{
		List<String> match = new ArrayList<String>();
		String line = null;
		      // Must use try-catch construct 
		      try{
		             BufferedReader bufferedReader = 
		                     new BufferedReader(new FileReader(fileName));
		             while((line = bufferedReader.readLine()) != null) 
		             {
		         			if(line.substring(from, to).equals(prefix))
		         			   match.add(line);
		              }    
		             bufferedReader.close();            
		         }
		      catch(IOException ex){}
		return match;      
	}
	
	// Write ArrayList of Strings to an output file on the disk.
	public void writeOutput(List<String> output, String outputFileName) 
	{     
	  try{ 
		   BufferedWriter bufferedWriter = 
			   	  new BufferedWriter(new FileWriter(new File(outputFileName),false));
		   // enhanced for loop - grab each element of the ArrayList<String> - ignore indexes
		   for (String line : output) 
		   {
		       //System.out.println(line);
		       bufferedWriter.write(line);
		       bufferedWriter.newLine();
		   }
		   bufferedWriter.close();
	    }
	  catch(IOException ex){}
    }
	
	// Write ArrayList of Ntuples to an output file on the disk.
	public void writeNtupleOutput(List<Ntuple> output, String outputFileName) 
	{     
		try{ 
			  BufferedWriter bufferedWriter = 
				     new BufferedWriter(new FileWriter(new File(outputFileName),false));
			         // enhanced for loop - grab each element of the ArrayList<String> - ignore indexes
			   for (Ntuple ntuple : output) 
			   {
			       //System.out.println(line);
			       bufferedWriter.write(ntuple.toString());
			       bufferedWriter.newLine();
			   }
			   bufferedWriter.close();
		   }
		catch(IOException ex){}
	}
}

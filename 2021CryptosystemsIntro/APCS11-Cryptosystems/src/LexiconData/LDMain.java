/********* LDMain.java ********
 * 
 *  APCS Labs 2011-2021
 *  Cryptology
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *  Copyright (c) 2011 to present John Pais. All rights reserved.
 *
 */

package LexiconData;
import java.util.*;

public class LDMain
{ 
	public static void main(String[] args)
	{
		// Start Clock
	    long start = System.currentTimeMillis();

	    // Create alphabet character array
	    List<Character> alphabet = new ArrayList<Character>();
	    for(int i = 97; i <= 122; i++)
	    {
	    	alphabet.add((char)i);
	    }
	    String dirPath = "LexiconData//";
	    String lexicon = "english26-3.txt";
	    boolean init = false;
	    // Create LexiconData object
		LexiconData lex = new LexiconData(alphabet,dirPath,lexicon,init);
		String wordInLexicon = "post";
		if(!init)
		{
		   lex.lexiconReport(wordInLexicon);
		}
		
	    // End Clock
	    long end = System.currentTimeMillis();
	    System.out.println("\ntime = " + (end-start)/1000. + " seconds");		
	}
	

}

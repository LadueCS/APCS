import java.lang.*;
public class MainLogic10 
{
	public static void main (String[] args)
	{
	    System.out.println("max1020(11, 19) = " + max1020(11, 19));
        System.out.println("max1020(19, 11) = " + max1020(19, 11));
        System.out.println("max1020(11, 9) = " + max1020(11, 9));
        System.out.println("max1020(25, 19) = " + max1020(25, 19));
	}

    // Given 2 positive int values, return the larger value that is in the
    // range 10 to 20 inclusive, or return 0 if neither is in that range.
    public static int max1020(int a, int b) 
    {
     	
        // insert your code here
        return Math.max(a,b)>20?(Math.min(a,b)>=10&&Math.min(a,b)<=20?Math.min(a,b):0):Math.max(a,b)>=10?Math.max(a, b):0;
    	
    }
	
}


public class MainLogic07 
{
	public static void main (String[] args)
	{
	    System.out.println("startHi(\"hi there\") = " + startHi("hi, there"));
	    System.out.println("startHi(\"hi\") = " + startHi("hi"));
	    System.out.println("startHi(\"hello hi\") = " + startHi("hello hi"));
	    System.out.println("startHi(\"Hi, there\") = " + startHi("Hi, there"));
	}

    // Given a string, return true if the string starts with "hi" and false otherwise.
    public static boolean startHi(String str) 
    {
     	
		// insert your code here
		return str.length()>1&&str.substring(0,2).equals("hi");
    }	
    
}

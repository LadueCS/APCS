
public class MainLogic08 
{
	public static void main (String[] args)
	{
	    System.out.println("stringTimes(\"Hi\", 2) = " + stringTimes("Hi", 2));
	    System.out.println("stringTimes(\"Hi\", 3) = " + stringTimes("Hi", 3));
	    System.out.println("stringTimes(\"Hi\", 1) = " + stringTimes("Hi", 1));
	    System.out.println("stringTimes(\"Hi\", 0) = " + stringTimes("Hi", 0));
	}

    // Given a string and a non-negative int n, return a larger string 
    // that is n copies of the original string.
    public static String stringTimes(String str, int n) 
    {
     	
    	// insert your code here
		String ret = "";
		for (int i = 0; i < n; ++i) ret += str;
		return ret;
    }
	
}

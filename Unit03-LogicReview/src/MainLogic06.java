
public class MainLogic06
{
	public static void main (String[] args)
	{
	    System.out.println("posNeg(1, -1, false) = " + posNeg(1, -1, false));
        System.out.println("posNeg(-1, 1, false) = " + posNeg(-1, 1, false));
        System.out.println("posNeg(-4, -5, true) = " + posNeg(-4, -5, true));
        System.out.println("posNeg(3, -7, true) = " + posNeg(3, -7, true));
	}

    // Given 2 int values, return true if one is negative and one is positive. 
    // Except if the parameter "negative" is true, then return true only if 
    // both are negative.
    public static boolean posNeg(int a, int b, boolean negative) 
    {
    	
    	// insert your code here
    	return negative?a<0&&b<0:(a>0&&b<0)||(a<0&&b>0);
    }
	
}

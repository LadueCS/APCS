/* Main.java 
** 
** 2011-21 APCS
** Dr John Pais
** pais.john@gmail.com
*/

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Main 
{
	public static void main(String[] args) 
	{    
		List<Integer> arr = new ArrayList<Integer>();
		arr.addAll(Arrays.asList(new Integer[]{4,1,3,5,0,2}));
		//arr.addAll(Arrays.asList(new Integer[]{5,4,3,2,1,0}));
		//arr.addAll(Arrays.asList(new Integer[]{1,0,5,3,2,4}));
		
		System.out.println("\n********** selSortInBasic **********");
		System.out.println(" original array = " + arr);
		System.out.println(" selSortInBasic = " + SortingArchive.selSortInBasic(arr,true));
		
		System.out.println("\n********** selSortDeBasic **********");
		System.out.println(" original array = " + arr);
		System.out.println(" selSortDeBasic = " + SortingArchive.selSortDeBasic(arr));
		
		System.out.println("\n************* selSortIn *************");
		System.out.println(" original array = " + arr);
		System.out.println(" selSortIn array = " + SortingArchive.selSortIn(arr));
		
		System.out.println("\n************* selSortDe *************");
		System.out.println(" original array = " + arr);
		System.out.println(" selSortDe array = " + SortingArchive.selSortDe(arr));
		
		System.out.println("\n************* bubSortIn *************");
		System.out.println(" original array = " + arr);
		System.out.println(" bubSortIn array = " + SortingArchive.bubSortIn(arr));
		
		System.out.println("\n************* bubSortDe *************");
		System.out.println(" original array = " + arr);
		System.out.println(" bubSortDe array = " + SortingArchive.bubSortDe(arr));
		
		System.out.println("\n************* insSortIn *************");
		System.out.println(" original array = " + arr);
		System.out.println(" insSortIn array = " + SortingArchive.insSortIn(arr));
		
		System.out.println("\n************* insSortDe *************");
		System.out.println(" original array = " + arr);
		System.out.println(" insSortDe array = " + SortingArchive.insSortDe(arr));
	}

}

/* SortingArchive.java 
** 
** 2011-21 APCS
** Dr John Pais
** pais.john@gmail.com
*/

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

// This class is just an archive of sorting methods. It is not a blueprint for
// objects. So, it does not have private instance variables or an explicit object 
// constructor. Further, every method is declared static which allows one to use 
// the class name alone to invoke one of the methods (see Main).

public class SortingArchive 
{
	
	// Selection sort an List in increasing order. This an O(N^2) algorithm.
	// This basic version is the easiest one to remember if you need to sort a list.
	public static List<Integer> selSortInBasic(List<Integer> input, boolean trace)
	{
		List<Integer> arr = new ArrayList<Integer>(input);
		int n = arr.size();
		int comp = 0, temp = 0, swap = 0;
		
		for (int i = 0; i < n-1; i++)
		{
			for (int j = i+1; j < n; j++)
			{
				comp++;
				if (arr.get(j) < arr.get(i)) // Put smallest in top by comparing 
				{                            // top with everything below and swapping
					temp = arr.get(i);       // only with top.
					arr.set(i,arr.get(j));
					arr.set(j,temp);
					swap++;
				}
				// trace
				if (trace)
				  System.out.println("i = " + i + " j = " + j + " arr = " + arr);	
			}
		}
		System.out.println(" comparisons = " + comp + " swaps = " + swap);
		return arr;		
	}
	
	// Selection sort an List in increasing order. This an O(N^2) algorithm.
	// This is a version without the trace feature. The ability to use the same
	// method name with different input parameters is called 'overloading' and
	// is very useful.
	public static List<Integer> selSortInBasic(List<Integer> input)
	{
		return selSortInBasic(input,false);
	}
	
	// Selection sort an List in decreasing order. This an O(N^2) algorithm.
	// This basic version is the easiest one to remember if you need to sort a list.
	public static List<Integer> selSortDeBasic(List<Integer> input, boolean trace)
	{
		List<Integer> arr = new ArrayList<Integer>(input);
		int n = arr.size();
		int comp = 0, temp = 0, swap = 0;
		
		for (int i = 0; i < n; i++)
		{
			for (int j = i+1; j < n; j++)
			{
				comp++;
				if (arr.get(j) > arr.get(i)) // Put largest in top by comparing 
				{                            // top with everything below and swapping
					temp = arr.get(i);       // only with top.
					arr.set(i,arr.get(j));
					arr.set(j,temp);
					swap++;
				}
				// trace
				if (trace)
				  System.out.println("i = " + i + " j = " + j + " arr = " + arr);	
			}
		}
		System.out.println(" comparisons = " + comp + " swaps = " + swap);
		return arr;	
	}

	// Selection sort an List in decreasing order. This an O(N^2) algorithm.
	// This is a version without the trace feature. The ability to use the same
	// method name with different input parameters is called 'overloading' and
	// is very useful.
	public static List<Integer> selSortDeBasic(List<Integer> input)
	{
		return selSortDeBasic(input,false);
	}
	
	// Selection sort an List in increasing order. This an O(N^2) algorithm.
	public static List<Integer> selSortIn(List<Integer> input, boolean trace)
	{
		List<Integer> arr = new ArrayList<Integer>(input);
		int n = arr.size();
		int comp = 0, index, temp = 0, swap = 0;
		
		for (int i = 0; i < n; i++)
		{
			index = i;                             // Put smallest in top by comparing
			for (int j = i+1; j < n; j++)          // top with everything below and swapping
			{                                      // only with top. In order to reduce
				comp++;                            // the number of swaps, this versions
				if (arr.get(j) < arr.get(index))   // repeatedly saves the index of the   
				{                                  // smallest and only swaps at the end
					index = j;                     // of the inner loop.    
				}                                   				                                      				                                     			                                     
				// trace
				if (trace)
				  System.out.println("i = " + i + " j = " + j + " arr = " + arr);
			}
			if (index != i)
			{
			   temp = arr.get(i);
			   arr.set(i,arr.get(index));
			   arr.set(index,temp);
			   swap++;
			}
		}
		System.out.println(" comparisons = " + comp + " swaps = " + swap);	
		return arr;		
	}
	
	// Selection sort an List in increasing order. This an O(N^2) algorithm.
	// This is a version without the trace feature. The ability to use the same
	// method name with different input parameters is called 'overloading' and
	// is very useful.
	public static List<Integer> selSortIn(List<Integer> input)
	{
		return selSortIn(input,false);
	}
	
	// Selection sort an List in decreasing order. This an O(N^2) algorithm.
	public static List<Integer> selSortDe(List<Integer> input, boolean trace)
	{
		List<Integer> arr = new ArrayList<Integer>(input);
		int n = arr.size();
		int comp = 0, index, temp = 0, swap = 0;
		
		for (int i = 0; i < n; i++)
		{
			index = i;                             // Put largest in top by comparing
			for (int j = i+1; j < n; j++)          // top with everything below and swapping
			{                                      // only with top. In order to reduce
				comp++;                            // the number of swaps, this versions
				if (arr.get(j) > arr.get(index))   // repeatedly saves the index of the   
				{                                  // largest and only swaps at the end
					index = j;                     // of the inner loop.    
				}                                   				                                      				                                     			                                     
				// trace
				if (trace)
				  System.out.println("i = " + i + " j = " + j + " arr = " + arr);
			}
			if (index != i)
			{
			   temp = arr.get(i);
			   arr.set(i,arr.get(index));
			   arr.set(index,temp);
			   swap++;
			}
		}
		System.out.println(" comparisons = " + comp + " swaps = " + swap);	
		return arr;		
	}
	
	// Selection sort an List in decreasing order. This an O(N^2) algorithm.
	// This is a version without the trace feature. The ability to use the same
	// method name with different input parameters is called 'overloading' and
	// is very useful.
	public static List<Integer> selSortDe(List<Integer> input)
	{
		return selSortDe(input,false);
	}
	
	// Bubble sort an List in increasing order. This an O(N^2) algorithm.
	public static List<Integer> bubSortIn(List<Integer> input, boolean trace)
	{
		List<Integer> arr = new ArrayList<Integer>(input);
		int n = arr.size();
		int comp = 0, temp = 0, swap = 0;
		
		for (int i = 0; i < n-1; i++)
		{
			for (int j = n-1; j > i; j--)      // Bubble smallest up from bottom using 
			{                                  // adjacent comparisons and swaps.
				comp++;
				if (arr.get(j) < arr.get(j-1))
				{
					temp = arr.get(j-1);
					arr.set(j-1,arr.get(j));
					arr.set(j,temp);
					swap++;
				}
				// trace
				if (trace)
				  System.out.println("i = " + i + " j = " + j + " arr = " + arr);	
			}
		}
		System.out.println(" comparisons = " + comp + " swaps = " + swap);
		return arr;	    
	}
	
	// Bubble sort an List in increasing order. This an O(N^2) algorithm.
	// This is a version without the trace feature. The ability to use the same
	// method name with different input parameters is called 'overloading' and
	// is very useful.
	public static List<Integer> bubSortIn(List<Integer> input)
	{
		return bubSortIn(input,false);
	}
	
	// Bubble sort an List in decreasing order. This an O(N^2) algorithm.
	public static List<Integer> bubSortDe(List<Integer> input, boolean trace)
	{
		List<Integer> arr = new ArrayList<Integer>(input);
		int n = arr.size();
		int comp = 0, temp = 0, swap = 0;
		
		for (int i = 0; i < n-1; i++)
		{
			for (int j = n-1; j > i; j--)      // Bubble largest up from bottom using 
			{                                  // adjacent comparisons and swaps.
				comp++;
				if (arr.get(j) > arr.get(j-1))
				{
					temp = arr.get(j-1);
					arr.set(j-1,arr.get(j));
					arr.set(j,temp);
					swap++;
				}
				// trace
				if (trace)
				  System.out.println("i = " + i + " j = " + j + " arr = " + arr);	
			}
		}
		System.out.println(" comparisons = " + comp + " swaps = " + swap);
		return arr;	    
	}
	
	// Bubble sort an List in decreasing order. This an O(N^2) algorithm.
	// This is a version without the trace feature. The ability to use the same
	// method name with different input parameters is called 'overloading' and
	// is very useful.
	public static List<Integer> bubSortDe(List<Integer> input)
	{
		return bubSortDe(input,false);
	}
	
	// Insertion sort an List in increasing order. This an O(N^2) algorithm.
	public static List<Integer> insSortIn(List<Integer> input, boolean trace)
	{
		List<Integer> arr = new ArrayList<Integer>(input);
		int n = arr.size();
		int comp = 0, temp = 0, swap = 0;
		for (int i = 0; i < n-1; i++)
		{
			for (int j = i+1; j > 0; j--)      
			{                                  // Bubble up first element outside (current) sorted  
				comp++;                        // prefix into (current) sorted prefix using adjacent  
				if (arr.get(j) < arr.get(j-1)) // comparisons and swaps, which has the effect of 
				{                              // inserting it into its proper (current) position in
					temp = arr.get(j-1);       // the (current) sorted prefix.
					arr.set(j-1,arr.get(j));
					arr.set(j,temp);
					swap++;
				}
				else
				{
					j = 1;
				}
				// trace
				if (trace)
				  System.out.println("i = " + i + " j = " + j + " arr = " + arr);	
			}
		}
		System.out.println(" comparisons = " + comp + " swaps = " + swap);
		return arr;	    
	}
	
	// Insertion sort an List in increasing order. This an O(N^2) algorithm.
	// This is a version without the trace feature. The ability to use the same
	// method name with different input parameters is called 'overloading' and
	// is very useful.
	public static List<Integer> insSortIn(List<Integer> input)
	{
		return insSortIn(input,false);
	}
	
	
	// Insertion sort an List in decreasing order. This an O(N^2) algorithm.
	public static List<Integer> insSortDe(List<Integer> input, boolean trace)
	{
		List<Integer> arr = new ArrayList<Integer>(input);
		int n = arr.size();
		int comp = 0, temp = 0, swap = 0;
		
		for (int i = 0; i < n-1; i++)
		{
			for (int j = i+1; j > 0; j--)       
			{                                  // Bubble up first element outside (current) sorted 
				comp++;                        // prefix into (current) sorted prefix using adjacent  
				if (arr.get(j) > arr.get(j-1)) // comparisons and swaps, which has the effect of 
				{                              // inserting it into its proper (current) position in
					temp = arr.get(j-1);       // the (current) sorted prefix.
					arr.set(j-1,arr.get(j));
					arr.set(j,temp);
					swap++;
				}
				else
				{
					j = 1;
				}
				// trace
				if (trace)
				  System.out.println("i = " + i + " j = " + j + " arr = " + arr);	
			}
		}
		System.out.println(" comparisons = " + comp + " swaps = " + swap);
		return arr;	    
	}
	
	// Insertion sort an List in decreasing order. This an O(N^2) algorithm.
	// This is a version without the trace feature. The ability to use the same
	// method name with different input parameters is called 'overloading' and
	// is very useful.
	public static List<Integer> insSortDe(List<Integer> input)
	{
		return insSortDe(input,false);
	}
	
/*********************************************************************** 
 *** Homework: Create integer array version of all of the above methods.
 *** The first two are already completed below. 	
 *** After you create these methods, also test them in Main. 
 ***********************************************************************/
	
	// Selection sort an Array in increasing order. This an O(N^2) algorithm.
	// This basic version is the easiest one to remember if you need to sort a list.
	public static int[] selSortInBasic(int[] input, boolean trace)
	{
		int[] arr = input.clone();
		int n = arr.length;
		int comp = 0, temp = 0, swap = 0;
		
		for (int i = 0; i < n-1; i++)
		{
			for (int j = i+1; j < n; j++)
			{
				comp++;
				if (arr[j] < arr[i]) // Put smallest in top by comparing 
				{                            // top with everything below and swapping
					temp = arr[i];       // only with top.
					arr[i] = arr[j];
					arr[j] = temp;
					swap++;
				}
				// trace
				if (trace)
				  System.out.println("i = " + i + " j = " + j + " arr = " + arr);	
			}
		}
		System.out.println(" comparisons = " + comp + " swaps = " + swap);
		return arr;		
	}
	
	public static int[] selSortInBasic(int[] input)
	{
		return selSortInBasic(input,false);
	}
}

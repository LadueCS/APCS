import java.util.*;
public class Quicksort {
    public static void main(String args[]) {
        ArrayList<Integer> A = new ArrayList<Integer>();
        int N = 100000;
        for (int i = 0; i < N; ++i) A.add((int)(N*Math.random()));

        // System.out.println(A);
        qsort(A, 0, A.size()-1);
        // System.out.println(A);

        for (int i = 1; i < N; ++i) if (A.get(i-1) > A.get(i)) System.out.println(i); // verify sorted
    }
    public static void qsort(ArrayList<Integer> A, int l, int r) {
        if (l > r) return;
        Collections.swap(A, l+(int)((r-l)*Math.random()), r); // random partition
        int p = A.get(r), i = l;
        for (int j = l; j < r; ++j) if (A.get(j) < p) Collections.swap(A, i++, j);
        Collections.swap(A, i, r);
        qsort(A, l, i-1); qsort(A, i+1, r);
    }
}

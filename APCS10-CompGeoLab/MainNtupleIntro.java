import java.util.*;

public class MainNtupleIntro 
{

	public static void main(String[] args) 
	{
		// Example use of Ntuple class
		System.out.println("***** Example use of Ntuple class *****");
		System.out.println();
		Ntuple ntuple1 = new Ntuple(0,1,2,3,4);
		System.out.println("ntuple1 = " + ntuple1);
		System.out.println("ntuple1.getkth(0) = " + ntuple1.getkth(0));
		
		Ntuple ntuple2 = new Ntuple("aa","bb","cc","dd");
		System.out.println("\nntuple2 = " + ntuple2);
		System.out.println("ntuple2.getkth(1) = " + ntuple2.getkth(1));
		
		Ntuple ntuple3 = new Ntuple(0,"aa",1,"bb",2,"cc",3,"dd");
		System.out.println("\nntuple3 = " + ntuple3);
		System.out.println("ntuple3.getkth(2) = " + ntuple3.getkth(2));
		
		ArrayList<Ntuple> ntuple4arr = new ArrayList<Ntuple>();
		ntuple4arr.add(ntuple1);
		ntuple4arr.add(ntuple2);
		ntuple4arr.add(ntuple3);
		System.out.println("\nntuple4arr = " + ntuple4arr);
		System.out.println("ntuple4arr.get(2).getkth(2) = " + ntuple4arr.get(2).getkth(2));
		
		// Ntuple class permits the combination of an arbitrary number of heterogeneous types.
		Ntuple ntuple5 = new Ntuple(ntuple1,ntuple2,ntuple3,ntuple4arr);
		System.out.println("\nNtuple class permits the combination of an arbitrary number of heterogeneous types:");
		System.out.println("\nntuple5 = " + ntuple5);
		System.out.println("ntuple5.getkth(0) = " + ntuple5.getkth(0));
		System.out.println("ntuple5.getkth(1) = " + ntuple5.getkth(1));
		System.out.println("ntuple5.getkth(2) = " + ntuple5.getkth(2));
		System.out.println("ntuple5.getkth(3) = " + ntuple5.getkth(3));
		System.out.println();
		
	   /**********************************************************************
		** Example of sorting using NtupleComparator class which implements **
		** the Comparator interface                                         **
		**********************************************************************/
		
		System.out.println("***** Sorting Using NtupleComparator *****");
		System.out.println();
		ReadWriteFile rwf = new ReadWriteFile("PersonDataStrings.txt");
		ArrayList<String> fileLines = rwf.getFileLines();
		ArrayList<ArrayList<String>> parsedFileLines = rwf.getParsedFileLines();
		// Check fileLines
		System.out.println("fileLines = " + fileLines);
		// Check parsedFileLines
		System.out.println("\nparsedFileLines = " + parsedFileLines);
		
		// Problem 1. 
		// Create ntuples, an ArrayList of Ntuple objects, which is comprised 
		// of Person data and add a new Double at index = 5, which is age/10.0
		// a person's number of decades.
		ArrayList<Ntuple> ntuples = new ArrayList<Ntuple>();
		Ntuple tempNtuple;
		for(ArrayList<String> arr : parsedFileLines)
		{
		  
		  tempNtuple = new Ntuple(
						    Integer.parseInt((String)arr.get(0).substring(7,arr.get(0).indexOf(","))),
						    arr.get(1).substring(0,arr.get(1).indexOf(",")),
						    arr.get(2).substring(0,arr.get(2).indexOf(",")),
						    Integer.parseInt(arr.get(3).substring(0,arr.get(3).indexOf(","))),
						    Integer.parseInt(arr.get(4).substring(0,arr.get(4).indexOf(")"))),
						    Integer.parseInt(arr.get(3).substring(0,arr.get(3).indexOf(",")))/10.0
						         );	
		  ntuples.add(tempNtuple);
		}
		
		// Check ntuples
		System.out.println("\nntuples = " + ntuples);
		
		// Note that since the getkth method for ntuples returns an object each
		// component of the ntuple must still be cast to the appropriate data type.
		System.out.println("\nExamples of necessary casts:");
		System.out.println("(Integer)ntuples.get(0).getkth(0)*2 = " + (Integer)ntuples.get(0).getkth(0)*2);
		System.out.println("((String)ntuples.get(0).getkth(1)).substring(0,3) = " +((String)ntuples.get(0).getkth(1)).substring(0,3));
		System.out.println("(Double)ntuples.get(0).getkth(5)*2 = " + (Double)ntuples.get(0).getkth(5)*2);
		
		// Problem 2. 
		// Sort ntuples consecutively on multiple different fields with different argument types.
		// Note that when sorting on multiple fields you should sort first on the least important 
		// field, and continue this way until the most important field which is sorted last.
		
		System.out.println("\nSort first on the least important field to most important field:");
		
		System.out.println("\n ntuples = " + ntuples);
		
		// Sort ntuples on index 2(female/male): a String, argType = 3, increasing = true
		NtupleComparator nc2 = new NtupleComparator(2,3,true);
		ArrayList<Ntuple> ntuples2 = new ArrayList<Ntuple>(ntuples);
		nc2.sortNtupleList(ntuples2);
		System.out.println("\n Sort ntuples on index 2(female/male): a String, argType = 3, increasing = true");
		System.out.println(" ntuples2 = " + ntuples2 );
		
		// Sort ntuples2 on index 4(grade level): an Integer, argType = 1, increasing = true
		NtupleComparator nc4 = new NtupleComparator(4,1,true);
		ArrayList<Ntuple> ntuples4 = new ArrayList<Ntuple>(ntuples2);
		nc4.sortNtupleList(ntuples4);
		System.out.println("\n Sort ntuples2 on index 4(grade level): an Integer, argType = 1, increasing = true");
		System.out.println(" ntuple4 = " + ntuples4);	
		
		// Sort ntuples4 on index 5(decades): a Double, argType = 2, increasing = false
		NtupleComparator nc5 = new NtupleComparator(5,2,false);
		ArrayList<Ntuple> ntuples5 = new ArrayList<Ntuple>(ntuples4);
		nc5.sortNtupleList(ntuples5);
		System.out.println("\n Sort ntuples4 on index 5(decades): a Double, argType = 2, increasing = false");
		System.out.println(" ntuples5 = " + ntuples5);

		
		System.out.println("\n Final list of ntuples sorted multiple times:" + "\n");
		// Check that the original ntuples is properly sorted by the three different
		// components above, ultimately into ntuples2.
		for(Ntuple ntuple : ntuples5)
		{
			System.out.println(" " + ntuple);
		}
		
	}

}

/********  NtupleComparator.java *********
 *  
 *  APCS Project 2011-2021
 *  Computational Geometry
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/*
  java.util.Comparator interface declares two methods,
  1) public int compare(Object object1, Object object2) and
  2) boolean equals(Object object)

  User defined Java comparator.
  To create a custom java comparator implement the Comparator interface and
  override the compare method.
 
  The comparator below compares Ntuples at the kth component as an Integer
  or String or Double, and in incresasing or decreasing order.
*/
 
public class NtupleComparator implements Comparator<Ntuple>
{
    private int index;
    private int argType;
    private boolean increasing;
       
    public NtupleComparator(int index)
    {
    	this.index = index;
    	argType = 1;
    	increasing = true;
    }
	
    public NtupleComparator(int index, int argType, boolean increasing)
    {
    	this.index = index;
    	this.argType = argType; // 1 = Integer, 2 = Double, anything else = String
    	this.increasing = increasing;
    }
    
    public int compare(Ntuple ntuple1, Ntuple ntuple2)
    {
    	
    	// The compare method compares its two arguments, returning a negative integer, 0, or 
    	// a positive integer depending on whether the first argument is less than, equal to, or
    	// greater than the second. If either of the arguments has an inappropriate type for the 
    	// Comparator, the compare method throws a ClassCastException.
    	
        // Ntuple kth components are of type Object, so we have to downcast them to Integer or 
    	// String or Double objects.
        
        if(argType == 1)
    	{
    	    Integer kthValue1 = (Integer)ntuple1.getkth(index);        
            Integer kthValue2 = (Integer)ntuple2.getkth(index);
    		
            return compareInt(kthValue1, kthValue2);
    	}
        else if(argType == 2)
        {
            Double kthValue1 = (Double)ntuple1.getkth(index);        
            Double kthValue2 = (Double)ntuple2.getkth(index);
            
            return compareDouble(kthValue1, kthValue2);
        }
    	else
    	{
    	    String kthValue1 = (String)ntuple1.getkth(index);        
            String kthValue2 = (String)ntuple2.getkth(index);
            
            return compareStr(kthValue1, kthValue2);
    	}
    }
 
    public int compareInt(int value1, int value2)
    {
    	if(increasing)
    	{
    		if(value1 > value2)
                return 1;
            else if(value1 < value2)
                return -1;
            else
                return 0;   
    	}
    	else
    	{
    		if(value2 > value1)
                return 1;
            else if(value2 < value1)
                return -1;
            else
                return 0;   
    	}   	
    }
    
    public int compareDouble(double value1, double value2)
    {
    	if(increasing)
    	{
    		if(value1 > value2)
                return 1;
            else if(value1 < value2)
                return -1;
            else
                return 0;   
    	}
    	else
    	{
    		if(value2 > value1)
                return 1;
            else if(value2 < value1)
                return -1;
            else
                return 0;   
    	}   	
    }
    
    public int compareStr(String str1, String str2)
    {
    	if(increasing)
    	{
    		return str1.compareTo(str2);
    	}
    	else
    	{
    		return str2.compareTo(str1);
    	}   	
    }
    
    // This method provides 6 sorting choices at any component of the Ntuple,
    // according to the constructor signature:
    // NtupleComparator(int index, boolean argType, boolean increasing)
    // NtupleComparator(    k,             1,               true) - sort kth component, an int, in increasing order
    // NtupleComparator(    k,             1,               false) - sort kth component, an int, in decreasing order
    // NtupleComparator(    k,             2,               true) - sort kth component, a double, in increasing order
    // NtupleComparator(    k,             2,               false) - sort kth component, an double, in decreasing order
    // NtupleComparator(    k,         other integer,       true) - sort kth component, a string, in increasing order 
    // NtupleComparator(    k,         other integer,       false) - sort kth component, a string, in decreasing order
    public void sortNtupleList(ArrayList<Ntuple> ntuples)
    {
    	// Java uses merge sort.
        Collections.sort(ntuples,this);
    }
    
    // Given an ArrayList of Ntuples and given the current index and argType,
    // collapse multiple occurrences of the same values at this index into
    // one new Ntuple, recording the total at a new index appended to the end.
    // Return an ArrayList of these collapsed ntuples.
    // Precondition: the list of ntuples is compatible with this instance of 
    // NtupleComparator and the current values of the variables: index, argType 
    // and increasing.
    public ArrayList<Ntuple> collapse(ArrayList<Ntuple> ntuples, boolean append)
	{
    	//NtupleComparator nc = new NtupleComparator(index, argType, increasing);
    	ArrayList<Ntuple> sorted = new ArrayList<Ntuple>(ntuples);
    	sortNtupleList(sorted);
    	
		ArrayList<Ntuple> collapse = new ArrayList<Ntuple>();
		if(0 <= index && index < sorted.get(0).getN())
		{
			int j;
			Ntuple tempNtuple;
			for(int i = 0; i < sorted.size(); i++)
			{
	            j = 1;
				while(i+j < sorted.size() && compare(sorted.get(i),sorted.get(i+j)) == 0)
				{
					j++;
				}
				if(append)
				{
				   tempNtuple = sorted.get(i).append(j);
				}
				else
				{
				   tempNtuple = sorted.get(i);
				}
				collapse.add(tempNtuple);
				i = i+j-1;
			}
		}
		return collapse;
	}
    
    
    
  } 
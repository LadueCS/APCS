/*****   MainSecTD.java
 *  
 *  APCS Project 2011-21
 *  Computational Geometry
 *  Dr. John Pais 
 *  pais.john@gmail.com
 * 
 *  GSConvexHull.java is a modified version of original code created by 
 *  Robert Sedgewick and Kevin Wayne. http://algs4.cs.princeton.edu/home/
 * 
 *  Generate random points and compute the convex hull using the
 *  Graham scan algorithm. The convex hull of a set of points in the 
 *  plane is the smallest subset of boundary points (fence posts) that 
 *  can be used to enclose the whole set of points within the shortest 
 *  perimeter fence. 
 *
 *  May be floating-point issues if x- and y-coordinates are not integers.
 *
 *  In addition, the following methods and graphics objects of the
 *  convex hull are created:
 *  (1) the diameter and its length, 
 *  (2) the interior segments that intersect the diameter, and
 *  (3) the centroid, centroid radius, and centroid circle.
 *
 *  This version of Main reads in test data sets and outputs summary
 *  comparative values computed using hull centroid and using the 
 *  smallest enclosing circle.
 *
 *********************************************************************/

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class MainSecTD
{
	private static final String inputFile = "secTestDatasets.in";
	
	public static void main(String[] args) throws IOException
	{
		// Start Clock
	    long startClock = System.currentTimeMillis();
	    ArrayList<Point2D[]> arr = new ArrayList<Point2D[]>();
	   
		BufferedReader in = new BufferedReader(new FileReader(new File(inputFile)));
		String output = new String();
		int datasetsCount = Integer.valueOf(in.readLine());    // in.readLine() reads only the
		System.out.println("datasetsCount = " + datasetsCount); // current next line when invoked.
		for (int i = 1; i <= datasetsCount; i++)
		{
			int numPoints = Integer.valueOf(in.readLine());
			System.out.println("\nDataset # " + i +" numPoints = " + numPoints);
			Point2D[] points = new Point2D[numPoints];
			String line;
			
			for (int j = 0; j < points.length; j++)
			{
				line = in.readLine();
				points[j] = new Point2D(Double.parseDouble(line.substring(1,line.indexOf(","))),
						                Double.parseDouble(line.substring(line.indexOf(" ")+1,
						                		                          line.indexOf(")"))));
			}
			
			// needed for displayCompGeo methods below (outside of these loops)
			arr.add(points);
			
			GSConvexHull gs = new GSConvexHull(points);
	        
			System.out.println("# hullPoints = " + gs.getHullPoints().size());
	        System.out.println(gs);
	        
	        System.out.println("hullCentroid = " + gs.getHullCentroid() + 
	        		           " hullCentroidRadius = " + gs.getHullCentroidRadius());
	       
	        System.out.println("secCenter = " + gs.getSecCenter() + 
			                   " secRadius = " + gs.getSecRadius());
	        
	        System.out.println("hullCentroidRadius - secRadius = " +
	                             (gs.getHullCentroidRadius()-gs.getSecRadius()));
		}
		in.close();
		
		// display all test data and images
		// sleeps for three seconds in between images (see method below)
		displayCompGeo(arr);
		
		// display test data and image at a specific index, e.g. index = 9
		//displayCompGeo(arr,9);
		
		// End Clock
	    long endClock = System.currentTimeMillis();
	    System.out.println("\nlab10 time = " + (endClock-startClock)/1000. + " seconds"); 
	}
	

	public static void displayCompGeo(ArrayList<Point2D[]> arr, int i)
	{
		if(i < arr.size())
		{
		   GSConvexHull gscg = new GSConvexHull(arr.get(i)); 
	       gscg.drawHull(0,true);
           System.out.println("\nDataset # " + (i+1) + " = " + Arrays.toString(arr.get(i)));
		}
	}
	public static void displayCompGeo(ArrayList<Point2D[]> arr)
	{
		for(int i = 0; i< arr.size(); i++)
		{	
		  try 
		  {
			  GSConvexHull gscg = new GSConvexHull(arr.get(i)); 
		      gscg.drawHull(0,true);
	          System.out.println("\nDataset # " + (i+1) + " = " + Arrays.toString(arr.get(i)));
	          Thread.sleep(3000);
	      }
	      catch(InterruptedException ex)
	      {
	          Thread.currentThread().interrupt();
	      }
		}
	}
	
}
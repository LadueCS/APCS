/*****   GSConvexHull.java
 *  
 *  APCS Labs 2011-21
 *  Computational Geometry
 *  Dr. John Pais 
 *  pais.john@gmail.com
 * 
 *  Modified and extended version of original convex hull code created by 
 *  Robert Sedgewick and Kevin Wayne. http://algs4.cs.princeton.edu/home/
 * 
 *  Generate random points and compute the convex hull using the
 *  Graham scan algorithm. The convex hull of a set of points in the 
 *  plane is the smallest subset of boundary points (fence posts) that 
 *  can be used to enclose the whole set of points within the shortest 
 *  perimeter fence. 
 *
 *  May be floating-point issues if x- and y-coordinates are not integers.
 *  
 *  In addition, the following methods and graphics objects of the
 *  convex hull are created:
 *  (1) the diameter and its length, 
 *  (2) the interior segments that intersect the diameter, and
 *  (3) the centroid, centroid radius, and centroid circle.
 *
 *********************************************************************/

import java.util.*;

public class GSConvexHull 
{
	private int N;
    private Point2D[] points;
    private Stack<Point2D> hullStack = new Stack<Point2D>();
    private ArrayList<Point2D> hullPoints = new ArrayList<Point2D>();  
    private ArrayList<Ntuple> nearFarNtuples = new ArrayList<Ntuple>();  // Problem 2.
    private Ntuple diameterNtuple;  // Problem 6.
    private Double diameter;  // Problem 8.
    private ArrayList<Point2D> interiorPoints = new ArrayList<Point2D>(); // Problem 11.
    private ArrayList<Segment2D> intSegsIntersectingDiameter = new ArrayList<Segment2D>();  // Problem 13.
    private Point2D hullCentroid;  // Problem 16.
    private Double hullCentroidRadius;  // Problem 17.

    // Constructor to generate N random points.
    public GSConvexHull(int N) 
    {
    	this.N = N;
    	points = new Point2D[N];
    	for (int i = 0; i < N; i++) 
        {
            int x = StdRandom.uniform(100);
            int y = StdRandom.uniform(100);
            points[i] = new Point2D(x, y);
        }
        init();
        createHullPoints();
        createNearFarNtuples();  // Problem 2.
        diameterNtuple = createDiameterNtuple(this); // Problem 6.
        diameter = createDiameter(this); // Problem 8.
        createInteriorPoints(); // Problem 11.
        createIntSegsIntersectingDiameter(); // Problem 13.
        createHullCentroid(); // Problem 16.
        createHullCentroidRadius(); // Problem 17.
        createSec();
    }
    
    // Constructor to use a given array of points.
    public GSConvexHull(Point2D[] points) 
    {
        this.points = points;
        N = points.length;
        init();
        createHullPoints();
        createNearFarNtuples();  // Problem 2.
        diameterNtuple = createDiameterNtuple(this);  // Problem 6.
        diameter = createDiameter(this);  // Problem 8.
        createInteriorPoints();  // Problem 11.
        createIntSegsIntersectingDiameter();  // Problem 13.
        createHullCentroid();  // Problem 16.
        createHullCentroidRadius();  // Problem 17.
        createSec();
    }

    // Problem 1. This method should return whether
    // or not a point is contained in the convex hull.
    public boolean inHull(Point2D point)
    {
    	return hullPoints.contains(point);
    }
    
    // Sort method to be used in Problem 2. This method compares the
    // distance from a given point to each of the elements in an 
    // array of points, and sorts the array of points in increasing
    // order with respect to the distance from the given point.
    public void sortDistanceToOrder(Point2D point, Point2D[] points)
    {
        Arrays.sort(points, point.distanceToOrder());
    }
    
    // Problem 2. This method initializes the nearFarNtuples 
    // private instance variable. For each point in the points
    // array, use the sort method above to create an ntuple 
    // of the form Ntuple(point, point1, point2) where point1
    // is the point nearest to point, and point2 is the point
    // farthest from point. Add each of these ntuples to 
    // the nearFarNtuples ArrayList.
    public void createNearFarNtuples()
    {
    	// insert your code here
    	for (int i = 0; i < N; ++i) {
            Point2D[] tmp = points;
            sortDistanceToOrder(points[i], tmp);
            nearFarNtuples.add(new Ntuple(points[i], tmp[1], tmp[N-1]));
    	}
    }
    
    // Problem 3. Getter method for nearFarNtuples.
    public ArrayList<Ntuple> getNearFarNtuples()
    {
    	// insert your code here
    	return nearFarNtuples;
    }
    
    // Problem 4. This method returns the point
    // nearest to the given point. Use the
    // nearFarNtuples private instance variable.
    public Point2D getNearestTo(Point2D point)
    {
    	// insert your code here
    	for (var p : nearFarNtuples)
            if (p.getkth(0).equals(point)) return (Point2D)p.getkth(1);
        return point;
    }
    
    // Problem 5. This method returns the point
    // farthest from the given point. Use the
    // nearFarNtuples private instance variable.
    public Point2D getFarthestFrom(Point2D point)
    {
    	// insert your code here
    	for (var p : nearFarNtuples)
            if (p.getkth(0).equals(point)) return (Point2D)p.getkth(2);
        return point;
    }
    
    // Problem 6. The diameter of the convex hull is defined
    // to be the line segment joining the two farthest points
    // on the convex hull. This method should use the 
    // nearFarNtuples private instance variable to (1) find
    // the Ntuple(point0, point1, point2) such that point0 is 
    // in the convex hull and the distance from point0 to point2
    // is a maximum for all points on the convex hull, and
    // (2) return the ntuple: Ntuple(point0, point2, maxDistance),
    // where p1 is omitted and this maximum distance, maxDistance,
    // is included at index 2. Also, use the appropriate Point2D
    // distance method(s) and NtupleComparator.
    public Ntuple createDiameterNtuple(GSConvexHull gs)
    {
    	// insert your code here
    	Ntuple ret = new Ntuple(nearFarNtuples.get(0).getkth(0), nearFarNtuples.get(0).getkth(2), ((Point2D)nearFarNtuples.get(0).getkth(0)).distanceTo((Point2D)nearFarNtuples.get(0).getkth(2)));
    	for (int i = 1; i < N; ++i) {
            if (((Point2D)nearFarNtuples.get(i).getkth(0)).distanceTo((Point2D)nearFarNtuples.get(i).getkth(2)) > (double)ret.getkth(2)) {
                ret = new Ntuple(nearFarNtuples.get(i).getkth(0), nearFarNtuples.get(i).getkth(2), ((Point2D)nearFarNtuples.get(i).getkth(0)).distanceTo((Point2D)nearFarNtuples.get(i).getkth(2)));
            }
    	}
    	return ret;
    }
        
    // Problem 7. Getter method for private instance variable: diameterNtuple.
    public Ntuple getDiameterNtuple()
    {
    	// insert your code here
    	return diameterNtuple;
    }
    
    // Problem 8. Return the length of the diameter of the convex hull.
    public Double createDiameter(GSConvexHull gs)
    {
    	// insert your code here
    	return (double)diameterNtuple.getkth(2);
    }
    
    // Problem 9. Getter method for private instance variable: diameter.
    public Double getDiameter()
    {
    	// insert your code here
    	return diameter;
    }
    
    // (**) Show More Graphics Method.
    // In order to draw the graphics you create in the methods
    // below, this method should be edited after completing each 
    // of the following: Problem 10, Problem 15, and Problem 18.
    public void drawHull(int delay, boolean showMoreGraphics)
    {
    	drawHull(delay);
    	if(showMoreGraphics)
    	{
    		// Draw diameter segment and text.
    		drawDiameterAndText(delay);  // Problem 10.
//     		System.out.println("Draw diameter");
    		// Draw interior segments intersecting diameter
        	drawIntSegsIntersectingDiameter(delay);  // Problem 15.
    		
        	// Draw centroid and circle.
        	drawCentroidAndCircle(delay); // Problem 18.

            drawSec(delay);
    	}
    }
    
    // Problem 10. Create a method to draw the diameter of
    // the convex hull and to label the highest point of
    // the diameter with its length. 
    // Use the private instance variables: diameterNtuple
    // and diameter. Also, use the Point2D class, the 
    // the StdDraw class and the appropriate methods 
    // that you need. In order to run this method,
    // uncomment the appropriate line in the drawHull
    // method labeled with (**) above.
    public void drawDiameterAndText(int delay)
    {
        var d = getDiameterNtuple();
    	// Draw diameter segment
    	// insert your code here
    	StdDraw.setPenColor(StdDraw.RED);
    	StdDraw.setPenRadius(.005);
    	System.out.println(((Point2D)d.getkth(0)).x());
    	System.out.println(((Point2D)d.getkth(0)).y());
    	System.out.println(((Point2D)d.getkth(1)).x());
    	System.out.println(((Point2D)d.getkth(1)).y());
    	StdDraw.line(((Point2D)d.getkth(0)).x(), ((Point2D)d.getkth(0)).y(), ((Point2D)d.getkth(1)).x(), ((Point2D)d.getkth(1)).y());
    	StdDraw.show();
		
		// Draw diameter text
    	// insert your code here
    	StdDraw.setPenColor(StdDraw.MAGENTA);
    	StdDraw.text(((Point2D)d.getkth(0)).x(), ((Point2D)d.getkth(0)).y(), "Diameter");
    	StdDraw.show(delay);
    }
    
    // Problem 11. The interior of the convex hull is the set
    // of all points in the original set of points that are
    // not on the convex hull. This method initializes the 
    // interiorPoints private instance variable. 
    public void createInteriorPoints()
    {
    	// insert your code here
    	interiorPoints = new ArrayList<Point2D>();
    	for (int i = 0; i < N; ++i)
            if (!inHull(points[i])) interiorPoints.add(points[i]);
    }
    
    // Problem 12. Getter method for private instance variable: interiorPoints.
    public ArrayList<Point2D> getInteriorPoints()
    {
    	// insert your code here
    	return interiorPoints;
    }
    
    // Problem 13. An interior segment intersecting the diameter of
    // the convex hull is a line segment, which is drawn between two 
    // interior points, that intersects the diameter of the convex hull. 
    // This method initializes the intSegsIntersectingDiameter which
    // is an ArrayList<Segment2D> containing all the interior segements
    // intersecting the diameter. Use diameterNtuple, and the classes
    // Point2D and Segment2D together with the appropriate method(s). 
    public void createIntSegsIntersectingDiameter() 
    {
    	Segment2D diamSeg = new Segment2D((Point2D)diameterNtuple.getkth(0),
    			                          (Point2D)diameterNtuple.getkth(1));
    	// insert your code here
    	for (int i = 0; i < points.length; ++i) if (hullPoints.indexOf(points[i]) == -1) {
            for (int j = i+1; j < points.length; ++j) if (hullPoints.indexOf(points[j]) == -1) {
                Segment2D seg = new Segment2D(points[i], points[j]);
                if (seg.segmentsIntersect(diamSeg, seg)) intSegsIntersectingDiameter.add(seg);
            }
        }
    }
    
    // Problem 14. Getter method for private instance variable: 
    // intSegsIntersectingDiameter.
    public ArrayList<Segment2D> getIntSegsIntersectingDiameter() 
    {
       // insert your code here
       return intSegsIntersectingDiameter;
    }
    
    // Problem 15. Create a method to draw all the interior line
    // segments that intersect the diameter of the convex hull. 
    // Use the private instance variable: intSegsIntersectingDiameter,
    // the Point2D class, the StdDraw class, and the appropriate methods 
    // that you need. Also, in order to run this method, uncomment the 
    // appropriate line in the drawHull method labeled with (**) above.
    public void drawIntSegsIntersectingDiameter(int delay)
    {
    	// Draw interior segments intersecting diameter
		// insert your code here
		for (var s : intSegsIntersectingDiameter) {
            var d = new Ntuple(s.getP1(), s.getP2());
            StdDraw.setPenColor(StdDraw.MAGENTA);
            StdDraw.line(((Point2D)d.getkth(0)).x(), ((Point2D)d.getkth(0)).y(), ((Point2D)d.getkth(1)).x(), ((Point2D)d.getkth(1)).y());
            StdDraw.show(delay);
		}
    }
    
    // Problem 16. The centroid of the convex hull is the
    // point (xBar, yBar) where xBar is the average of
    // the x-coordinates of points on the convex hull and
    // y-Bar is the average of the y-coordinates on the
    // convex hull. This method initializes the hullCentroid
    // private instance variable.
    public void createHullCentroid()
    {
    	// insert your code here
    	hullCentroid = new Point2D(0, 0);
    	for (var p : hullPoints)
            hullCentroid = new Point2D(hullCentroid.x()+p.x(), hullCentroid.y()+p.y());
        hullCentroid = new Point2D(hullCentroid.x()/hullPoints.size(), hullCentroid.y()/hullPoints.size());
    }
    
    // Problem 17. The hull centroid radius is the 
    // farthest distance from the hull centroid to
    // a point on the convex hull. This method 
    // initializes the hullCentroidRadius private 
    // instance variable.
    public void createHullCentroidRadius()
    {
    	// insert your code here
    	hullCentroidRadius = 0.0;
    	for (var p : hullPoints)
            if (hullCentroid.distanceTo(p) > hullCentroidRadius) hullCentroidRadius = hullCentroid.distanceTo(p);
    }
    
    // Problem 18. Create a method to draw the hull centroid
    // and the hull centroid circle.
    // Use the private instance variables: hullCentroid and
    // hullCentroidRadius. Also, use the StdDraw class and the 
    // appropriate methods that you need. 
    // In order to run this method, uncomment the appropriate
    // line in the drawHull method labeled with (**) above.
    public void drawCentroidAndCircle(int delay)
    {
    	// Draw hull centroid
    	// insert your code here
    	StdDraw.setPenColor(StdDraw.ORANGE);
    	StdDraw.point(hullCentroid.x(), hullCentroid.y());
        
        // Draw hull centroid circle
    	// insert your code here
    	StdDraw.circle(hullCentroid.x(), hullCentroid.y(), hullCentroidRadius);
    	StdDraw.show(delay);
    }
    
    public Point2D getHullCentroid() {
        return hullCentroid;
    }
    
    public double getHullCentroidRadius() {
        return hullCentroidRadius;
    }
    
    // Problems 19 & 20. Research Project.
    // After experimenting with several different sets of 
    // random points, it is easy to see that, although the 
    // hull centroid circle does contain one point on the 
    // convex hull and does enclose all the other points, 
    // it is not the smallest circle enclosing the convex
    // hull. 
    /****************************************************
    ** Your job is to devise (or research) a smallest 
    ** enclosing circle (SEC) algorithm that improves the
    ** hull centroid circle and implement it here.
    ** If necessary you can create a separate class
    ** and use it to make an SEC object here and call
    ** its methods here.
    *****************************************************/
    /**************************************************
    ** Note that you must also test your work using the file
    ** MainSecTD.java provided. This means that you must
    ** create all the additional methods used in this file.
    *******************************************************/
    private Point2D secCenter;
    private double secRadius;
    
    public boolean inCirc(Point2D c, double r) {
        for (var p : points)
            if (p.distanceTo(c) > r+0.001) return false;
        return true;
    }
    
    public void createSec() {
        secRadius = 1e9;
        for (int i = 0; i < points.length; ++i) {
            for (int j = i+1; j < points.length; ++j) {
                Point2D c = new Point2D((points[i].x()+points[j].x())/2, (points[i].y()+points[j].y())/2);
                double r = c.distanceTo(points[i]);
                if (inCirc(c, r) && r < secRadius+0.001) {
                    secCenter = c;
                    secRadius = r;
                }
            }
        }
        for (int i = 0; i < points.length; ++i) {
            for (int j = i+1; j < points.length; ++j) {
                for (int k = j+1; k < points.length; ++k) {
                    // https://hratliff.com/posts/2019/02/curvature-of-three-points/
                    double x1 = points[i].x();
                    double y1 = points[i].y();
                    double x2 = points[j].x();
                    double y2 = points[j].y();
                    double x3 = points[k].x();
                    double y3 = points[k].y();
                    double x = ((y3-y1)/2-(x2*x2-x1*x1)/(2*y2-2*y1)+(x3*x3-x2*x2)/(2*y3-2*y2));
                    x /= ((x3-x2)/(y3-y2)-(x2-x1)/(y2-y1));
                    double y = -(x2-x1)/(y2-y1)*(x-(x1+x2)/2)+(y1+y2)/2;
                    try {
                        Point2D c = new Point2D(x, y);
                        double r = c.distanceTo(points[i]);
                        if (inCirc(c, r) && r < secRadius+0.001) {
                            secCenter = c;
                            secRadius = r;
                        }
                    }
                    catch (Exception e) {
                        // prob should do something here?
                    }
                }
            }
        }
    }

    public void drawSec(int delay)
    {
    	// Draw hull centroid
    	// insert your code here
    	StdDraw.setPenColor(StdDraw.RED);
    	StdDraw.point(secCenter.x(), secCenter.y());
        
        // Draw hull centroid circle
    	// insert your code here
    	StdDraw.circle(secCenter.x(), secCenter.y(), secRadius);
    	StdDraw.show(delay);
    }
    
    public Point2D getSecCenter() {
        return secCenter;
    }
    
    public double getSecRadius() {
        return secRadius;
    }
    
    
/***********************************
 ** Do not change anything below. **
 ***********************************/
    
    public void init()
    {
    	// Sort points by lowest y-coordinate (see Points2D) and break 
    	// ties using the x-coordinate. This makes points[0] the base
    	// point of the convex hull.
        Arrays.sort(points);
        hullStack.push(points[0]); 
        
        // Next, sort points by (counter clockwise) polar angle with respect
        // to the base point, and break ties by distance to the base point
        // (see Points2D for details).
        Arrays.sort(points, points[0].polarOrder());
        Point2D[] temp = points.clone();
        //System.out.println(Arrays.toString(temp));
         
        // Depending on the constructor used, there may be duplicate points. 
        // Find the index k1 of first point not equal to the base point.
        // Note that declaring k1 outside the loop makes it available to use.
        int k1;
        for (k1 = 1; k1 < N; k1++)
        {
            if (!points[0].equals(points[k1])) break;
        }
        // By construction p1 must be on the convex hull, so we add it.
        hullStack.push(points[k1]);   
        
        // Find the index k2 of the first point not collinear with the base point
        // and the point, points[k1]. Let p0 = points[0], p1 = points[k1], and
        // p2 = points[k2]. Further, let w = vector from p1 to p0 and let 
        // v = vector from p1 to p2. From Calculus III, if v and w are not
        // parallel, then the vector cross product, v x w, is not equal to zero.
        // The Point2D.ccw method determines if v x w is zero, and if not, it
        // determines if it sign is positive or negative. If v x w is zero then
        // v and w are parallel which means that p0, p1, and p2 are collinear, 
        // in which case we want to keep looking for another point.
        int k2;
        for (k2 = k1 + 1; k2 < N; k2++)
        {
            if (Point2D.ccw(points[0], points[k1], points[k2]) != 0) break;
        }
     
        // Now, we use the method Point2D.ccw to determine whether or not the angle 
        // p0-p1-p2 makes a left-hand turn, which it must do by construction, and 
        // so we put p2 on the stack (possibly temporarily). Next, we continue in
        // this way checking whether or not the angle p1-p2-p3 makes a left-hand
        // turn. If it does we put p3 on the stack, otherwise we don't put it on
        // the stack, etc.
        for (int i = k2; i < N; i++) 
        {
            Point2D top = hullStack.pop();
            while (Point2D.ccw(hullStack.peek(), top, points[i]) <= 0) 
            {
                top = hullStack.pop();
            }
            hullStack.push(top);
            hullStack.push(points[i]);
        }
    }
    
    public void createHullPoints()
    {
           for(Point2D point : hullStack)
           {
    	      hullPoints.add(point);
           }
    }
    
    public void drawHull(int delay)
    {
        StdDraw.setCanvasSize(700, 700);
        StdDraw.setXscale(-10, 110);
        StdDraw.setYscale(-10, 110);
        StdDraw.setPenRadius(.005);
        
        for (int i = 0; i < N; i++) 
        {
            points[i].draw();
        }
    	
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.setPenRadius(.02);
        Point2D redPoint = hullPoints.get(0);
        redPoint.draw();
       
        StdDraw.setPenColor(StdDraw.GRAY);
        StdDraw.setPenRadius();
        
        for (int i = 1; i < N; i++) 
        {
        	redPoint.drawTo(points[i]);
            StdDraw.show(delay);
        }
        
        hullPoints.add(hullPoints.get(0));
        StdDraw.setPenColor(StdDraw.BLUE);
        StdDraw.setPenRadius();
        for(int i = 0; i <hullPoints.size()-1; i++)
        {
        	hullPoints.get(i).drawTo(hullPoints.get(i+1));
            StdDraw.show(delay);
        }
        hullPoints.remove(hullPoints.size()-1);
    }
    
    public Point2D[] getPoints()
    {
    	return points;
    }
    
    public Stack<Point2D> getHullStack()
    {
    	return hullStack;
    }

    public ArrayList<Point2D> getHullPoints()
    {
    	return hullPoints;
    }
    
    public String toString()
    {
    	return "convex hull: " + hullPoints.toString();
    }

    // Check that boundary of hull is indeed convex.
    public boolean isConvex() 
    {
        int N = hullStack.size();
        if (N <= 2) return true;

        Point2D[] points = new Point2D[N];
        int n = 0;
        for (Point2D p : hullStack) 
        {
            points[n++] = p;
        }
       
        for (int i = 0; i < N; i++) 
        {
            if (Point2D.ccw(points[i], points[(i+1) % N], points[(i+2) % N]) <= 0) 
            {
                return false;
            }
        }
        return true;
    }
}


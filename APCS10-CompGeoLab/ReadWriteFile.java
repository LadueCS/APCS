/********  ReadWriteFile.java *********
 *  
 *  APCS Labs 2011-21
 *  Computational Geometry
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *
 */

import java.io.*;
import java.util.*;

public class ReadWriteFile 
{
	private ArrayList<String> fileLines;
	private ArrayList<ArrayList<String>> parsedFileLines;
	private ArrayList<Ntuple> ntupleLines;

	public ReadWriteFile()
	{
		
	}
	
	public ReadWriteFile(String fileName)
	{ 
		// create fileLines array
		fileLines = new ArrayList<String>();
		createFileLines(fileName);
		parsedFileLines = parseFileLines(fileLines);
	}
	
	public ReadWriteFile(String fileName, int[] typeList)
	{ 
		// create ntupleLines array
		ntupleLines = new ArrayList<Ntuple>();
		createNtupleLines(fileName, typeList);
	}
	
	public ReadWriteFile(String fileName, int type)
	{ 
		// create ntupleLines array
		ntupleLines = new ArrayList<Ntuple>();
		createNtupleLines(fileName, type);
	}
	
	public ArrayList<Ntuple> getNtupleLines()
	{
		return ntupleLines;
	}
	
	// Read each homogeneous line (record) of the input file of possibly varying
	// length into an ArrayList of Ntuples: ntupleLines.
	public void createNtupleLines(String fileName, int type)
	{
		String line = null;
				    // Must use try-catch construct 
				    try{
				           BufferedReader bufferedReader = 
				                   new BufferedReader(new FileReader(fileName));
				            while((line = bufferedReader.readLine()) != null) 
				            {
				            	//System.out.println(createNtuple(parseLine(line), typeList));
				               ntupleLines.add(createNtuple(parseLine(line), type));
				             }    
				             bufferedReader.close();            
				         }
				      catch(IOException ex){}
	}
		
	// convert an ArrayList of Strings into an Ntuple of homogeneous types
	public Ntuple createNtuple(ArrayList<String> parsedLine, int type)
	{
		Ntuple ntuple = null;
				
		if(parsedLine.size() > 0)
		{
			for(int i = 0; i < parsedLine.size(); i++)
			{
				if (i == 0)
				{
					   if (type == 1)
						    ntuple = new Ntuple(Integer.parseInt(parsedLine.get(i)));
					   else if (type == 2)
							ntuple = new Ntuple(Double.parseDouble(parsedLine.get(i)));
					   else
							ntuple = new Ntuple(parsedLine.get(i));
				}
				else
				{
					   if (type == 1)
					   {
							ntuple = ntuple.append(Integer.parseInt(parsedLine.get(i)));
					   }
					   else if (type == 2)
							ntuple = ntuple.append(Double.parseDouble(parsedLine.get(i)));
					   else
							ntuple = ntuple.append(parsedLine.get(i));
				}
			}
		}
		return ntuple;
	}	
	
	// Read each heterogeneous line (record) of the input file into an ArrayList
	// of Ntuples: ntupleLines.
	public void createNtupleLines(String fileName, int[] typeList)
	{
		String line = null;
			     // Must use try-catch construct 
			     try{
			            BufferedReader bufferedReader = 
			                    new BufferedReader(new FileReader(fileName));
			            while((line = bufferedReader.readLine()) != null) 
			            {
			            	//System.out.println(createNtuple(parseLine(line), typeList));
			               ntupleLines.add(createNtuple(parseLine(line), typeList));
			             }    
			             bufferedReader.close();            
			         }
			      catch(IOException ex){}
	}
	
	// convert an ArrayList of strings into an Ntuple of heterogeneous types
	public Ntuple createNtuple(ArrayList<String> parsedLine, int[] typeList)
	{
		Ntuple ntuple = null;
			
		if(parsedLine.size() == typeList.length)
		{
			for(int i = 0; i < parsedLine.size(); i++)
			{
				if (i == 0)
				{
				    if (typeList[i] == 1)
						ntuple = new Ntuple(Integer.parseInt(parsedLine.get(i)));
					else if (typeList[i] == 2)
						ntuple = new Ntuple(Double.parseDouble(parsedLine.get(i)));
					else
						ntuple = new Ntuple(parsedLine.get(i));
				}
				else
				{
					if (typeList[i] == 1)
					{
						ntuple = ntuple.append(Integer.parseInt(parsedLine.get(i)));
					}
					else if (typeList[i] == 2)
						ntuple = ntuple.append(Double.parseDouble(parsedLine.get(i)));
					else
						ntuple = ntuple.append(parsedLine.get(i));
				}
			}
		}
		return ntuple;
	}
		
	public ArrayList<ArrayList<String>> getParsedFileLines()
	{
		return parsedFileLines;
	}
	
	public ArrayList<ArrayList<String>> parseFileLines(ArrayList<String> line)
	{
		ArrayList<ArrayList<String>> parsedFileLines = new ArrayList<ArrayList<String>>();
		for(String str : line)
		{
			parsedFileLines.add(parseLine(str));
		}
		return parsedFileLines;
	}
	
	// parse an individual line of a file into
	// an array of strings
	public ArrayList<String> parseLine(String str)
	{
		String line = str;
		ArrayList<String> lineArray = new ArrayList<String>();
		while(!line.equals(""))
		{
			lineArray.add(prefixBeforeBlank(line));
			line = suffixAfterBlank(line);
		}
		return lineArray;
	}
	
	// precondition: str is a nonempty String possibly beginning with a blank
	// Find the first blank in str and return the suffix after that blank
	// with all blanks trimmed off the front and back of that suffix.
	// If there are no blanks in str then return the empty string. 
	public String suffixAfterBlank(String str)
	{
        String suffix = "";  
		int index = str.indexOf(" ");
		if(index != -1)
		   suffix = str.substring(index).trim();
		return suffix;
	}
	
	// precondition: str is a non null String
	// Return the nonempty prefix of str before (upto) the first blank.
	// If there is no blank, return str.
	public String prefixBeforeBlank(String str)
	{  
		String prefix = "";
		while(str.length() > 0 && !str.substring(0,1).equals(" "))
		{
			prefix += str.substring(0,1);
			str = str.substring(1);
		}
		return prefix;
	}
	
	// print file lines 
	public void printFileLines()
	{
		for(int i = 0; i < fileLines.size(); i++)
		{
			System.out.println("fileLines.get(" + i + ") = " + fileLines.get(i));
		}
	}
	
	// getter method for file = array of Strings
	public ArrayList<String> getFileLines()
	{
		return fileLines;
	}

	// Read each line (record) of the input file into an ArrayList
	// of Strings: fileLines.
	public void createFileLines(String fileName)
	{
		String line = null;
		      // Must use try-catch construct 
		      try{
		             BufferedReader bufferedReader = 
		                     new BufferedReader(new FileReader(fileName));
		             while((line = bufferedReader.readLine()) != null) 
		             {
		            	 //System.out.println(line);
		                 fileLines.add(line);
		              }    
		             bufferedReader.close();            
		         }
		      catch(IOException ex){}
	}
	
	// Write ArrayList of Strings to an output file on the disk.
	public void writeOutput(ArrayList<String> output, String outputFileName) 
	{     
	  try{ 
		   BufferedWriter bufferedWriter = 
			   	  new BufferedWriter(new FileWriter(new File(outputFileName),false));
		   // enhanced for loop - grab each element of the ArrayList<String> - ignore indexes
		   for (String line : output) 
		   {
		       //System.out.println(line);
		       bufferedWriter.write(line);
		       bufferedWriter.newLine();
		   }
		   bufferedWriter.close();
	    }
	  catch(IOException ex){}
    }
	
	
}

/********  Ntuple.java *********
 *  
 *  APCS Labs 2011-21
 *  Computational Geometry
 *  Dr. John Pais 
 *  pais.john@gmail.com
 *
 *  Ntuple is a universal data type that creates an
 *  ordered ntuple of java Objects. Since every data type 
 *  created by a class is a subclass of the Object class,
 *  the Ntuple class provides a data structure for combining
 *  (representing) any collection of heterogeneous data types, 
 *  i.e. any collection of data objects into one Ntuple object.
 */

import java.util.ArrayList;

class Ntuple 
{

	private Object[] objList;
	private int N;
	
	public Ntuple(Object... objList)
	{
		this.objList = objList;
		N = objList.length;
	}

	public int getN()
	{
		return N;
	}
	
	public Object getkth(int k)
	{
		if (k < N)
		   return objList[k];
		else
		   return null;
	}
	/**
	public Ntuple replacekth(Ntuple ntuple, int k, Object object)
	{
		Integer n = ntuple.getN();
		if(0 <= k && k < n)
		{
			Object[] objList = new Object[n];
			for(int i = 0; i < n; i++)
			{
				if(i != k)
					objList[i] = ntuple.objList[i];
				else
					objList[i] = object;	
			}
			return new Ntuple(objList);
		}
		else
		{
			return ntuple;
		}
	}
	**/
	
	
	
	public Ntuple replacekth(int k, Object object)
	{
		if(0 <= k && k < N)
		{
			Object[] objList = new Object[N];
			for(int i = 0; i < N; i++)
			{
				if(i != k)
					objList[i] = this.objList[i];
				else
					objList[i] = object;	
			}
			return new Ntuple(objList);
		}
		else
		{
			return this;
		}
	}
	
	public Ntuple append(Object object)
	{
		Object[] objList = new Object[N+1];
		for(int i = 0; i < N+1; i++)
		{
		   if(i < N)
			   objList[i] = this.objList[i];
		   else
			   objList[i] = object;	
		}
		return new Ntuple(objList);
	}
		
	public Ntuple append(Ntuple ntuple)
	{
		int M = ntuple.getN();
		Object[] objList = new Object[N+M];
		for(int i = 0; i < N+M; i++)
		{
		   if(i < N)
			   objList[i] = this.objList[i];
		   else
			   objList[i] = ntuple.objList[i-N];	
		}
		return new Ntuple(objList);
	}
	
	
	public boolean equals(Ntuple tuple)
	{
		boolean itsEqual = true;
		
		for(int i = 0; i < N; i++)
		{
			itsEqual = itsEqual && (this.getkth(i).equals(tuple.getkth(i)));
			if(!itsEqual)
			   i = N;
		}
		return itsEqual;
	}
	
	public int indexIn(ArrayList<Ntuple> ntuples)
	{
		int index = -1;
		for(int i = 0; i < ntuples.size(); i++)
		{
			if(this.equals(ntuples.get(i)))
			{
				index = i;
				i = ntuples.size();
			}
		}
		return index;
	}
	
	public boolean containedIn(ArrayList<Ntuple> ntuples)
	{
		return (this.indexIn(ntuples) > -1);
	}
	
	// Find the longest, contiguous, increasing (nondecreasing) 
    // sequence contained in this ntuple. Return the
	// first such sequence as a new ntuple, if there 
	// is more than one.
    // Precondition: 1. this ntuple is homogeneous, 
    // i.e. it either is comprised of all Integer, 
    // or all Double, or all String components, and
    // 2. this ntuple is of length at least 2.
    public Ntuple findMaxIncSeq(int type)
	{
    	Ntuple code = codeAllIncSeq(type).get(0);
    	int start = (Integer)code.getkth(0);
    	int M = (Integer)code.getkth(2);
    	
    	Object[] objList = new Object[M];
		for(int i = 0; i < M; i++)
		{
			objList[i] = this.objList[i+start];
		}
		return new Ntuple(objList);
	}
	
	// Code all contiguous, increasing (nondecreasing) sequences
    // contained in this ntuple into an ntuple containing
    // the start index, the end index, and the length of 
    // each such sequence. Return an ArrayList of these
    // ntuple codes sorted by the length of the sequences.
    // Preconditions: 1. this ntuple is homogeneous, 
    // i.e. it either is comprised of all Integer, or all
    // Double, or all String components, and 2. this ntuple
    // is of length at least 2.
    public ArrayList<Ntuple> codeAllIncSeq(int type)
	{
		ArrayList<Ntuple> incSeqs = new ArrayList<Ntuple>();
		
		if(N > 1)
		{
			int start = 0;
			boolean condition;
			for(int i = 0; i < N-1; i++)
			{
				if (type == 1)
				    condition = ((Integer)this.getkth(i) > (Integer)this.getkth(i+1)); 
				else if (type == 2)
				    condition = ((Double)this.getkth(i) > (Double)this.getkth(i+1));
				else
				    condition = (((String)this.getkth(i)).compareTo((String)this.getkth(i+1)) > 0);
			    
				if(condition)
				{
			       incSeqs.add(new Ntuple(start,i,i+1-start));
			       start = i+1;
			       if(i+1 == N-1)
					   incSeqs.add(new Ntuple(i+1,i+1,1));
				}
				else
				{
				   if(i+1 == N-1)
					   incSeqs.add(new Ntuple(start,i+1,N-start));
				}
			}
		}
		NtupleComparator nc = new NtupleComparator(2,1,false);
		nc.sortNtupleList(incSeqs);
		return(incSeqs);
	}

	public String toString()
	{
		String vargs = "Ntuple(";
		for(int i = 0; i < N; i++)
		{  if (i < N-1)
			  vargs += getkth(i) + "," ;
		   else
			  vargs += getkth(i) + ")";
		}
		return vargs;
	}
	
}


/*****   Segment2D.java
 *  
 *  APCS Labs 2011-21
 *  Computational Geometry
 *  Dr. John Pais 
 *  pais.john@gmail.com
 */

public class Segment2D 
{
	private Point2D p1, p2;
	private double x1, y1, x2, y2;
	
	public Segment2D(Point2D p1, Point2D p2)
	{
		   this.p1 = p1;
		   this.p2 = p2;
		   x1 = p1.x();
		   y1 = p1.y();
		   x2 = p2.x();
		   y2 = p2.y();
	}
	
	public Point2D getP1()
	{
		return p1;
	}
	
	public Point2D getP2()
	{
		return p2;
	}
	
	// Test whether or not a line segment object is nontrivial,
	// i.e. whether or not it is comprised of two different points.
	public boolean isValid(Segment2D seg)
	{
		return !seg.getP1().equals(seg.getP2());
	}
	 
	// Test whether or not two valid (nontrivial) line segments 
	// intersect when not parallel or overlap when parallel.
	public boolean segmentsIntersect(Segment2D seg1, Segment2D seg2)
	{
	   boolean segmentsIntersect = false;
	   if (isValid(seg1) && isValid(seg2))
	   {
	    /* Starting with the vector parametric equations of the 
	     * two line segments: (1-t)*v1 + t*v2 = (1-s)*w1 + s*w2,
	     * we need to solve the system (I):
	     *    at + bs = e
	     *    ct + ds = f
	     *  which is obtained by rewriting the vector equation 
	     *  above in component form. 
	     *  Since this is a 2x2 system, we will do this by using
	     *  the inverse of the coefficient matrix.
	     */
	 	double a = seg1.x2 - seg1.x1;
	 	double b = seg2.x1 - seg2.x2;
	 	double c = seg1.y2 - seg1.y1;
	 	double d = seg2.y1 - seg2.y2;
	 	double e = seg2.x1 - seg1.x1;
	 	double f = seg2.y1 - seg1.y1;
	 	//System.out.println("this.x2 = " + this.x2 + " seg1.x2 = " + seg1.x2 + " seg2.x2 = " + seg2.x2);
	 	double t, s;
	 	
	    // Determinant != 0, which means the two lines intersect somewhere.
	 	if (a*d - b*c != 0) 
	 	{
	 		t = (d*e - b*f)/(a*d - b*c);
	 		s = (-c*e + a*f)/(a*d - b*c);
	 		
	 	    // Intersection occurs on segments themselves.
	 		if (0 <= t && t <= 1 && 0 <= s && s <= 1) 
	 			segmentsIntersect = true;	
	 	}
	 	// Line segments are parallel.
	 	else
	 	{
	 		// Line segments overlap, in which case an endpoint
	 		// of (each) one must satisfy the line segment equation
	 		// of the other. So, if this is true, then (for example)
	 		// the equations in system (I) must be solvable with 
	 		// either t = 0 or t = 1, and 0 <= s <= 1.
	 		if( b != 0 && ((0 <= e/b && e/b <= 1) || (0 <= (e-a)/b && (e-a)/b <= 1)) )
	 			segmentsIntersect = true;
	 		else if ( d != 0 && ((0 <= f/d && f/d <= 1) || (0 <= (f-c)/d && (f-c)/d <= 1)) )
	 		          segmentsIntersect = true;
	 	}
	   }
	   return segmentsIntersect;
	 }
	
	public String toString()
	{
		return "Segment(" + p1 + "," + p2 + ")";
	}
}

/*****   Main.java
 *  
 *  APCS Project 2011-21
 *  Computational Geometry
 *  Dr. John Pais 
 *  pais.john@gmail.com
 * 
 *  GSConvexHull.java is a modified version of original code created by 
 *  Robert Sedgewick and Kevin Wayne. http://algs4.cs.princeton.edu/home/
 * 
 *  Generate random points and compute the convex hull using the
 *  Graham scan algorithm. The convex hull of a set of points in the 
 *  plane is the smallest subset of boundary points (fence posts) that 
 *  can be used to enclose the whole set of points within the shortest 
 *  perimeter fence. 
 *
 *  May be floating-point issues if x- and y-coordinates are not integers.
 *  
 *  In addition, the following methods and graphics objects of the
 *  convex hull are created:
 *  (1) the diameter and its length, 
 *  (2) the interior segments that intersect the diameter, and
 *  (3) the centroid, centroid radius, and centroid circle.
 *
 *********************************************************************/

import java.util.*;

public class Main 
{
	public static void main(String[] args) 
	{
		int N = 12;
        GSConvexHull gs = new GSConvexHull(N);
        gs.drawHull(400,true);
        System.out.println(gs);
        
        // Write code here to (appropriately) test all the methods
        // in Problems 1-18 that you create in GSConvexHull.java.
        // Note that Problems 19-20 must be tested in MainSecTD.java.
        
    }

}

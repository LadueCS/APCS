import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainMapPairs11
{
	public static void main ( String[] args )
	{
		ArrayList<String> arr = new ArrayList<String>();
		arr.add("It's");
		arr.add("Due");
		arr.add("Time");
		Map<String,String> map = changeIt11(arr);
		System.out.println("arr = " + arr);
		System.out.println("map = changeIt11(arr)");
	    for(String key : map.keySet())
	    {
	       System.out.println("f("+key+") = map.get("+key+") = " + map.get(key));
	    }  
	}
	
	// Problem 11. Given an array of non-empty strings, create and return a 
	// Map<String, String> as follows: for each string add its first character 
	// as a key with its last character as the value.
	public static Map<String,String> changeIt11(ArrayList<String> arr) 
	{
		Map<String,String> map = new HashMap<String, String>();
		// insert your code here
		
		
	}

}

/**Make a new empty map
Map<String, String> map = new HashMap<String, String>();
map.get(key) -- retrieves the stored value for a key, or null if that key is not present in the map.
map.put(key, value) -- stores a new key/value pair in the map. Overwrites any existing value for that key.
map.containsKey(key) -- returns true if the key is in the map, false otherwise.
map.remove(key) -- removes the key/value pair for this key if present. Does nothing if the key is not present.
**/
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainMapPairs10
{
	public static void main ( String[] args )
	{
		ArrayList<String> arr = new ArrayList<String>();
		arr.add("It's");
		arr.add("Due");
		arr.add("Time");
		Map<String,Integer> map = changeIt10(arr);
		System.out.println("arr = " + arr);
		System.out.println("map = changeIt10(arr)");
	    for(String key : map.keySet())
	    {
	       System.out.println("f("+key+") = map.get("+key+") = " + map.get(key));
	    }  
	}
	
	// Problem 10. Given an array of strings, return a Map<String, Integer> containing a 
	// key for every different string in the array, and the value is that string's length.
	public static Map<String,Integer> changeIt10(ArrayList<String> arr) 
	{
		Map<String,Integer> map = new HashMap<String, Integer>();
		// insert your code here
		
		
	}

}

/**Make a new empty map
Map<String, Integer> map = new HashMap<String, Integer>();
map.get(key) -- retrieves the stored value for a key, or null if that key is not present in the map.
map.put(key, value) -- stores a new key/value pair in the map. Overwrites any existing value for that key.
map.containsKey(key) -- returns true if the key is in the map, false otherwise.
map.remove(key) -- removes the key/value pair for this key if present. Does nothing if the key is not present.
**/

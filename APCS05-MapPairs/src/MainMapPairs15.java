import java.util.*;

public class MainMapPairs15 
{
	public static void main (String[] args)
	{
		String str1 = "laduettes";
		String str2 = "testladue";
		System.out.println("str1 = " + str1);
		System.out.println("str2 = " + str2);
		System.out.println("mapCharCounts(str1) = " + mapCharCounts(str1));
		System.out.println("mapCharCounts(str2) = " + mapCharCounts(str2));
		//System.out.println("areScrambled(str1,str2) = " + areScrambled(str1,str2));
	}
	
	// Problem 15. Use map to check whether or not two strings are
	// scrambled versions of each other. Also, write helper method
	// mapCharCounts to create a map to count occurrences of each 
	// character in a string.
    public static boolean areScrambled(String str1, String str2)
    {
	    // insert your code here
    	
    	
    }
    
    // Create a map containing (key,value) pairs to count the number of 
    // occurrences of each character in a string.
    public static Map<Character,Integer> mapCharCounts(String str)
    {
    	Map<Character,Integer> map = new HashMap<Character,Integer>();
    	char[] ch = str.toCharArray();
    	
    	for(int i = 0; i < ch.length; i++)
    	{
    	    if(map.get(ch[i]) == null) // character ch[i] not yet in map
    	    {
    	    	map.put(ch[i], 1);     // start count of this char at 1
    	    }
    	    else
    	    {
    	    	map.put(ch[i], map.get(ch[i]) + 1); // increment count of char
    	    }
    	}
    	return map;
    }
}
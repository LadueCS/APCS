import java.util.HashMap;
import java.util.Map;

public class MainMapPairs08
{
	public static void main ( String[] args )
	{
		Map<String,String> map = new HashMap<String, String>();
		map.put("salad", "oil"); // add (key, value) pair to map
		map.put("spinach", "lemon juice");
		map.put("potato", "ketchup");
		
		// display (key, value) as f(key) = value
	    for(String key : map.keySet())
	    {
	       System.out.println("f("+key+") = map.get("+key+") = " + map.get(key));
	    }
	    System.out.println("changeIt8(map)");
	    
	    // display (key, value) as f(key) = value
	    for(String key : changeIt8(map).keySet())
	    {
	       System.out.println("f("+key+") = map.get("+key+") = " + map.get(key));
	    }
	}
	
	// A Map<String, String> is a collection {(key1, value1), (key2, value2),...} of paired
	// strings, similar to an ArrayList<String> which is a list {str1, str2,...} of strings.
	// Further, it is similar to a set of ordered pairs of numbers {(x1,y1), (x2, y2),...}
	// that you often deal with in your math class, i.e. it is essentially a function f 
	// such that f(x1) = y1, f(x2) = y2,...etc.
	
	// Problem 8. Given a map of food keys and topping values, modify and return the map as
	// follows: if the key "potato" has a value, set that as the value for the key "fries". 
	// If the key "salad" has a value, set that as the value for the key "spinach".
	public static Map<String,String> changeIt8(Map<String,String> map) 
	{
		// insert your code here
		
		
	}

}

/**Make a new empty map
Map<String, String> map = new HashMap<String, String>();
map.get(key) -- retrieves the stored value for a key, or null if that key is not present in the map.
map.put(key, value) -- stores a new key/value pair in the map. Overwrites any existing value for that key.
map.containsKey(key) -- returns true if the key is in the map, false otherwise.
map.remove(key) -- removes the key/value pair for this key if present. Does nothing if the key is not present.
**/

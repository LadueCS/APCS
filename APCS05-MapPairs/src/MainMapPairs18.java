import java.util.*;

public class MainMapPairs18 
{
	public static void main (String[] args)
	{
		String str1 = "mmaduttes";
		String str2 = "teslauer";
		System.out.println("str1 = " + str1);
		System.out.println("str2 = " + str2);
		System.out.println("mapCharCounts(str1) = " + mapCharCounts(str1));
		System.out.println("mapCharCounts(str2) = " + mapCharCounts(str2));
		//System.out.println("mapDiffCount(str1,str2) = " + mapDiffCount(str1,str2));
		//System.out.println("mapDiffCount(str2,str1) = " + mapDiffCount(str2,str1));
	}
	
	// Problem 18. Given two strings, create a map containing (key,value) pairs
	// to count the occurrences of each character that occurs in the first one 
	// of the strings but not in the second one.
    public static Map<Character,Integer> mapDiffCount(String str1, String str2)
    {
    	Map<Character,Integer> map = new HashMap<Character,Integer>();
    	
    	Map<Character,Integer> map1 = mapCharCounts(str1);
    	Map<Character,Integer> map2 = mapCharCounts(str2);
    	
    	Set<Character> diff = new HashSet<Character>();
    	diff.addAll(map1.keySet());
    	diff.removeAll(map2.keySet());
    	
    	// insert your code here
    	
    	
    }
    
    // Create a map containing (key,value) pairs to count the number of 
    // occurrences of each character in a string.
    public static Map<Character,Integer> mapCharCounts(String str)
    {
    	Map<Character,Integer> map = new HashMap<Character,Integer>();
    	char[] ch = str.toCharArray();
    	
    	for(int i = 0; i < ch.length; i++)
    	{
    	    if(map.get(ch[i]) == null) // character ch[i] not yet in map
    	    {
    	    	map.put(ch[i], 1);     // start count of this char at 1
    	    }
    	    else
    	    {
    	    	map.put(ch[i], map.get(ch[i]) + 1); // increment count of char
    	    }
    	}
    	return map;
    }
}
import java.util.HashMap;
import java.util.Map;

public class MainMapPairs09
{
	public static void main ( String[] args )
	{
		Map<String,String> map = new HashMap<String, String>();
		map.put("a", "aa"); // add (key, value) pair to map
		map.put("b", "bb");
		map.put("c", "candy");
		
		// display (key, value) as f(key) = value
	    for(String key : map.keySet())
	    {
	       System.out.println("f("+key+") = map.get("+key+") = " + map.get(key));
	    }
	    System.out.println("changeIt9(map)");
	    
	    // display (key, value) as f(key) = value
	    for(String key : changeIt9(map).keySet())
	    {
	       System.out.println("f("+key+") = map.get("+key+") = " + map.get(key));
	    }
	}
	
	// A Map<String, String> is a collection {(key1, value1), (key2, value2),...} of paired
	// strings, similar to an ArrayList<String> which is a list {str1, str2,...} of strings.
	// Further, it is similar to a set of ordered pairs of numbers {(x1,y1), (x2, y2),...}
	// that you often deal with in your math class, i.e. it is essentially a function f 
	// such that f(x1) = y1, f(x2) = y2,...etc.
	
	// Problem 9. Modify and return the given map as follows: if the keys "a" and "b" 
	// have values that have different lengths, then set "c" to have the longer value. 
	// If the values exist and have the same length, change them both to the empty 
	// string in the map.
	public static Map<String,String> changeIt9(Map<String,String> map) 
	{
		// insert your code here
		
		
		
	}

}

/**Make a new empty map
Map<String, String> map = new HashMap<String, String>();
map.get(key) -- retrieves the stored value for a key, or null if that key is not present in the map.
map.put(key, value) -- stores a new key/value pair in the map. Overwrites any existing value for that key.
map.containsKey(key) -- returns true if the key is in the map, false otherwise.
map.remove(key) -- removes the key/value pair for this key if present. Does nothing if the key is not present.
**/
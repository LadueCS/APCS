import java.util.*;

public class MainMapPairs19 
{
	public static void main (String[] args)
	{
		String str1 = "mmaduttes";
		String str2 = "teslauer";
		System.out.println("str1 = " + str1);
		System.out.println("str2 = " + str2);
		//System.out.println("maxLenNoRepeats(str1) = " + maxLenNoRepeats(str1));
		//System.out.println("maxLenNoRepeats(str2) = " + maxLenNoRepeats(str2));
	}
	
	// Problem 19. Given a string, find the length of the longest substring such that 
	// all characters occur only once, i.e. no repeats. For "ababcbb" the corresponding 
	// substring is "abc", which has length 3. For "bbbbb" the corresponding substring 
	// is "b", which has length 1.
    public static int maxLenNoRepeats(String str)
    {
    	char[] arr = str.toCharArray();
    	int len = 0;
    	HashMap<Character, Integer> map = new HashMap<Character, Integer>();
    	for (int i = 0; i < arr.length; i++) 
    	{
    	    if (!map.containsKey(arr[i])) 
    	    {
    	    	// insert your code here
    	    	
    	    } 
    	    else 
    	    {
    	    	// insert your code here
    	    	
    	    }
    	}
    	return Math.max(len, map.size());
    }
  
}
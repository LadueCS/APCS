import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainMapPairs14
{
	public static void main ( String[] args )
	{
		ArrayList<String> arr = new ArrayList<String>();
		arr.add("It's");
		arr.add("Due");
		arr.add("ITime");
		arr.add("Dude");
		arr.add("Nicht wahr");
		Map<String,String> map = changeIt14(arr);
		System.out.println("arr = " + arr);
		System.out.println("map = changeIt14(arr)");
	    for(String key : map.keySet())
	    {
	       System.out.println("f("+key+") = map.get("+key+") = " + map.get(key));
	    }  
	}
	
	// Problem 14. Given an array of non-empty strings, return a Map<String, String> 
	// with a key for every different first character seen, with the value of all the 
	// strings starting with that character appended together in the order they appear 
	// in the array. 
	public static Map<String,String> changeIt14(ArrayList<String> arr) 
	{
		Map<String,String> map = new HashMap<String, String>();
		ArrayList<String> temp = new ArrayList<String>(arr);
		String key = "";
		// insert your code here
		
		
	}

	// helper method 
	public static String sameStartAppend(String key, ArrayList<String> arr)
	{
		String append = "";
		// insert your code here
		
		
	}
	
	// helper method 
	public static ArrayList<String> sameStartRemove(String key, ArrayList<String> arr)
	{
		ArrayList<String> result = new ArrayList<String>();
		// insert your code here
		
		
	}	
	
}

/**Make a new empty map
Map<String, String> map = new HashMap<String, String>();
map.get(key) -- retrieves the stored value for a key, or null if that key is not present in the map.
map.put(key, value) -- stores a new key/value pair in the map. Overwrites any existing value for that key.
map.containsKey(key) -- returns true if the key is in the map, false otherwise.
map.remove(key) -- removes the key/value pair for this key if present. Does nothing if the key is not present.
**/
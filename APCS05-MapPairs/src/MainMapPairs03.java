import java.util.HashMap;
import java.util.Map;

public class MainMapPairs03
{
	public static void main ( String[] args )
	{
		Map<String,String> map = new HashMap<String, String>();
		map.put("a", "candy"); // add (key, value) pair to map
		map.put("b", "dirt");
		
		// display (key, value) as f(key) = value
	    for(String key : map.keySet())
	    {
	       System.out.println("f("+key+") = map.get("+key+") = " + map.get(key));
	    }
	    System.out.println("changeIt3(map)");
	    
	    // display (key, value) as f(key) = value
	    for(String key : changeIt3(map).keySet())
	    {
	       System.out.println("f("+key+") = map.get("+key+") = " + map.get(key));
	    }
	}
	
	// A Map<String, String> is a collection {(key1, value1), (key2, value2),...} of paired
	// strings, similar to an ArrayList<String> which is a list {str1, str2,...} of strings.
	// Further, it is similar to a set of ordered pairs of numbers {(x1,y1), (x2, y2),...}
	// that you often deal with in your math class, i.e. it is essentially a function f 
	// such that f(x1) = y1, f(x2) = y2,...etc.
	
	// Problem 3. Modify and return the given map as follows: for this problem the map may 
	// or may not contain the "a" and "b" keys. If both keys are present, append their 2 
	// string values together and store the result under the key "ab".
	public static Map<String,String> changeIt3(Map<String,String> map) 
	{
		// insert your code here
		
		
	}

}

/**Make a new empty map
Map<String, String> map = new HashMap<String, String>();
map.get(key) -- retrieves the stored value for a key, or null if that key is not present in the map.
map.put(key, value) -- stores a new key/value pair in the map. Overwrites any existing value for that key.
map.containsKey(key) -- returns true if the key is in the map, false otherwise.
map.remove(key) -- removes the key/value pair for this key if present. Does nothing if the key is not present.
**/
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainMapPairs12
{
	public static void main ( String[] args )
	{
		ArrayList<String> arr = new ArrayList<String>();
		arr.add("a");
		arr.add("b");
		arr.add("a");
		arr.add("b");
		arr.add("b");
		arr.add("c");
		
		Map<String,Integer> map = changeIt12(arr);
		System.out.println("arr = " + arr);
		System.out.println("map = changeIt12(arr)");
	    for(String key : map.keySet())
	    {
	       System.out.println("f("+key+") = map.get("+key+") = " + map.get(key));
	    }  
	}
	
	// Problem 12. Given an array of strings, return a Map<String, Integer> with a key 
	// for each different string, with the value the number of times that string appears 
	// in the array.
	public static Map<String,Integer> changeIt12(ArrayList<String> arr) 
	{
		Map<String,Integer> map = new HashMap<String, Integer>();
		
		for (String s : arr) {
            int x = 1;
            if (map.containsKey(s)) x += map.get(s);
            map.put(s, x);
		}
		
		return map;
	}
	
	// helper method to compute how many times key appears in arr
	public static int countNStr(String key, ArrayList<String> arr)
	{
		// insert your code here
		
		return 0;
	}

}

/**Make a new empty map
Map<String, Integer> map = new HashMap<String, Integer>();
map.get(key) -- retrieves the stored value for a key, or null if that key is not present in the map.
map.put(key, value) -- stores a new key/value pair in the map. Overwrites any existing value for that key.
map.containsKey(key) -- returns true if the key is in the map, false otherwise.
map.remove(key) -- removes the key/value pair for this key if present. Does nothing if the key is not present.
**/

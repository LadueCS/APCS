import java.util.*;

public class MainMapPairs17 
{
	public static void main (String[] args)
	{
		String str1 = "mmaduttes";
		String str2 = "teslauer";
		System.out.println("str1 = " + str1);
		System.out.println("str2 = " + str2);
		System.out.println("mapCharCounts(str1) = " + mapCharCounts(str1));
		System.out.println("mapCharCounts(str2) = " + mapCharCounts(str2));
		//System.out.println("mapMaxCount(str1,str2) = " + mapMaxCount(str1,str2));
	}
	
	// Problem 17. Given two strings, create a map containing (key,value) pairs
	// to count the maximum number of occurrences of each character that occurs
	// in either one of the strings.
    public static Map<Character,Integer> mapMaxCount(String str1, String str2)
    {
    	Map<Character,Integer> map = new HashMap<Character,Integer>();
    	
    	Map<Character,Integer> map1 = mapCharCounts(str1);
    	Map<Character,Integer> map2 = mapCharCounts(str2);
    	
    	Set<Character> union = new HashSet<Character>();
    	union.addAll(map1.keySet());
    	union.addAll(map2.keySet());
    	
    	// insert your code here
    	
    	
    }
    
    // Create a map containing (key,value) pairs to count the number of 
    // occurrences of each character in a string.
    public static Map<Character,Integer> mapCharCounts(String str)
    {
    	Map<Character,Integer> map = new HashMap<Character,Integer>();
    	char[] ch = str.toCharArray();
    	
    	for(int i = 0; i < ch.length; i++)
    	{
    	    if(map.get(ch[i]) == null) // character ch[i] not yet in map
    	    {
    	    	map.put(ch[i], 1);     // start count of this char at 1
    	    }
    	    else
    	    {
    	    	map.put(ch[i], map.get(ch[i]) + 1); // increment count of char
    	    }
    	}
    	return map;
    }
}
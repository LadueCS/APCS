import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainMapPairs13
{
	public static void main ( String[] args )
	{
		ArrayList<String> arr = new ArrayList<String>();
		arr.add("It's");
		arr.add("Due");
		arr.add("ITime");
		arr.add("Due");
		arr.add("It's");
		Map<String,Boolean> map = changeIt13(arr);
		System.out.println("arr = " + arr);
		System.out.println("map = changeIt13(arr)");
	    for(String key : map.keySet())
	    {
	       System.out.println("f("+key+") = map.get("+key+") = " + map.get(key));
	    }  
	}
	
	// Problem 13. Given an array of strings, return a Map<String, Boolean> where each 
	// different string is a key and its value is true if that string appears 2 or more 
	// times in the array, and otherwise, its value is false if it appears only once.
	public static Map<String,Boolean> changeIt13(ArrayList<String> arr) 
	{
		Map<String,Boolean> map = new HashMap<String, Boolean>();
		ArrayList<String> temp = new ArrayList<String>(arr);
		String key = "";
		// insert your code here
		
		
	}

	// helper method 
	public static int countStr(String key, ArrayList<String> arr)
	{
		// insert your code here
		
		
	}

	// helper method 
	public static ArrayList<String> sameRemove(String key, ArrayList<String> arr)
	{
		ArrayList<String> result = new ArrayList<String>();
		// insert your code here
		
		
	}	
	
}

/**Make a new empty map
Map<String, String> map = new HashMap<String, String>();
map.get(key) -- retrieves the stored value for a key, or null if that key is not present in the map.
map.put(key, value) -- stores a new key/value pair in the map. Overwrites any existing value for that key.
map.containsKey(key) -- returns true if the key is in the map, false otherwise.
map.remove(key) -- removes the key/value pair for this key if present. Does nothing if the key is not present.
**/

public class Salsba extends AbstractDance
{

	 private static DanceStep[] steps = 
	    {
	       new DanceStep(0,  50,   0, 0,    0,   0,   0, 0),
	       new DanceStep(0,   0,   0, 0,    0,   0,   0, -50),
	       new DanceStep(0, -50, 0,  0,    0,   0, 0,  0),
	       new DanceStep(0,   0,  40, 0,    0,   0,   0, 0),

	       new DanceStep(0,   0,   0, 0,    0, 50,   0, 0),
	       new DanceStep(0,   0,   0, 0,    0,   0,   0, -50),
	       new DanceStep(0,   0, 0,  0,    0,  50, 0,  0),
	       new DanceStep(0,   0,   0, 0,    0,   0, -40, 0),
	    };

	  //private static int[] rhythm = {1, 2, 1, 1};
	 private static int[] rhythm = {1, 2, 1, 2};
	 
	  public Salsba()
	  {
	    super(steps, rhythm);
	  }

	  public String getName()
	  {
	    return "Salsba";
	  }

	  public int getTempo()
	  {
	    return 600;
	  }

	
}


public class Main 
{
	
	private static DanceFloor danceFloor;
    private static StudentGroup students;
    private static DanceLesson lesson;
    
    public static void main(String[] args)
    {
	  danceFloor = new DanceFloor();
	  students = new DanceGroup(danceFloor);
      lesson = new DanceLesson(students, danceFloor);
    }
  
}

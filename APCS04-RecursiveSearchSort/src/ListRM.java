import java.util.*;

public class ListRM 
{
	private List<String> list;
	public ListRM(List<String> list)
	{
		this.list = list;
	}
	
	public List<String> getList()
	{
		return list;
	}
	
	// mergesort method calls merge
	public List<String> mergesort(List<String> a) 
	{
	     int n = a.size();
	     if (n <= 1) 
	     {
	    	 return a;
	     }
	     else
	     {
	         return merge(mergesort(a.subList(0,n/2)), mergesort(a.subList(n/2,n)));
	     }   
	 }
	    
	 // merge method
	 public List<String> merge(List<String> a, List<String> b) 
	 {
		 List<String> c = Arrays.asList(new String[a.size()+b.size()]);
	     int i = 0, j = 0;
	     for (int k = 0; k < c.size(); k++) 
	     {
	         if (i >= a.size()) 
	         {
	        	 c.set(k,b.get(j++));
	         }
	         else if (j >= b.size())
	         {
	        	 c.set(k,a.get(i++));
	         }
	         else if (a.get(i).compareTo(b.get(j)) <= 0)
	         {
	        	 c.set(k, a.get(i++));
	         }
	         else 
	         {
	        	 c.set(k, b.get(j++));
	         }
	     }
	     return c;
	 }

	 // checks whether or not list is sorted
	 public boolean isSorted(List<String> a) 
	 {
	     for (int i = 1; i < a.size(); i++)
	     {
	         if (a.get(i).compareTo(a.get(i-1)) < 0) 
	        	 return false;
	     }
	     return true;
	 }
	 
	 // Calls recursive binary search, returns index of key
	 public int search(String key, List<String> a)
	 {
		 return search(key, a, 0, a.size());
	 }
		
	 // Recursive binary search, returns index of key
	 public int search(String key, List<String> a, int lo, int hi)
     {
		if(hi <= lo)
		{
			return -1;
		}
		int mid = lo + (hi - lo)/2;
		int cmp = a.get(mid).compareTo(key);
		if(cmp > 0)
		{
			return search(key, a, lo, mid);
		}
		else if (cmp < 0)
		{
			return search(key, a, mid+1, hi);
		}
		else
		{
			return mid;
		}			
	}	 
	

}

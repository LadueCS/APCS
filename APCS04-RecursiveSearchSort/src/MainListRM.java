import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class MainListRM 
{
    public static void main(String[] args) throws IOException
    {
       /******************************************************
		** create small example to test merge and mergesort **
		******************************************************/
    	
		List<String> arr1 = new ArrayList<String>();
		arr1.add("a");
		arr1.add("f");
		arr1.add("e");
		List<String> arr2 = new ArrayList<String>();
		arr2.add("b");
		arr2.add("d");
		arr2.add("c");
		System.out.println("small example:");
		System.out.println("arr1 = " + arr1);
		System.out.println("arr2 = " + arr2);
		List<String> arr3 = new ArrayList<String>(arr1);
		arr3.addAll(arr2);
		ListRM sm = new ListRM(arr3);
		System.out.println("sm.merge(" +arr1 +","+ arr2 +") = " + sm.merge(arr1, arr2));
		System.out.println("arr3 = " + arr3);
		System.out.println("sm.mergesort("+arr3+") = " + sm.mergesort(arr3));
		
		/************************************************
		** read large file to test merge and mergesort **
		*************************************************/
		
		ListRM lrm = new ListRM(readFileLines("large1M.txt"));
// 		ListRM lrm = new ListRM(readFileLines("large10M.txt"));
		System.out.println("\nlarge example:");
		List<String> list = lrm.getList();
		System.out.println("list.size() = " + list.size());
		System.out.println("list.subList(0,10) = " + list.subList(0, 10));	
		
		// start sort time
	    long startSort = System.currentTimeMillis();
	    // sort
	    System.out.println("sorting ...");
		list = lrm.mergesort(list);
// 		Collections.sort(list);
		System.out.println("list.subList(0,100) = " + list.subList(0, 100));
		System.out.println("lrm.isSorted(list) = " + lrm.isSorted(list));
		// end sort time
	    long endSort = System.currentTimeMillis();
	    System.out.println("\nsort time = " + (endSort-startSort)/1000. + " seconds"); 

	    /***************************************************
		** read and sort large file to test binary search **
		****************************************************/	  
	    		
	    // generate random key
	    // use sorted list from above
	    int rndIndex =  (int)(list.size()*Math.random());
		String rndKey = list.get(rndIndex);
		
		// start search time
	    long startSearch = System.currentTimeMillis();
	    
	    // binary search
		int indexOfKey = lrm.search(rndKey, list);
		System.out.println("\nrndIndex =  " + rndIndex + " ,rndKey =  " + rndKey );
		System.out.println("index found = " + indexOfKey + ", key found = " + list.get(indexOfKey));
		
		// end search time
	    long endSearch = System.currentTimeMillis();
	    System.out.println("\nsearch time = " + (endSearch-startSearch)/1000. + " seconds"); 	    
	}
	
    // utility method to read from disk
    public static ArrayList<String> readFileLines(String fileName) throws IOException
    {
	    ArrayList<String> fileLines = new ArrayList<String>();
	    String line = null;
	    BufferedReader bufferedReader = 
	           new BufferedReader(new FileReader(fileName));
	    while((line = bufferedReader.readLine()) != null) 
	    {
	        fileLines.add(line);
	    }    
	    bufferedReader.close();
	    return fileLines;
    }
}


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;

public class MainArrayRM 
{
    public static void main(String[] args) throws IOException
    {
       /******************************************************
		** create small example to test merge and mergesort **
		******************************************************/
    	
    	String[] arr1 = {"a","f","e"};
    	String[] arr2 = {"b","d","c"};
		System.out.println("small example:");
		System.out.println("arr1 = " + Arrays.toString(arr1));
		System.out.println("arr2 = " + Arrays.toString(arr2));
		String[] arr3 = new String[arr1.length + arr2.length];
		for(int i = 0; i < arr3.length; i++)
        {
			if(i < arr1.length)
			{
        	   arr3[i] = arr1[i];
			}
			else
			{
				arr3[i] = arr2[i-arr1.length];
			}
        }

/**	uncomment below after completing ArrayRM.java methods 	

		ArrayRM sm = new ArrayRM(arr3);
		System.out.println("sm.merge(" +Arrays.toString(arr1) +","+ Arrays.toString(arr2) +") = " + Arrays.toString(sm.merge(arr1, arr2)));
		System.out.println("arr3 = " + Arrays.toString(arr3));
		System.out.println("sm.mergesort("+Arrays.toString(arr3)+") = " + Arrays.toString(sm.mergesort(arr3)));
		
	// read large file to test merge and mergesort
	
		ArrayList<String> input = readFileLines("large1M.txt");
		//ArrayList<String> input = readFileLines("large10M.txt");
		String[] arr = new String[input.size()];
        for(int i = 0; i < arr.length; i++)
        {
        	arr[i] = input.get(i);
        }
		ArrayRM arm = new ArrayRM(arr);
		System.out.println("\nlarge example:");
		System.out.println("arr.length = " + arr.length);
		for(int i = 0; i < 10; i++)
		{
		   System.out.print(" arr["+ i + "] = " + arr[i]);	
		}
		// start sort time
	    long startSort = System.currentTimeMillis();
	    // sort
	    System.out.println("\nsorting ...");
		arr = arm.mergesort(arr);
		for(int i = 0; i < 10; i++)
		{
		   System.out.print(" arr["+ i + "] = " + arr[i]);	
		}
		System.out.println("\narm.isSorted(arr) = " + arm.isSorted(arr));
		// end sort time
	    long endSort = System.currentTimeMillis();
	    System.out.println("\nsort time = " + (endSort-startSort)/1000. + " seconds"); 
	    
	// read and sort large file to test binary search 
			  	
	    // generate random key
	    // use sorted list from above
	    int rndIndex =  (int)(arr.length*Math.random());
		String rndKey = arr[rndIndex];
		
		// start search time
	    long startSearch = System.currentTimeMillis();
	    
	    // binary search
		int indexOfKey = arm.search(rndKey, arr);
		System.out.println("\nrndIndex =  " + rndIndex + " ,rndKey =  " + rndKey );
		System.out.println("index found = " + indexOfKey + ", key found = " + arr[indexOfKey]);
		
		// end search time
	    long endSearch = System.currentTimeMillis();
	    System.out.println("\nsearch time = " + (endSearch-startSearch)/1000. + " seconds"); 	
**/        
	}
	
    // utility method to read from disk
    public static ArrayList<String> readFileLines(String fileName) throws IOException
    {
	    ArrayList<String> fileLines = new ArrayList<String>();
	    String line = null;
	    BufferedReader bufferedReader = 
	           new BufferedReader(new FileReader(fileName));
	    while((line = bufferedReader.readLine()) != null) 
	    {
	        fileLines.add(line);
	    }    
	    bufferedReader.close();
	    return fileLines;
    }
}

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class Craps extends JFrame
{
	private DisplayPanel display;
    private CrapsTable table;
    private ControlPanel controls; 
    private JPanel panel;
    private Container container;
    
  // Constructor
  public Craps()
  {
    super("Craps");

    display = new DisplayPanel();
    table = new CrapsTable(display);
    controls = new ControlPanel(table); 

    panel = new JPanel();
    panel.setLayout(new BorderLayout());
    panel.setBorder(new EmptyBorder(0, 5, 0, 5));
    panel.add(display, BorderLayout.NORTH);
    panel.add(table, BorderLayout.CENTER);
    panel.add(controls, BorderLayout.SOUTH);

    container = getContentPane();
    container.add(panel, BorderLayout.CENTER);
  }

  public void createGUI()
  {
	    setBounds(100, 100, 320, 240);
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setVisible(true);
  }
  
/**  
  public static void main(String[] args)
  {
    Craps window = new Craps();
    window.setBounds(100, 100, 320, 240);
    window.setDefaultCloseOperation(EXIT_ON_CLOSE);
    window.setVisible(true);
  }
  **/
}


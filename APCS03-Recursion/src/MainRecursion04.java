public class MainRecursion04
{
	public static void main ( String[] args )
	{
		System.out.println("\ntriangle(0) = " + triangle(0));
        System.out.println("\ntriangle(1) = " + triangle(1));
        System.out.println("\ntriangle(2) = " + triangle(2));
        System.out.println("\ntriangle(3) = " + triangle(3));
	}
	
	// We have triangle made of blocks. The topmost row has 1 block, 
	// the next row down has 2 blocks, the next row has 3 blocks, and 
	// so on. Compute recursively (no loops or multiplication) the total 
	// number of blocks in such a triangle with the given number of rows.
	public static int triangle(int rows) 
	{
    	
    	
		//insert your code here
    	return rows>0?rows+triangle(rows-1):0;
    	
    }
}

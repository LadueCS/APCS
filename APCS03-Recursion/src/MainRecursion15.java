public class MainRecursion15
{
	public static void main ( String[] args )
	{
		System.out.println("\ncountSepPairs(\"aaxaa\") = " + countSepPairs("aaxaa"));
        System.out.println("\ncountSepPairs(\"axaxaxa\") = " + countSepPairs("axaxaxa"));
        System.out.println("\ncountSepPairs(\"xhihix\") = " + countSepPairs("xhihix"));
	}
	
	// We'll say that a "separated pair" in a string is two instances of the 
	// same character separated by another character. So, in "AxA" the A's make 
	// a separated pair. Separated pairs can overlap, so "AxAxA" contains 3 
	// separated pairs: 2 for A and 1 for x. Recursively compute the number of 
	// separated pairs in the given string.
	public static int countSepPairs(String str) 
	{
    	
    	
		//insert your code here
    	
    	
    }

}

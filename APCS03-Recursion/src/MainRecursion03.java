public class MainRecursion03
{
	public static void main ( String[] args )
	{
		System.out.println("\nfibonacci(0) = " + fibonacci(0));
        System.out.println("\nfibonacci(1) = " + fibonacci(1));
        System.out.println("\nfibonacci(2) = " + fibonacci(2));
        System.out.println("\nfibonacci(3) = " + fibonacci(3));
        System.out.println("\nfibonacci(4) = " + fibonacci(4));
        System.out.println("\nfibonacci(5) = " + fibonacci(5));
	}
	
	// The fibonacci sequence is a famous bit of mathematics, and it happens to have
	// a recursive definition. The sequence is: 0, 1, 1, 2, 3, 5, 8, 13, 21 and so on. 
	// Define a recursive fibonacci(n) method that returns the nth fibonacci number, 
	// with n = 0 representing the start of the sequence.
	public static int fibonacci(int n) 
	{
    	
    	
		//insert your code here
    	
    	
    }
}
